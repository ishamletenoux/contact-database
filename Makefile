develop:
	pip install -e .[test,dev]

test:
	py.test -vx

download-db:
	scp -r eurordis.prod:/srv/eurordis/backup.gz ./tmp/

restore-db:
	mongorestore --drop --gzip --archive=./tmp/backup.gz
