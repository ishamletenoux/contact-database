from eurordis import basket
from eurordis.models import (
    Disease,
    Group,
    GroupBelonging,
    Keyword,
    Membership,
    Organisation,
    Person,
    Role,
)


def test_load(person, organisation, person2, disease):
    person.diseases.append(disease)
    person.save()
    organisation.diseases.append(disease)
    organisation.save()
    assert basket.load(
        [
            {"person": person.pk},
            {"person": person2.pk},
            {"organisation": organisation.pk, "person": person.pk},
            {"organisation": organisation.pk},
        ]
    ) == [
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "diseases": "Congenital deficiency in alpha-fetoprotein",
            "email": "bmollison@bill.org",
            "groupings": "Bone,Eye",
            "first_name": "Bill",
            "languages": "en,fr",
            "last_name": "Mollison",
            "phone": "+9876543210",
            "title": "Mr",
            "organisation": "Bill Org",
            "kind": "Patient Organisation",
            "role": "CEO",
            "is_primary": "1",
            "organisation_pk": organisation.pk,
            "created_by": person.created_by.email,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": person.modified_at.isoformat(),
        },
        {
            "person_pk": person2.pk,
            "pk": person2.pk,
            "country_label": "Australia",
            "country": "AU",
            "email": "david@somewhere.org",
            "first_name": "David",
            "languages": "de,en",
            "last_name": "Holmgren",
            "phone": "+9876543234",
            "title": "Mr",
            "created_by": person2.created_by.email,
            "created_at": person2.created_at.isoformat(),
            "modified_by": person2.modified_by.email,
            "modified_at": person2.modified_at.isoformat(),

        },
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "organisation_pk": organisation.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "diseases": "Congenital deficiency in alpha-fetoprotein",
            "email": "bmollison@bill.org",
            "first_name": "Bill",
            "is_primary": "1",
            "languages": "en,fr",
            "last_name": "Mollison",
            "organisation": "Bill Org",
            "phone": "+9876543210",
            "role": "CEO",
            "title": "Mr",
            "kind": "Patient Organisation",
            "groupings": "Bone,Eye",
            "created_by": person.created_by.email,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": person.modified_at.isoformat(),
        },
        {
            "organisation_pk": organisation.pk,
            "pk": organisation.pk,
            "country_label": "Belgium",
            "country": "BE",
            "diseases": "Congenital deficiency in alpha-fetoprotein",
            "email": "contact@bill.org",
            "organisation": "Bill Org",
            "kind": "Patient Organisation",
            "groupings": "Bone,Eye",
            "created_by": person.created_by.email,
            "created_at": organisation.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": organisation.modified_at.isoformat(),
        },
    ]


def test_load_email_order(person, organisation, person2, person3):
    organisation2 = Organisation(
        name="David Org",
        email="contact@david.org",
        country="FR",
        kind=60,
    )
    organisation2.roles = [
        Role(
            name="CEO",
            is_default=True,
            person=person2.pk,
            organisation=organisation2,
            email="dholmgren@david.org",
        )
    ]
    organisation2.save()
    organisation3 = Organisation(
        name="Patrick Org",
        email="contact@patrick.org",
        country="AU",
        kind=60,
    )
    organisation3.roles = [
        Role(
            name="CEO",
            is_default=True,
            person=person3.pk,
            organisation=organisation3,
        )
    ]
    organisation3.save()
    person4 = Person(
        title="mr",
        first_name="Leo",
        last_name="Blaise",
        phone="+9138650367",
        languages=["en", "fr"],
        country="FR",
        email="leoblaise@mail.com"
    ).save()
    organisation4 = Organisation(
        name="Leo Org",
        country="FR",
        kind=60,
        email="contact@leo.org"
    )
    organisation4.roles = [
        Role(
            name="CEO",
            person=person4.pk,
            organisation=organisation4,
        )
    ]
    organisation4.save()
    person5 = Person(
        title="mr",
        first_name="Firstname",
        last_name="Lastname",
        phone="+9138650367",
        languages=["en", "fr"],
        country="FR",
        email="firstname@mail.com"
    ).save()
    person6 = Person(
        title="mr",
        first_name="Firstname2",
        last_name="Lastname2",
        phone="+9138650368",
        languages=["en", "fr"],
        country="FR",
        email="firstname2@mail.com"
    ).save()
    organisation5 = Organisation(
        name="ABC Org",
        country="FR",
        kind=60,
    )
    organisation5.roles = [
        Role(
            name="CEO",
            person=person5.pk,
            organisation=organisation5,
        ),
        Role(
            name="Test Role",
            person=person6.pk,
            organisation=organisation5,
            email="firstname2@abc.org"
        )
    ]
    organisation5.save()
    organisation6 = Organisation(
        name="DEF Org",
        country="FR",
        kind=60,
    )
    organisation6.roles = [
        Role(
            name="CEO",
            person=person6.pk,
            organisation=organisation6,
            is_default=True
        )
    ]
    organisation6.save()
    assert basket.load(
        [
            {"person": person.pk},
            {"person": person2.pk},
            {"person": person3.pk},
            {"person": person4.pk},
            {"person": person5.pk},
            {"person": person6.pk}
        ]
    ) == [
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "email": "bmollison@bill.org",
            "first_name": "Bill",
            "languages": "en,fr",
            "last_name": "Mollison",
            "phone": "+9876543210",
            "title": "Mr",
            "organisation": "Bill Org",
            "kind": "Patient Organisation",
            "role": "CEO",
            "is_primary": "1",
            "organisation_pk": organisation.pk,
            "created_by": person.created_by.email,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": person.modified_at.isoformat(),
        },
        {
            "person_pk": person2.pk,
            "pk": person2.pk,
            "country_label": "Australia",
            "country": "AU",
            "email": "dholmgren@david.org",
            "first_name": "David",
            "languages": "de,en",
            "last_name": "Holmgren",
            "phone": "+9876543234",
            "title": "Mr",
            "organisation": "David Org",
            "kind": "Patient Organisation",
            "role": "CEO",
            "is_default": "1",
            "organisation_pk": organisation2.pk,
            "created_by": person2.created_by.email,
            "created_at": person2.created_at.isoformat(),
            "modified_by": person2.modified_by.email,
            "modified_at": person2.modified_at.isoformat(),
        },
        {
            "person_pk": person3.pk,
            "pk": person3.pk,
            "country_label": "France",
            "country": "FR",
            "email": "contact@patrick.org",
            "first_name": "Patrick",
            "languages": "en,fr",
            "last_name": "Morel",
            "phone": "+9198457501",
            "title": "Mr",
            "organisation": "Patrick Org",
            "kind": "Patient Organisation",
            "role": "CEO",
            "is_default": "1",
            "organisation_pk": organisation3.pk,
            "created_by": person3.created_by.email,
            "created_at": person3.created_at.isoformat(),
            "modified_by": person3.modified_by.email,
            "modified_at": person3.modified_at.isoformat(),
        },
        {
            "person_pk": person4.pk,
            "pk": person4.pk,
            "country_label": "France",
            "country": "FR",
            "email": "leoblaise@mail.com",
            "first_name": "Leo",
            "languages": "en,fr",
            "last_name": "Blaise",
            "phone": "+9138650367",
            "title": "Mr",
            "organisation": "Leo Org",
            "kind": "Patient Organisation",
            "role": "CEO",
            "organisation_pk": organisation4.pk,
            "created_by": person4.created_by.email,
            "created_at": person4.created_at.isoformat(),
            "modified_by": person4.modified_by.email,
            "modified_at": person4.modified_at.isoformat(),
        },
        {
            "person_pk": person5.pk,
            "pk": person5.pk,
            "country_label": "France",
            "country": "FR",
            "email": "firstname@mail.com",
            "first_name": "Firstname",
            "languages": "en,fr",
            "last_name": "Lastname",
            "phone": "+9138650367",
            "title": "Mr",
            "organisation": "ABC Org",
            "kind": "Patient Organisation",
            "role": "CEO",
            "organisation_pk": organisation5.pk,
            "created_by": person5.created_by.email,
            "created_at": person5.created_at.isoformat(),
            "modified_by": person5.modified_by.email,
            "modified_at": person5.modified_at.isoformat(),
        },
        {
            "person_pk": person6.pk,
            "pk": person6.pk,
            "country_label": "France",
            "country": "FR",
            "email": "firstname2@mail.com",
            "first_name": "Firstname2",
            "languages": "en,fr",
            "last_name": "Lastname2",
            "phone": "+9138650368",
            "title": "Mr",
            "organisation": "DEF Org",
            "kind": "Patient Organisation",
            "role": "CEO",
            "is_default": "1",
            "organisation_pk": organisation6.pk,
            "created_by": person6.created_by.email,
            "created_at": person6.created_at.isoformat(),
            "modified_by": person6.modified_by.email,
            "modified_at": person6.modified_at.isoformat(),
        }
    ]


def test_role_email_should_have_precedence(person, organisation):
    role = person.roles[0]
    role.email = "another@elsewhere.org"
    role.save()
    person.reload()  # Reload modified_at (saving role save person and organisation)
    assert basket.load([{"organisation": organisation.pk, "person": person.pk}]) == [
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "organisation_pk": organisation.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "email": "another@elsewhere.org",
            "first_name": "Bill",
            "is_primary": "1",
            "languages": "en,fr",
            "last_name": "Mollison",
            "organisation": "Bill Org",
            "phone": "+9876543210",
            "role": "CEO",
            "title": "Mr",
            "kind": "Patient Organisation",
            "created_by": person.created_by.email,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": person.modified_at.isoformat(),
        }
    ]


def test_email_should_fallback_on_organisation(person, organisation):
    role = person.roles[0]
    role.email = None
    role.save()
    person.email = None
    person.save()
    organisation.email = "orga@email.org"
    organisation.save()
    data = basket.load([{"organisation": organisation.pk, "person": person.pk}])
    assert data[0]["email"] == "orga@email.org"


def test_organisation_should_be_loaded_if_person_has_no_email(person, organisation):
    role = person.roles[0]
    role.email = None
    role.save()
    person.email = None
    person.save()
    organisation.email = "orga@email.org"
    organisation.save()
    assert basket.load([{"person": person.pk}]) == [
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "organisation_pk": organisation.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "email": "orga@email.org",
            "first_name": "Bill",
            "is_primary": "1",
            "languages": "en,fr",
            "last_name": "Mollison",
            "organisation": "Bill Org",
            "phone": "+9876543210",
            "role": "CEO",
            "title": "Mr",
            "kind": "Patient Organisation",
            "created_by": person.created_by.label,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.label,
            "modified_at": person.modified_at.isoformat(),
        }
    ]


def test_role_with_email_should_be_loaded_if_person_has_no_email(person, organisation):
    # Reset.
    Role.get(organisation=organisation, person=person).delete()
    person.email = None
    person.save()
    # Create a bunch of roles without emails.
    for i in range(10):
        org = Organisation(name=f"Orga{i}").save()
        Role(person=person, organisation=org).save()
    Role(
        organisation=organisation, person=person, email="we@want.it", name="Boss"
    ).save()
    # Make sure the one role we'll test is not an edge of the data.
    org = Organisation(name="One More Orga").save()
    Role(person=person, organisation=org).save()

    # We ask a person resource, not a couple, but still we expect to have the email from
    # the role.
    assert basket.load([{"person": person.pk}]) == [
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "organisation_pk": organisation.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "email": "we@want.it",
            "first_name": "Bill",
            "languages": "en,fr",
            "last_name": "Mollison",
            "organisation": "Bill Org",
            "phone": "+9876543210",
            "role": "Boss",
            "title": "Mr",
            "kind": "Patient Organisation",
            "created_by": person.created_by.label,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.label,
            "modified_at": person.modified_at.isoformat(),
        }
    ]


def test_is_default_role_should_be_loaded_if_person_has_no_email(person, organisation):
    # Reset.
    Role.get(organisation=organisation, person=person).delete()
    person.email = None
    person.save()
    # Create a bunch of roles without emails.
    for i in range(10):
        org = Organisation(name=f"Orga{i}").save()
        Role(
            person=person,
            organisation=org,
            email="we@donot.want.it",
            is_default=False,
            is_primary=False,
        ).save()
    Role(
        organisation=organisation,
        person=person,
        email="we@want.it",
        name="Boss",
        is_default=True,
    ).save()
    # Make sure the one role we'll test is not an edge of the data.
    org = Organisation(name="One More Orga").save()
    Role(person=person, organisation=org, is_primary=True, email="2d@choi.ce").save()

    assert basket.load([{"person": person.pk}])[0]["email"] == "we@want.it"

    # Now delete the is_default role and we whould get the second choice
    Role.get(organisation=organisation, person=person).delete()
    assert basket.load([{"person": person.pk}])[0]["email"] == "2d@choi.ce"


def test_orga_with_email_should_be_loaded_if_person_has_no_email(person, organisation):
    # Reset.
    Role.get(organisation=organisation, person=person).delete()
    person.email = None
    person.save()
    organisation.email = "we@want.it"
    organisation.save()
    # Create a bunch of roles without emails.
    for i in range(10):
        org = Organisation(name=f"Orga{i}").save()
        Role(person=person, organisation=org).save()
    Role(organisation=organisation, person=person, name="Boss").save()
    # Make sure the one role we'll test is not an edge of the data.
    org = Organisation(name="One More Orga").save()
    Role(person=person, organisation=org).save()

    # We ask a person resource, not a couple, but still we expect to have the email from
    # the role.
    assert basket.load([{"person": person.pk}]) == [
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "organisation_pk": organisation.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "email": "we@want.it",
            "first_name": "Bill",
            "languages": "en,fr",
            "last_name": "Mollison",
            "organisation": "Bill Org",
            "phone": "+9876543210",
            "role": "Boss",
            "title": "Mr",
            "kind": "Patient Organisation",
            "created_by": person.created_by.label,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.label,
            "modified_at": person.modified_at.isoformat(),
        }
    ]


def test_primary_person_should_be_loaded_if_organisation_has_no_email(person, person2, organisation):
    role = organisation.roles[0]
    role.email = None
    role.save()
    person.email = "fall@back.org"
    person.save()
    person.reload()
    role2 = Role(
        person=person2.pk,
        organisation=organisation,
        email="person2@bill.org",
        name="Test Role"
    ).save()
    organisation.email = None
    organisation.save()
    assert basket.load([{"organisation": organisation.pk}]) == [
        {
            "email": "fall@back.org",
            "phone": "+9876543210",
            "cell_phone": "+611111111",
            "country": "BE",
            "title": "Mr",
            "first_name": "Bill",
            "last_name": "Mollison",
            "languages": "en,fr",
            "pk": organisation.pk,
            "person_pk": person.pk,
            "role": "CEO",
            "kind": "Patient Organisation",
            "organisation": "Bill Org",
            "organisation_pk": organisation.pk,
            "is_primary": "1",
            "country_label": "Belgium",
            "created_by": person.created_by.email,
            "created_at": organisation.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": organisation.modified_at.isoformat(),
        }
    ]


def test_person_should_be_loaded_if_organisation_has_no_email(person, person2, organisation):
    role = organisation.roles[0]
    role.email = None
    role.save()
    person.email = None
    person.save()
    person.reload()
    role2 = Role(
        person=person2.pk,
        organisation=organisation,
        email="person2@bill.org",
        name="Test Role"
    ).save()
    person2.reload()
    organisation.email = None
    organisation.save()
    assert basket.load([{"organisation": organisation.pk}]) == [
        {
            "email": "person2@bill.org",
            "phone": "+9876543234",
            "country": "BE",
            "title": "Mr",
            "first_name": "David",
            "last_name": "Holmgren",
            "languages": "de,en",
            "pk": organisation.pk,
            "person_pk": person2.pk,
            "role": "Test Role",
            "kind": "Patient Organisation",
            "organisation": "Bill Org",
            "organisation_pk": organisation.pk,
            "country_label": "Belgium",
            "created_by": person2.created_by.name,
            "created_at": organisation.created_at.isoformat(),
            "modified_by": person2.modified_by.name,
            "modified_at": organisation.modified_at.isoformat(),
        }
    ]


def test_is_primary_role_should_have_precedence(person, organisation, person2):
    Role.objects.delete()
    person.email = "fall@back.org"
    person.save()
    person2.email = "fall2@back.org"
    person2.save()
    organisation.email = None
    organisation.save()
    Role(person=person.pk, organisation=organisation).save()
    Role(person=person2.pk, organisation=organisation, is_primary=True).save()
    data = basket.load([{"organisation": organisation.pk}])
    assert data[0]["person_pk"] == person2.pk


def test_load_ignores_unknown_id(person, organisation, person2):
    assert (
        basket.load(
            [
                {"person": "5bfd1c61b9b0f15d733d9f40"},
                {"organisation": "5bfd1c61b9b0f15d733d9f40", "person": person.pk},
            ]
        )
        == []
    )


def test_load_organisation_without_kind(person, organisation):
    organisation.kind = None
    organisation.save()
    assert basket.load(
        [
            {"organisation": organisation.pk},
            {"organisation": organisation.pk, "person": person.pk},
        ]
    ) == [
        {
            "country_label": "Belgium",
            "country": "BE",
            "email": "contact@bill.org",
            "organisation": "Bill Org",
            "organisation_pk": organisation.pk,
            "pk": organisation.pk,
            "created_by": person.created_by.email,
            "created_at": organisation.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": organisation.modified_at.isoformat(),
        },
        {
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "email": "bmollison@bill.org",
            "first_name": "Bill",
            "is_primary": "1",
            "languages": "en,fr",
            "last_name": "Mollison",
            "organisation": "Bill Org",
            "organisation_pk": organisation.pk,
            "person_pk": person.pk,
            "pk": person.pk,
            "phone": "+9876543210",
            "role": "CEO",
            "title": "Mr",
            "created_by": person.created_by.email,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": person.modified_at.isoformat(),
        },
    ]


def test_load_organisation_website(person, organisation):
    organisation.website = "https://web.site"
    organisation.save()
    data = basket.load([{"organisation": organisation.pk}])
    assert data[0]["website"] == "https://web.site"


def test_load_organisation_acronym(person, organisation):
    organisation.acronym = "ACRO"
    organisation.save()
    data = basket.load([{"organisation": organisation.pk}])
    assert data[0]["acronym"] == "ACRO"


def test_should_not_fail_on_unconsistant_person_data(person):
    person.languages = []
    person.save()
    basket.load([{"person": person.pk}])
    person.languages = None
    person.save()
    basket.load([{"person": person.pk}])
    person.replace({"last_name": "foo", "languages": None})
    basket.load([{"person": person.pk}])
    person.replace({"last_name": "foo"})
    basket.load([{"person": person.pk}])


def test_should_not_fail_on_unconsistant_organisation_data(organisation):
    organisation.groupings = []
    organisation.save()
    basket.load([{"organisation": organisation.pk}])
    organisation.groupings = None
    organisation.save()
    basket.load([{"organisation": organisation.pk}])
    organisation.replace({"name": "foo", "groupings": None})
    basket.load([{"organisation": organisation.pk}])
    organisation.replace({"name": "foo"})
    basket.load([{"organisation": organisation.pk}])


def test_with_organisation_disease(person, organisation, disease):
    organisation.diseases.append(disease)
    organisation.save()
    assert basket.load(
        [
            {"person": person.pk},
            {"organisation": organisation.pk, "person": person.pk},
            {"organisation": organisation.pk},
        ]
    ) == [
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "email": "bmollison@bill.org",
            "first_name": "Bill",
            "languages": "en,fr",
            "last_name": "Mollison",
            "phone": "+9876543210",
            "title": "Mr",
            "organisation": "Bill Org",
            "diseases": "Congenital deficiency in alpha-fetoprotein",
            "groupings": "Bone,Eye",
            "kind": "Patient Organisation",
            "role": "CEO",
            "is_primary": "1",
            "organisation_pk": organisation.pk,
            "created_by": person.created_by.email,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": person.modified_at.isoformat(),
        },
        {
            "person_pk": person.pk,
            "pk": person.pk,
            "organisation_pk": organisation.pk,
            "cell_phone": "+611111111",
            "country_label": "France",
            "country": "FR",
            "email": "bmollison@bill.org",
            "first_name": "Bill",
            "is_primary": "1",
            "languages": "en,fr",
            "last_name": "Mollison",
            "organisation": "Bill Org",
            "phone": "+9876543210",
            "role": "CEO",
            "title": "Mr",
            "diseases": "Congenital deficiency in alpha-fetoprotein",
            "groupings": "Bone,Eye",
            "kind": "Patient Organisation",
            "created_by": person.created_by.email,
            "created_at": person.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": person.modified_at.isoformat(),
        },
        {
            "organisation_pk": organisation.pk,
            "country_label": "Belgium",
            "country": "BE",
            "email": "contact@bill.org",
            "organisation": "Bill Org",
            "diseases": "Congenital deficiency in alpha-fetoprotein",
            "groupings": "Bone,Eye",
            "kind": "Patient Organisation",
            "pk": organisation.pk,
            "created_by": person.created_by.email,
            "created_at": organisation.created_at.isoformat(),
            "modified_by": person.modified_by.email,
            "modified_at": organisation.modified_at.isoformat(),
        },
    ]


def test_with_rdi_membership(person, organisation, rdi_org):
    organisation.membership.append(Membership(organisation=rdi_org, status="f"))
    organisation.save()
    data = basket.load(
        [
            {"person": person.pk},
            {"organisation": organisation.pk, "person": person.pk},
            {"organisation": organisation.pk},
        ]
    )
    print(data[0])
    assert data[0]["rdi_membership"] == "Full"
    assert data[1]["rdi_membership"] == "Full"
    assert data[2]["rdi_membership"] == "Full"


def test_with_eurordis_membership(person, organisation, eurordis_org):
    organisation.membership.append(Membership(organisation=eurordis_org, status="f"))
    organisation.save()
    data = basket.load(
        [
            {"person": person.pk},
            {"organisation": organisation.pk, "person": person.pk},
            {"organisation": organisation.pk},
        ]
    )
    assert data[0]["eurordis_membership"] == "Full"
    assert data[1]["eurordis_membership"] == "Full"
    assert data[2]["eurordis_membership"] == "Full"


def test_groups_mass_edit(person, person2, group, organisation):
    assert basket.mass_edit(
        [
            {"person": person.pk, "organisation": organisation.pk},
            {"person": person2.pk, "organisation": organisation.pk},
        ],
        "groups",
        "push",
        {"group": group.pk, "role": "Member"},
    ) == [
        {"raw": {"person": "1", "organisation": "3"}, "status": "success"},
        {
            "raw": {"person": "2", "organisation": "3"},
            "status": "error",
            "error": "Role does not exist",
        },
    ]
    assert Role.get(person=person.pk, organisation=organisation.pk).groups == [
        GroupBelonging(group=group, role="Member")
    ]


def test_groups_mass_edit_does_not_edit_contacts(person, group, organisation):
    assert basket.mass_edit(
        [{"person": person.pk}, {"organisation": organisation.pk}],
        "groups",
        "push",
        {"group": group.pk, "role": "Member"},
    ) == [
        {
            "raw": {"person": "1"},
            "status": "error",
            "error": "Can only apply groups to roles",
        },
        {
            "raw": {"organisation": "2"},
            "status": "error",
            "error": "Can only apply groups to roles",
        },
    ]


def test_groups_mass_edit_does_not_duplicate(person, group, organisation):
    person.roles[0].add_group(group=group, role="Member").save()
    assert basket.mass_edit(
        [{"person": person.pk, "organisation": organisation.pk}],
        "groups",
        "push",
        {"group": group.pk, "role": "Member"},
    ) == [
        {"raw": {"person": "1", "organisation": organisation.pk}, "status": "success"}
    ]
    assert Role.get(organisation.pk, person.pk).groups == [
        GroupBelonging(group=group, role="Member")
    ]


def test_groups_mass_edit_with_unknown_group(person, organisation):
    assert basket.mass_edit(
        [{"person": person.pk, "organisation": organisation.pk}],
        "groups",
        "push",
        {"group": "123", "role": "Member"},
    ) == [
        {
            "raw": {"person": "1", "organisation": "2"},
            "status": "error",
            "error": "Invalid value: {'group': '123', 'role': 'Member'}",
        }
    ]
    assert Person.get(person.pk).groups == []


def test_groups_mass_edit_with_unknown_group_role(person, group, organisation):
    assert basket.mass_edit(
        [{"person": person.pk, "organisation": organisation.pk}],
        "groups",
        "push",
        {"group": group.pk, "role": "Unknown"},
    ) == [
        {
            "raw": {"person": "1", "organisation": "2"},
            "status": "error",
            "error": f"Invalid value: {{'group': '{group.pk}', 'role': 'Unknown'}}",
        }
    ]
    assert Person.get(person.pk).groups == []


def test_groups_mass_edit_with_unknown_operator(person, group, organisation):
    assert basket.mass_edit(
        [{"person": person.pk, "organisation": organisation.pk}],
        "groups",
        "append",
        {"group": group.pk, "role": "Member"},
    ) == [
        {
            "raw": {"person": "1", "organisation": "2"},
            "error": "Unsupported operator append",
            "status": "error",
        }
    ]
    assert Person.get(person.pk).groups == []


def test_groups_mass_edit_with_unknown_field(person, group):
    assert basket.mass_edit(
        [{"person": person.pk}],
        "unknown",
        "push",
        {"group": group.pk, "role": "Member"},
    ) == [{"raw": {"person": "1"}, "error": "Invalid field unknown", "status": "error"}]


def test_groups_mass_edit_with_unknown_resource(person, group, organisation):
    assert basket.mass_edit(
        [{"person": "456", "organisation": organisation.pk}],
        "groups",
        "push",
        {"group": group.pk, "role": "Member"},
    ) == [
        {
            "raw": {"person": "456", "organisation": organisation.pk},
            "error": "Role does not exist",
            "status": "error",
        }
    ]


def test_from_refs_with_pk(person):
    assert basket.from_refs([person.pk]) == {
        "not_found": set(),
        "resources": [{"person": person.pk}],
    }


def test_from_refs_with_email(person):
    # Mase sure search is case insensitive.
    person.email = "StrANGE@cAse.org"
    person.save()
    assert basket.from_refs(["STRange@case.ORG"]) == {
        "not_found": set(),
        "resources": [{"person": person.pk}],
    }


def test_from_refs_with_email_in_role(person, organisation):
    # Mase sure search is case insensitive.
    Role.get(organisation=organisation, person=person).delete()
    Role(organisation=organisation, person=person, email="StrANGE@cAse.org").save()
    assert basket.from_refs(["STRange@case.ORG"]) == {
        "not_found": set(),
        "resources": [{"person": person.pk, "organisation": organisation.pk}],
    }


def test_from_refs_with_person_resource_and_organisation_pk(organisation):
    assert basket.from_refs([organisation.pk]) == {
        "not_found": {organisation.pk},
        "resources": [],
    }


def test_from_refs_with_organisation_unknown():
    assert basket.from_refs(["pouet"]) == {"not_found": {"pouet"}, "resources": []}


def test_from_refs_with_organisation_resource(organisation):
    assert basket.from_refs([organisation.pk], resource=Organisation) == {
        "not_found": set(),
        "resources": [{"organisation": organisation.pk}],
    }


def test_from_refs_should_load_default_role(person, organisation):
    role = Role.get(organisation=organisation, person=person)
    role.is_default = True
    role.save()
    assert basket.from_refs([person.pk]) == {
        "not_found": set(),
        "resources": [{"person": person.pk, "organisation": organisation.pk}],
    }


def test_groups_mass_pull_keywords(person, person2, keyword):
    keyword2 = Keyword(name="foobar").save()
    person.keywords = [keyword, keyword2]
    person.save()
    person2.keywords = [keyword, keyword2]
    person2.save()
    assert basket.mass_edit(
        [{"person": person.pk}, {"person": person2.pk}], "keywords", "pull", keyword.pk,
    ) == [
        {"raw": {"person": "1"}, "status": "success"},
        {"raw": {"person": "2"}, "status": "success"},
    ]
    assert Person.get(person.pk).keywords == [keyword2]
    assert Person.get(person2.pk).keywords == [keyword2]


def test_groups_mass_pull_diseases(person, person2, disease):
    disease2 = Disease(name="foobar", pk=654321).save()
    person.diseases = [disease, disease2]
    person.save()
    person2.diseases = [disease, disease2]
    person2.save()
    assert basket.mass_edit(
        [{"person": person.pk}, {"person": person2.pk}], "diseases", "pull", disease.pk,
    ) == [
        {"raw": {"person": "1"}, "status": "success"},
        {"raw": {"person": "2"}, "status": "success"},
    ]
    assert Person.get(person.pk).diseases == [disease2]
    assert Person.get(person2.pk).diseases == [disease2]


def test_groups_mass_pull_groups(person, person2, group, organisation):
    group2 = Group(name="foobar").save()
    person.roles[0].add_group(group=group, role="Member").add_group(
        group=group2, role="Member"
    ).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[
                GroupBelonging(group=group, role="Chair"),
                GroupBelonging(group=group2, role="Director"),
            ],
        ),
    ]
    person2.save()
    assert basket.mass_edit(
        [
            {"person": person.pk, "organisation": organisation.pk},
            {"person": person2.pk, "organisation": organisation.pk},
        ],
        "groups",
        "pull",
        {"group": group.pk},
    ) == [
        {"raw": {"person": "1", "organisation": "3"}, "status": "success"},
        {"raw": {"person": "2", "organisation": "3"}, "status": "success"},
    ]
    assert Role.get(person=person.pk, organisation=organisation.pk).groups == [
        GroupBelonging(group=group2, role="Member")
    ]
    assert Role.get(person=person2.pk, organisation=organisation.pk).groups == [
        GroupBelonging(group=group2, role="Director")
    ]


def test_groups_mass_pull_languages(person, person2, group):
    person.languages = ["es", "it", "ru"]
    person.save()
    person2.languages = ["es", "de"]
    person2.save()
    assert basket.mass_edit(
        [{"person": person.pk}, {"person": person2.pk}], "languages", "pull", "es"
    ) == [
        {"raw": {"person": "1"}, "status": "success"},
        {"raw": {"person": "2"}, "status": "success"},
    ]
    assert Person.get(person.pk).languages == ["it", "ru"]
    assert Person.get(person2.pk).languages == ["de"]


def test_groups_mass_set_kind(organisation):
    organisation.kind = None
    organisation.save()
    assert basket.mass_edit([{"organisation": organisation.pk}], "kind", "set", 60) == [
        {"raw": {"organisation": organisation.pk}, "status": "success"},
    ]
    assert Organisation.get(organisation.pk).kind == 60


def test_groups_mass_unset_kind(organisation):
    organisation.kind = 60
    organisation.save()
    assert basket.mass_edit(
        [{"organisation": organisation.pk}], "kind", "set", None
    ) == [{"raw": {"organisation": organisation.pk}, "status": "success"}]
    assert not Organisation.get(organisation.pk).kind
