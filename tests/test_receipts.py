from datetime import date, datetime
from pathlib import Path

import pytest

from eurordis import receipts
from eurordis.models import Fee, Membership, Organisation, Role

FAKE_NOW = datetime(2020, 12, 25, 17, 5, 55)


def test_fee_receipt(person, eurordis_org, monkeypatch):
    monkeypatch.setattr("eurordis.receipts.PDF.sent_date", lambda s: date(2021, 3, 3))
    member = Organisation(
        name="Active membership",
        membership=[
            Membership(
                organisation=eurordis_org,
                start_date="2018-01-01",
                fees=[Fee(year=2018, amount=50, payment_date=date(2018, 2, 13))],
                status="f",
            )
        ],
    ).save()
    role = Role(person=person, organisation=member).save()
    pdf = receipts.fee(role, 2018)
    pdf.set_creation_date(FAKE_NOW)
    out = Path(__file__).parent / "data/fee.pdf"
    # out.write_bytes(pdf().read())
    assert out.read_bytes() == bytes(pdf().read())


def test_fee_receipt_with_bad_year(person, eurordis_org):
    member = Organisation(
        name="Active membership",
        membership=[
            Membership(
                organisation=eurordis_org,
                start_date="2018-01-01",
                fees=[Fee(year=2018, amount=50, payment_date=date(2018, 2, 13))],
                status="f",
            )
        ],
    ).save()
    role = Role(person=person, organisation=member).save()
    with pytest.raises(ValueError):
        receipts.fee(role, 2016)


def test_fee_receipt_with_non_member_org(person, eurordis_org):
    member = Organisation(name="Not a member").save()
    role = Role(person=person, organisation=member).save()
    with pytest.raises(ValueError):
        receipts.fee(role, 2016)
