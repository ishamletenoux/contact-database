import json
from datetime import timedelta

import pytest
from eurordis import config, helpers, mailchimp
from eurordis.models import Membership, Organisation, Person, RoleToEvent, Role, Keyword

pytestmark = pytest.mark.asyncio


@pytest.fixture(scope="function", autouse=True)
def patch_config():
    config.MAILCHIMP_LIST_EURORDIS_MEMBERS = ""
    config.MAILCHIMP_LIST_RDI_MEMBERS = ""
    config.MAILCHIMP_LIST_TEST = ""
    config.MAILCHIMP_LIST_ALL = ""
    config.MAILCHIMP_LIST_ALUMNI = ""


async def test_sync_eurordis_members(mock_http, person, person2, organisation, eurordis_org):
    config.MAILCHIMP_LIST_EURORDIS_MEMBERS = "foo"
    organisation.membership.append(Membership(organisation=eurordis_org, status="f"))
    organisation.save()
    Role(
        is_primary=True,
        person=person2.pk,
        organisation=organisation,
        email="david@org.org",
    ).save()
    Organisation.objects(pk=organisation.pk).update(
        modified_at=helpers.utcnow() - timedelta(hours=3)  # Skip this entry.
    )
    Person.objects(pk=person2.pk).update(
        modified_at=helpers.utcnow() - timedelta(hours=3)  # Skip this entry.
    )
    calls = mock_http(content={"total_items": 8})  # Get total items
    mock_http(
        content={
            "members": [
                {"email_address": "david@org.org", "id": "123", "status": "subscribed"},
                {"email_address": "remove@me.me", "id": "124", "status": "subscribed"},
                {"email_address": "keep@me.me", "id": "125", "status": "unsubscribed"},
            ]
        }
    )  # Get members.
    await mailchimp.sync()
    assert len(calls) == 4
    delete = calls[2]["kwargs"]["json"]["operations"]
    assert delete == [{"method": "DELETE", "path": "lists/foo/members/124"}]
    members = json.loads(calls[3]["kwargs"]["json"]["operations"][0]["body"])
    assert members == {
        "members": [
            {
                "email_address": "bmollison@bill.org",
                "merge_fields": {
                    "PK": person.pk,
                    "FIRSTLANG": "en",
                    "TITLE": "Mr",
                    "FIRSTNAME": "Bill",
                    "LASTNAME": "Mollison",
                    "MEMBSTATUS": "Full",
                    "ORGCOUNTRY": "BE",
                    "ORGNAME": "Bill Org",
                    "ORGID": "3",
                    "SECONDLANG": "fr",
                    "CEF": "no",
                    "CNA": "no",
                },
                "status": "subscribed",
            }
        ],
        "update_existing": True,
    }


async def test_sync_rdi_members(mock_http, person, person2, organisation, rdi_org):
    config.MAILCHIMP_LIST_RDI_MEMBERS = "rdiabc"
    organisation.membership.append(Membership(organisation=rdi_org, status="f"))
    organisation.save()
    Role(
        is_primary=True,
        person=person2.pk,
        organisation=organisation,
        email="david@org.org",
    ).save()
    Organisation.objects(pk=organisation.pk).update(
        modified_at=helpers.utcnow() - timedelta(hours=3)  # Skip this entry.
    )
    Person.objects(pk=person2.pk).update(
        modified_at=helpers.utcnow() - timedelta(hours=3)  # Skip this entry.
    )
    calls = mock_http(content={"total_items": 8})  # Get total items
    mock_http(
        content={
            "members": [
                {"email_address": "david@org.org", "id": "123", "status": "subscribed"},
                {"email_address": "remove@me.me", "id": "124", "status": "subscribed"},
                {"email_address": "keep@me.me", "id": "125", "status": "unsubscribed"},
            ]
        }
    )  # Get members.
    await mailchimp.sync()
    assert len(calls) == 4
    delete = calls[2]["kwargs"]["json"]["operations"]
    assert delete == [{"method": "DELETE", "path": "lists/rdiabc/members/124"}]
    members = json.loads(calls[3]["kwargs"]["json"]["operations"][0]["body"])
    assert members == {
        "members": [
            {
                "email_address": "bmollison@bill.org",
                "merge_fields": {
                    "PK": person.pk,
                    "FIRSTLANG": "en",
                    "TITLE": "Mr",
                    "FIRSTNAME": "Bill",
                    "LASTNAME": "Mollison",
                    "MEMBSTATUS": "Full",
                    "ORGCOUNTRY": "BE",
                    "ORGNAME": "Bill Org",
                    "ORGID": "3",
                    "SECONDLANG": "fr",
                    "CEF": "no",
                    "CNA": "no",
                },
                "status": "subscribed",
            }
        ],
        "update_existing": True,
    }


async def test_sync_is_chunked(
    mock_http, person, person2, organisation, monkeypatch, eurordis_org
):
    config.MAILCHIMP_LIST_EURORDIS_MEMBERS = "foo"
    organisation.membership.append(Membership(organisation=eurordis_org, status="f"))
    organisation.save()
    monkeypatch.setattr(mailchimp, "CHUNKS", 2)
    Role(
        is_primary=True,
        person=person2.pk,
        organisation=organisation,
        email="david@org.org",
    ).save()
    person3 = Person(
        title="mr",
        first_name="Alan",
        last_name="Chadwick",
        email="alan@somewhere.org",
        phone="+9876543234",
        languages=["en", "de"],
        country="GB",
    ).save()
    Role(is_primary=True, person=person3.pk, organisation=organisation).save()
    calls = mock_http(content={"total_items": 0})  # Get total items
    mock_http(content={"members": []})  # Get members.
    await mailchimp.sync()
    assert len(calls) == 2
    members = json.loads(calls[1]["kwargs"]["json"]["operations"][0]["body"])
    assert members == {
        "members": [
            {
                "email_address": "bmollison@bill.org",
                "merge_fields": {
                    "PK": person.pk,
                    "FIRSTLANG": "en",
                    "TITLE": "Mr",
                    "FIRSTNAME": "Bill",
                    "LASTNAME": "Mollison",
                    "MEMBSTATUS": "Full",
                    "ORGCOUNTRY": "BE",
                    "ORGNAME": "Bill Org",
                    "ORGID": "3",
                    "CEF": "no",
                    "CNA": "no",
                    "SECONDLANG": "fr",
                },
                "status": "subscribed",
            },
            {
                "email_address": "david@org.org",
                "merge_fields": {
                    "PK": person2.pk,
                    "FIRSTLANG": "en",
                    "TITLE": "Mr",
                    "FIRSTNAME": "David",
                    "LASTNAME": "Holmgren",
                    "MEMBSTATUS": "Full",
                    "ORGCOUNTRY": "BE",
                    "ORGNAME": "Bill Org",
                    "ORGID": "3",
                    "CEF": "no",
                    "CNA": "no",
                    "SECONDLANG": "de",
                },
                "status": "subscribed",
            },
        ],
        "update_existing": True,
    }
    body = json.loads(calls[1]["kwargs"]["json"]["operations"][1]["body"])
    assert body == {
        "members": [
            {
                "email_address": "alan@somewhere.org",
                "status": "subscribed",
                "merge_fields": {
                    "PK": person3.pk,
                    "FIRSTLANG": "en",
                    "TITLE": "Mr",
                    "FIRSTNAME": "Alan",
                    "LASTNAME": "Chadwick",
                    "ORGNAME": "Bill Org",
                    "ORGID": "3",
                    "MEMBSTATUS": "Full",
                    "ORGCOUNTRY": "BE",
                    "CEF": "no",
                    "CNA": "no",
                    "SECONDLANG": "de",
                },
            }
        ],
        "update_existing": True,
    }


async def test_sync_recently_changed(mock_http, person, organisation, eurordis_org):
    config.MAILCHIMP_LIST_EURORDIS_MEMBERS = "foo"
    organisation.membership.append(Membership(organisation=eurordis_org, status="f"))
    organisation.save()
    calls = mock_http(content={"total_items": 8})  # Get total items
    mock_http(
        content={
            "members": [
                {
                    "email_address": "bmollison@bill.org",
                    "id": "123",
                    "status": "unsubscribed",
                }
            ]
        }
    )  # Get members.
    await mailchimp.sync()
    assert len(calls) == 3
    body = json.loads(calls[2]["kwargs"]["json"]["operations"][0]["body"])
    assert body == {
        "members": [
            {
                "email_address": "bmollison@bill.org",
                "merge_fields": {
                    "PK": person.pk,
                    "FIRSTLANG": "en",
                    "TITLE": "Mr",
                    "FIRSTNAME": "Bill",
                    "LASTNAME": "Mollison",
                    "MEMBSTATUS": "Full",
                    "ORGCOUNTRY": "BE",
                    "ORGNAME": "Bill Org",
                    "ORGID": "2",
                    "CEF": "no",
                    "CNA": "no",
                    "SECONDLANG": "fr",
                },
                "status": "unsubscribed",
            }
        ],
        "update_existing": True,
    }


async def test_sync_org_recently_changed(mock_http, person, organisation, eurordis_org):
    config.MAILCHIMP_LIST_EURORDIS_MEMBERS = "foo"
    organisation.membership.append(Membership(organisation=eurordis_org, status="f"))
    organisation.save()
    # Person has not been modified in the last 2 hours, but organisation has.
    Person.objects(pk=person.pk).update(
        modified_at=helpers.utcnow() - timedelta(hours=3)
    )
    calls = mock_http(content={"total_items": 8})  # Get total items
    mock_http(
        content={
            "members": [
                {
                    "email_address": "bmollison@bill.org",
                    "id": "123",
                    "status": "unsubscribed",
                }
            ]
        }
    )  # Get members.
    await mailchimp.sync()
    assert len(calls) == 3
    body = json.loads(calls[2]["kwargs"]["json"]["operations"][0]["body"])
    assert body == {
        "members": [
            {
                "email_address": "bmollison@bill.org",
                "merge_fields": {
                    "PK": person.pk,
                    "FIRSTLANG": "en",
                    "TITLE": "Mr",
                    "FIRSTNAME": "Bill",
                    "LASTNAME": "Mollison",
                    "MEMBSTATUS": "Full",
                    "ORGCOUNTRY": "BE",
                    "ORGNAME": "Bill Org",
                    "ORGID": "2",
                    "CEF": "no",
                    "CNA": "no",
                    "SECONDLANG": "fr",
                },
                "status": "unsubscribed",
            }
        ],
        "update_existing": True,
    }


async def test_sync_test(mock_http, person, person2, person3):
    config.MAILCHIMP_LIST_TEST = "testabc"
    test_keyword = Keyword(name="test", for_person=True).save()
    config.MAILCHIMP_TEST_KEYWORD = test_keyword.pk
    person.keywords.append(test_keyword)
    person.save()
    person2.keywords.append(test_keyword)
    person2.save()
    calls = mock_http(content={"total_items": 8})  # Get total items
    mock_http(
        content={
            "members": []
        }
    )  # Get members.
    await mailchimp.sync()
    assert len(calls) == 3
    members = json.loads(calls[2]["kwargs"]["json"]["operations"][0]["body"])
    assert members == {
        "members": [
            {
                "email_address": "bill@somewhere.org",
                "merge_fields": {
                    "PK": person.pk,
                    "FIRSTNAME": "Bill",
                    "LASTNAME": "Mollison",
                    "COUNTRY": "FR",
                    "FIRSTLANG": "en",
                    "SECONDLANG": "fr"
                },
                "status": "subscribed",
            },
            {
                "email_address": "david@somewhere.org",
                "merge_fields": {
                    "PK": person2.pk,
                    "FIRSTNAME": "David",
                    "LASTNAME": "Holmgren",
                    "COUNTRY": "AU",
                    "FIRSTLANG": "en",
                    "SECONDLANG": "de"
                },
                "status": "subscribed",
            }
        ],
        "update_existing": True,
    }


async def test_sync_all(mock_http, person, organisation):
    config.MAILCHIMP_LIST_ALL = "bazbar"
    calls = mock_http(content={"total_items": 0})
    mock_http(content={"members": []})
    await mailchimp.sync()
    assert len(calls) == 2
    members = json.loads(calls[1]["kwargs"]["json"]["operations"][0]["body"])
    assert members == {
        "members": [
            {
                "email_address": "contact@bill.org",
                "merge_fields": {
                    "COUNTRY": "BE",
                    "LANG": "en",
                    "NAME": "Bill Org",
                    "FIRSTNAME": "",
                    "TYPE": "organisation",
                },
                "status": "subscribed",
            },
            {
                "email_address": "bill@somewhere.org",
                "merge_fields": {
                    "COUNTRY": "FR",
                    "LANG": "en",
                    "NAME": "Bill Mollison",
                    "FIRSTNAME": "Bill",
                    "TYPE": "person",
                },
                "status": "subscribed",
            },
            {
                "email_address": "bmollison@bill.org",
                "merge_fields": {
                    "COUNTRY": "FR",
                    "LANG": "en",
                    "NAME": "Bill Mollison",
                    "FIRSTNAME": "Bill",
                    "TYPE": "person",
                },
                "status": "subscribed",
            },
        ],
        "update_existing": True,
    }


async def test_sync_multi_org_person(mock_http, person, eurordis_org):
    config.MAILCHIMP_LIST_EURORDIS_MEMBERS = "foo"
    non_member_org = Organisation(
        name="Member",
        membership=[Membership(organisation=eurordis_org, status="f")],
        kind=60,
    ).save()
    member_org = Organisation(
        name="Member",
        membership=[Membership(organisation=eurordis_org, status="f")],
        kind=60,
    ).save()
    Role(person=person.pk, organisation=non_member_org).save()
    Role(person=person.pk, organisation=member_org).save()
    calls = mock_http(content={"total_items": 0})
    mock_http(content={"members": []})
    await mailchimp.sync()
    assert len(calls) == 2
    members = json.loads(calls[1]["kwargs"]["json"]["operations"][0]["body"])
    assert members == {
        "members": [
            {
                "email_address": "bill@somewhere.org",
                "merge_fields": {
                    "PK": person.pk,
                    "FIRSTLANG": "en",
                    "TITLE": "Mr",
                    "FIRSTNAME": "Bill",
                    "LASTNAME": "Mollison",
                    "MEMBSTATUS": "Full",
                    "ORGCOUNTRY": '',
                    "ORGNAME": "Member",
                    "ORGID": "3",
                    "CEF": "no",
                    "CNA": "no",
                    "SECONDLANG": "fr",
                },
                "status": "subscribed",
            }
        ],
        "update_existing": True,
    }


async def test_sync_alumni(mock_http, role, role2, event):
    config.MAILCHIMP_LIST_ALUMNI = "bazbar"
    event.kind = "summer_school"
    event.save()
    # Case insensitive.
    role.events.append(RoleToEvent(event=event, role="Participant"))
    role.save()
    role2.events.append(RoleToEvent(event=event, role="Speaker"))
    role2.save()
    calls = mock_http(content={"total_items": 0})
    mock_http(content={"members": []})
    await mailchimp.sync()
    assert len(calls) == 2
    members = json.loads(calls[1]["kwargs"]["json"]["operations"][0]["body"])
    assert members == {
        "members": [
            {
                "email_address": "bmollison@bill.org",
                "merge_fields": {
                    "COUNTRY": "FR",
                    "LANG": "en",
                    "NAME": "Bill Mollison",
                    "FIRSTNAME": "Bill",
                },
                "status": "subscribed",
            },
        ],
        "update_existing": True,
    }


async def test_hard_bounce(mock_http, person2, organisation, keyword):
    config.HARDBOUNCE_KEYWORD = keyword.pk
    config.MAILCHIMP_LIST_EURORDIS_MEMBERS = "xxxyyy"
    Role(person=person2, organisation=organisation, email="bounce@mail.net").save()
    person2.comment = "A comment."
    person2.save()
    calls = mock_http(content={"total_items": 2})
    mock_http(
        content={
            "members": [
                {"email_address": "bounce@mail.net", "last_changed": "2019-12-01"},
                {"email_address": organisation.email, "last_changed": "2020-01-03"},
            ]
        }
    )
    await mailchimp.hard_bounce()
    assert len(calls) == 2
    person2.reload()
    organisation.reload()
    assert keyword in person2.keywords
    assert (
        person2.comment
        == 'A comment.\nHard bounce on mailing list "eurordis_members" on 2019-12-01: bounce@mail.net'
    )
    assert keyword in organisation.keywords
    assert organisation.email in organisation.comment


def test_get_eurordis_members(person, eurordis_org):
    member_org = Organisation(
        name="Member",
        membership=[Membership(organisation=eurordis_org, status="f")],
        kind=60,
    ).save()
    Role(person=person.pk, organisation=member_org).save()
    contacts = mailchimp.get_eurordis_members()
    assert contacts == {'bill@somewhere.org': {
        'CEF': 'no',
        'CNA': 'no',
        'FIRSTLANG': 'en',
        'SECONDLANG': 'fr',
        'FIRSTNAME': 'Bill',
        'LASTNAME': 'Mollison',
        'MEMBSTATUS': 'Full',
        'ORGNAME': 'Member',
        'ORGCOUNTRY': '',
        'ORGID': member_org.pk,
        'TITLE': 'Mr',
        'PK': person.pk,
        'modified_at': max(person.modified_at, member_org.modified_at),
    }}


def test_get_rdi_members(person, rdi_org):
    member_org = Organisation(
        name="Member",
        membership=[Membership(organisation=rdi_org, status="f")],
        kind=60,
    ).save()
    Role(person=person.pk, organisation=member_org).save()
    contacts = mailchimp.get_rdi_members()
    assert contacts == {'bill@somewhere.org': {
        'CEF': 'no',
        'CNA': 'no',
        'FIRSTLANG': 'en',
        'SECONDLANG': 'fr',
        'FIRSTNAME': 'Bill',
        'LASTNAME': 'Mollison',
        'MEMBSTATUS': 'Full',
        'ORGNAME': 'Member',
        'ORGCOUNTRY': '',
        'ORGID': member_org.pk,
        'TITLE': 'Mr',
        'PK': person.pk,
        'modified_at': max(person.modified_at, member_org.modified_at),
    }}


def test_get_test(person, person2, person3):
    test_keyword = Keyword(name="test", for_person=True).save()
    config.MAILCHIMP_TEST_KEYWORD = test_keyword.pk
    person.keywords.append(test_keyword)
    person.save()
    person2.keywords.append(test_keyword)
    person2.save()
    contacts = mailchimp.get_test()
    assert contacts == {
        "bill@somewhere.org": {
            "PK": person.pk,
            "FIRSTNAME": "Bill",
            "LASTNAME": "Mollison",
            "COUNTRY": "FR",
            "FIRSTLANG": "en",
            "SECONDLANG": "fr",
            "modified_at": person.modified_at,
        },
        "david@somewhere.org": {
            "PK": person2.pk,
            "FIRSTNAME": "David",
            "LASTNAME": "Holmgren",
            "COUNTRY": "AU",
            "FIRSTLANG": "en",
            "SECONDLANG": "de",
            "modified_at": person2.modified_at
        }
    }
