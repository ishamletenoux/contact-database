from datetime import datetime, timedelta

import pytest
import ujson as json
from eurordis import helpers, session
from eurordis.models import (
    Disease,
    Event,
    Fee,
    Group,
    GroupBelonging,
    Keyword,
    Membership,
    Organisation,
    Person,
    RoleToEvent,
    Role,
)

pytestmark = pytest.mark.asyncio


async def test_search_endpoint(aclient, person, organisation):
    person2 = Person(first_name="Mike", last_name="Bill").save()
    resp = await aclient.get("/contact/search?q=Bill")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 3
    assert set(data.keys()) == {"data", "facets", "total"}
    assert data["total"] == 3
    assert data["data"][0] == {
        "country": None,
        "email": None,
        "first_name": "Mike",
        "label": "Mike Bill",
        "last_name": "Bill",
        "pk": person2.pk,
        "roles": [],
        "resource": "person",
        "uri": f"/person/{person2.pk}",
        "diseases": [],
        "role": None,
    }
    assert data["data"][1] == {
        "country": "FR",
        "email": "bill@somewhere.org",
        "first_name": "Bill",
        "label": "Bill Mollison",
        "last_name": "Mollison",
        "pk": person.pk,
        "roles": [
            {
                "is_primary": True,
                "is_default": False,
                "organisation": organisation.pk,
                "person": person.pk,
            }
        ],
        "resource": "person",
        "uri": f"/person/{person.pk}",
        "diseases": [],
        "role": None,
    }
    assert data["data"][2] == {
        "country": "BE",
        "label": "Bill Org",
        "name": "Bill Org",
        "pk": organisation.pk,
        "roles": [
            {
                "is_primary": True,
                "is_default": False,
                "organisation": organisation.pk,
                "person": person.pk,
            }
        ],
        "resource": "organisation",
        "kind": 60,
        "uri": f"/organisation/{organisation.pk}",
        "diseases": [],
        "role": None,
    }
    assert data["facets"] == {
        "country": [
            {"count": 2, "label": "Europe", "pk": "EUR", "weight": 197},
            {"count": 2, "label": "European Union", "pk": "EU", "weight": 196},
            {"count": 1, "label": "France", "pk": "FR", "weight": 57},
            {"count": 1, "label": "Belgium", "pk": "BE", "weight": 14},
        ],
        "disease": [],
        "grouping": [],
        "group": [],
        "group_kind": [],
        "group_role": [],
        "keyword": [],
        "keyword_kind": [],
        "event": [],
        "event_kind": [],
        "event_role": [],
        "membership": [],
        "kind": [{"count": 2, "label": "Patient Organisation", "pk": 60}],
        "resource": [
            {"count": 2, "label": "Person", "pk": "person"},
            {"count": 1, "label": "Organisation", "pk": "organisation"},
        ],
    }


async def test_empty_search_returns_400(aclient):
    resp = await aclient.get("/contact/search")
    assert resp.status == 400


async def test_facets_should_be_computed_on_total(aclient, person, organisation):
    Person(first_name="Mike", last_name="Bill").save()
    resp = await aclient.get("/contact/search?q=Bill")
    assert resp.status == 200
    data = json.loads(resp.body)
    no_limit_facets = data["facets"]
    resp = await aclient.get("/contact/search?q=Bill&limit=1")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["facets"] == no_limit_facets


async def test_facets_can_be_opted_out(aclient, person, organisation):
    resp = await aclient.get("/contact/search?q=Bill")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data["facets"]
    resp = await aclient.get("/contact/search?q=Bill&facets=off")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert not data["facets"]


async def test_kind_category_facet(aclient, person, organisation):
    Role.get(person=person, organisation=organisation).delete()
    # Should be included in Patient Organisation counter.
    Organisation(kind=61, name="Bill's European Federation").save()
    resp = await aclient.get("/contact/search?q=Bill&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert data["facets"]["kind"] == [
        {"count": 2, "label": "Patient Organisation", "pk": 60},
        {"count": 1, "label": "European Federation", "pk": 61},
    ]


async def test_search_endpoint_with_limit(aclient, person, organisation):
    Person(first_name="Mike", last_name="Bill").save()
    resp = await aclient.get("/contact/search?q=Bill&limit=2")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2


async def test_search_endpoint_unlimited(aclient, person, organisation):
    Person(first_name="Mike", last_name="Bill").save()
    resp = await aclient.get("/contact/search?q=Bill&limit=0")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 3


async def test_search_with_resource(aclient, person, organisation):
    resp = await aclient.get("/contact/search?q=Bill&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["total"] == 1
    assert data["data"][0]["resource"] == "person"

    resp = await aclient.get("/contact/search?q=Bill&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["total"] == 1
    assert data["data"][0]["resource"] == "organisation"

    resp = await aclient.get("/contact/search?q=Bill&resource=unknown")
    assert resp.status == 400

    resp = await aclient.get("/contact/search?q=Bill&resource=")
    assert resp.status == 200


async def test_search_from_diseases(aclient, person, organisation, disease):
    disease2 = Disease(name="Duchenne muscular dystrophy", orpha_number="12347").save()
    person.diseases.append(disease)
    person.diseases.append(disease2)
    person.save()
    organisation.diseases.append(disease)
    organisation.name = "Riad Org"
    organisation.save()
    disease3 = Disease(
        name="Hereditary persistence of alpha-fetoprotein", orpha_number="12346"
    ).save()
    disease.descendants.append(disease3)
    disease3.ancestors.append(disease)
    disease.save()
    disease3.save()
    resp = await aclient.get(f"/contact/search?disease={disease.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert data["facets"]["disease"] == [
        {
            "pk": 12345,
            "label": "Congenital deficiency in alpha-fetoprotein",
            "count": 2,
            "depth": 0,
            "max_depth": 8,
        },
        {
            "count": 1,
            "label": "Duchenne muscular dystrophy",
            "pk": 12347,
            "depth": 0,
            "max_depth": 0,
        },
    ]
    resp = await aclient.get(f"/contact/search?disease={disease.pk}&q=riad")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    resp = await aclient.get(f"/contact/search?disease=>{disease.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    resp = await aclient.get(f"/contact/search?disease={disease3.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 0
    resp = await aclient.get(f"/contact/search?disease=<{disease3.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2


async def test_search_with_multiple_diseases(aclient, person, organisation, disease):
    Role.get(organisation, person).delete()
    person.reload()  # Reload with deleted role.
    organisation.reload()
    disease2 = Disease(
        name="Hereditary persistence of alpha-fetoprotein", orpha_number="12346"
    ).save()
    person.diseases = [disease, disease2]
    person.save()
    organisation.diseases.append(disease2)
    organisation.save()
    resp = await aclient.get(f"/contact/search?disease={disease.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get(f"/contact/search?disease=-{disease.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk
    resp = await aclient.get(
        f"/contact/search?disease={disease.pk}&disease={disease2.pk}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    resp = await aclient.get(
        f"/contact/search?disease=%2B{disease.pk}&disease=%2B{disease2.pk}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get(
        f"/contact/search?disease=-{disease.pk}&disease=-{disease2.pk}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 0


async def test_diseases_intersection(aclient, person, organisation, disease):
    disease2 = Disease(name="Duchenne muscular dystrophy", orpha_number="12347").save()
    person.diseases = [disease, disease2]
    person.save()
    organisation.diseases.append(disease)
    organisation.save()
    disease3 = Disease(
        name="Hereditary persistence of alpha-fetoprotein", orpha_number="12346"
    ).save()
    disease.descendants.append(disease3)
    disease3.ancestors.append(disease)
    disease.save()
    disease3.save()
    resp = await aclient.get(
        f"/contact/search?disease=%2B<{disease3.pk}&disease=%2B{disease2.pk}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_from_empty_diseases(aclient, person, organisation, disease):
    person.diseases.append(disease)
    person.save()
    organisation.diseases = []
    organisation.save()
    resp = await aclient.get("/contact/search?disease=-")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk


async def test_search_from_non_rare_disease(aclient, person, disease):
    # Non rare disease is a special case, out of Orphadata, so using 0 as number.
    non_rare = Disease(
        name="Non rare in Europe", orpha_number=0, descendants=[disease]
    ).save()
    disease.ancestors = [non_rare]
    disease.save()
    person.diseases = [non_rare]
    person.save()
    resp = await aclient.get(f"/contact/search?disease=0")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    person.diseases = [disease]
    person.save()
    resp = await aclient.get("/contact/search?disease=>0")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_from_grouping(aclient, organisation, disease, person):
    Role.get(organisation, person).delete()
    organisation.diseases.append(disease)
    organisation.save()
    resp = await aclient.get("/contact/search?grouping=eye")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Bill Org"
    assert len(data["facets"]["grouping"]) == 2
    assert {"count": 1, "label": "Bone", "pk": "bond"} in data["facets"]["grouping"]
    assert {"count": 1, "label": "Eye", "pk": "eye"} in data["facets"]["grouping"]


async def test_can_exclude_grouping(aclient, organisation, disease, person):
    Role.get(organisation, person).delete()
    organisation.diseases.append(disease)
    organisation.save()
    resp = await aclient.get("/contact/search?grouping=-eye")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Bill Mollison"
    assert len(data["facets"]["grouping"]) == 0


async def test_search_from_event(aclient, person, event, role):
    role.events.append(RoleToEvent(event=event))
    role.save()
    resp = await aclient.get(f"/contact/search?event={event.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Bill Mollison"
    assert data["facets"]["event"] == [
        {"count": 1, "label": event.name, "pk": event.pk}
    ]


async def test_search_can_exclude_event(aclient, event, role, role2):
    role.events.append(RoleToEvent(event=event))
    role.save()
    event2 = Event(
        name="CEF Meeting",
        kind="cef",
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role2.events = [RoleToEvent(event=event2)]
    role2.save()
    resp = await aclient.get(f"/contact/search?event=-{event.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == role2.person.pk
    assert data["facets"]["event_kind"] == [{"count": 1, "label": "CEF", "pk": "cef"}]


async def test_search_event_intersection(aclient, event, role, role2):
    event2 = Event(
        name="Royal Gala 2",
        kind=event.kind,
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role.events = [RoleToEvent(event=event), RoleToEvent(event=event2)]
    role.save()
    role2.events = [RoleToEvent(event=event)]
    role2.save()
    resp = await aclient.get(
        f"/contact/search?event=%2B{event.pk}&event=%2B{event2.pk}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == role.person.pk
    assert data["facets"]["event_kind"] == [
        {"count": 1, "label": "Awards ceremony", "pk": "awards"}
    ]


async def test_search_from_event_kind(aclient, event, role, role2):
    event2 = Event(
        name="Royal Gala 2",
        kind=event.kind,
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role.events.append(RoleToEvent(event=event))
    role.events.append(RoleToEvent(event=event2))
    role.save()
    role2.events.append(RoleToEvent(event=event))
    role2.save()
    resp = await aclient.get(f"/contact/search?event_kind={event.kind}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert data["facets"]["event_kind"] == [
        {"count": 2, "label": "Awards ceremony", "pk": "awards"}
    ]


async def test_search_from_event_kind_intersection(aclient, event, role, role2):
    event2 = Event(
        name="CEF Meeting",
        kind="cef",
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    # Another event with kind "CEF", but not linked to "person".
    Event(
        name="CEF Meeting again",
        kind="cef",
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role.events.append(RoleToEvent(event=event))
    role.events.append(RoleToEvent(event=event2))
    role.save()
    role2.events.append(RoleToEvent(event=event))
    role2.save()
    resp = await aclient.get(
        f"/contact/search?event_kind=%2B{event.kind}&event_kind=%2B{event2.kind}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["facets"]["event_kind"] == [
        {"count": 1, "label": "CEF", "pk": "cef"},
        {"count": 1, "label": "Awards ceremony", "pk": "awards"},
    ]


async def test_search_from_excluded_event_kind(aclient, event, role, role2):
    event2 = Event(
        name="CEF",
        kind="cef",
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role.events.append(RoleToEvent(event=event))
    role.save()
    role2.events.append(RoleToEvent(event=event2))
    role2.save()
    resp = await aclient.get(
        f"/contact/search?event_kind=-{event.kind}&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == role2.person.pk


async def test_search_from_event_and_role(aclient, event, role, role2):
    role.events.append(RoleToEvent(event=event, role="Speaker"))
    role.save()
    role2.events.append(RoleToEvent(event=event, role="Awardee"))
    role2.save()
    # Case insensitive.
    resp = await aclient.get(f"/contact/search?event={event.pk}&event_role=speaker")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Bill Mollison"
    assert data["facets"]["event"] == [
        {"count": 1, "label": event.name, "pk": event.pk}
    ]
    assert data["facets"]["event_role"] == [
        {"count": 1, "label": "Speaker", "pk": "Speaker"}
    ]


async def test_search_from_event_and_empty_role(aclient, person, event, role, role2):
    role.events.append(RoleToEvent(event=event))
    role.save()
    role2.events.append(RoleToEvent(event=event, role="Awardee"))
    role2.save()
    resp = await aclient.get(f"/contact/search?event={event.pk}&event_role=")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert data["facets"]["event"] == [
        {"count": 1, "label": event.name, "pk": event.pk}
    ]
    assert data["facets"]["event_role"] == [
        {"count": 1, "label": "[Empty]", "pk": ""},
    ]


async def test_search_empty_event_role_without_event_role(aclient, role, role2, event):
    role.events.append(RoleToEvent(event=event))
    role2.events.append(RoleToEvent(event=event, role="Awardee"))
    role.save()
    role2.save()
    resp = await aclient.get("/contact/search?resource=person&event_role=")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == role.person.pk
    assert data["facets"]["event"] == [
        {"count": 1, "label": event.name, "pk": event.pk}
    ]
    assert data["facets"]["event_role"] == [
        {"count": 1, "label": "[Empty]", "pk": ""},
    ]


async def test_search_excluded_role_without_event(aclient, person, role, role2, event):
    role.events.append(RoleToEvent(event=event))
    role2.events.append(RoleToEvent(event=event, role="Awardee"))
    role.save()
    role2.save()
    resp = await aclient.get("/contact/search?event_role=-Awardee&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert data["facets"]["event"] == [
        {"count": 1, "label": event.name, "pk": event.pk}
    ]
    assert data["facets"]["event_role"] == [
        {"count": 1, "label": "[Empty]", "pk": ""},
    ]


async def test_event_role_facet_is_targeted_to_searched_events(aclient, role, event):
    event2 = Event(
        name="Royal Gala 2",
        kind=event.kind,
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role.events.append(RoleToEvent(event=event, role="Speaker"))
    role.events.append(RoleToEvent(event=event2, role="Awardee"))
    role.save()
    resp = await aclient.get(f"/contact/search?event={event.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data["facets"]["event_role"] == [
        {"count": 1, "label": "Speaker", "pk": "Speaker"}
    ]


async def test_event_role_intersection(aclient, role, event):
    event2 = Event(
        name="Royal Gala 2",
        kind=event.kind,
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role.events.append(RoleToEvent(event=event, role="Speaker"))
    role.events.append(RoleToEvent(event=event2, role="Awardee"))
    role.save()
    resp = await aclient.get("/contact/search?event_role=Speaker&event_role=Awardee")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data["facets"]["event_role"] == [
        {"count": 1, "label": "Speaker", "pk": "Speaker"},
        {"count": 1, "label": "Awardee", "pk": "Awardee"},
    ]


async def test_search_from_event_and_excluded_role(aclient, role, role2, event):
    role.events.append(RoleToEvent(event=event, role="Speaker"))
    role.save()
    role2.events.append(RoleToEvent(event=event, role="Awardee"))
    role2.save()
    # Case insensitive.
    resp = await aclient.get(f"/contact/search?event={event.pk}&event_role=-speaker")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == role2.person.pk
    assert data["facets"]["event"] == [
        {"count": 1, "label": event.name, "pk": event.pk}
    ]
    assert data["facets"]["event_role"] == [
        {"count": 1, "label": "Awardee", "pk": "Awardee"}
    ]


async def test_combine_intersection_and_exclude_event(aclient, role, role2, event):
    event2 = Event(
        name="CEF Meeting",
        kind="cef",
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    event3 = Event(
        name="CNA Meeting again",
        kind="cna",
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role.events = [RoleToEvent(event=event), RoleToEvent(event=event2)]
    role.save()
    role2.events = [
        RoleToEvent(event=event),
        RoleToEvent(event=event2),
        RoleToEvent(event=event3),
    ]
    role2.save()
    resp = await aclient.get(
        f"/contact/search?event_kind=%2B{event.kind}&event_kind=%2B{event2.kind}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    resp = await aclient.get(
        f"/contact/search?event_kind=%2B{event.kind}&event_kind=%2B{event2.kind}&event_kind=-{event3.kind}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["facets"]["event_kind"] == [
        {"count": 1, "label": "CEF", "pk": "cef"},
        {"count": 1, "label": "Awards ceremony", "pk": "awards"},
    ]


async def test_search_from_group(aclient, organisation, person, person2):
    cef = Group(name="CEF").save()
    cna = Group(name="CNA").save()
    person.roles[0].add_group(group=cef).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[
                GroupBelonging(group=cef, is_active=False),
                GroupBelonging(group=cna),
            ],
        )
    ]
    person2.save()
    resp = await aclient.get(f"/contact/search?group={cef.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert data["facets"]["group"] == [{"count": 1, "label": "CEF", "pk": cef.pk}]


async def test_search_with_has_groups(aclient, organisation, person, person2, group):
    person.roles[0].add_group(group=group).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[
                GroupBelonging(group=group, is_active=False),
            ],
        )
    ]
    person2.save()
    resp = await aclient.get("/contact/search?q=has:groups")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    resp = await aclient.get("/contact/search?q=has:groups&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk
    resp = await aclient.get("/contact/search?q=has:groups&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get("/contact/search?q=has:groups:inactive&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    resp = await aclient.get("/contact/search?q=has:groups:active&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1


async def test_search_organisation_from_group(aclient, organisation, person, group):
    person.roles[0].add_group(group=group).save()
    resp = await aclient.get(f"/contact/search?group={group.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert data["data"][0]["pk"] == person.pk
    assert data["data"][1]["pk"] == organisation.pk


async def test_search_from_group_role(aclient, organisation, person, person2):
    cef = Group(name="CEF").save()
    cna = Group(name="CNA").save()
    person.roles[0].add_group(group=cef, role="Applicant").save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[
                GroupBelonging(group=cef, role="Member"),
                GroupBelonging(group=cna),
            ],
        )
    ]
    person2.save()
    resp = await aclient.get(f"/contact/search?group={cef.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    facets = data["facets"]["group_role"]
    assert len(facets) == 2
    assert {"count": 1, "label": "Applicant", "pk": "Applicant"} in facets
    assert {"count": 1, "label": "Member", "pk": "Member"} in facets
    resp = await aclient.get(
        f"/contact/search?group={cef.pk}&group_role=Member&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    assert data["facets"]["group_role"] == [
        {"count": 1, "label": "Member", "pk": "Member"}
    ]
    resp = await aclient.get(f"/contact/search?group={cna.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    assert data["facets"]["group_role"] == [
        {"count": 1, "label": "[Empty]", "pk": None}
    ]


async def test_group_role_facet_should_be_restricted_to_searched_group_kind(
    aclient, person, organisation
):
    cef = Group(name="CEF", kind="epag").save()
    cna = Group(name="CNA", kind="ema").save()
    person.roles[0].add_group(group=cef, role="Member").add_group(
        group=cna, role="Applicant"
    ).save()
    resp = await aclient.get("/contact/search?group_kind=epag&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1

    # Roles of this person in other groups should not be returned in facets.
    assert data["facets"]["group_role"] == [
        {"count": 1, "label": "Member", "pk": "Member"}
    ]
    resp = await aclient.get("/contact/search?group_kind=ema&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["facets"]["group_role"] == [
        {"count": 1, "label": "Applicant", "pk": "Applicant"}
    ]


async def test_search_from_excluded_group_role(aclient, person, person2, organisation):
    cef = Group(name="CEF").save()
    person.roles[0].add_group(group=cef, role="Applicant").save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=cef, role="Member")],
        )
    ]
    person2.save()
    resp = await aclient.get(
        f"/contact/search?group={cef.pk}&group_role=-Member&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_group_role_intersection_with_group_kind(
    aclient, person, person2, organisation
):
    group1 = Group(name="G1", kind="ema").save()
    group2 = Group(name="G2", kind="ema").save()
    person.roles[0].add_group(group=group1, role="Applicant").add_group(
        group=group2, role="Member"
    ).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group2, role="Member")],
        )
    ]
    person2.save()
    resp = await aclient.get(
        "/contact/search?group_kind=ema&group_role=%2BMember&group_role=%2BApplicant&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_group_role_intersection_with_group_status(
    aclient, person, person2, group, organisation
):
    person.roles[0].add_group(group=group, role="Member", is_active=True).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group, role="Member", is_active=False)],
        )
    ]
    person2.save()
    resp = await aclient.get("/contact/search?group_role=>Member&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get("/contact/search?group_role=<Member&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    resp = await aclient.get("/contact/search?group_role=*Member&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2


async def test_search_group_role_with_all_group_status(
    aclient, person, person2, organisation
):
    group1 = Group(name="G1", kind="ema").save()
    group2 = Group(name="G2", kind="ema").save()
    person.roles[0].add_group(group=group1, role="Member", is_active=True).add_group(
        group=group2, role="Applicant", is_active=True
    ).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[
                GroupBelonging(group=group2, role="Applicant", is_active=True),
                GroupBelonging(group=group1, role="Member", is_active=False),
            ],
        ),
    ]
    person2.save()
    resp = await aclient.get(
        "/contact/search?group_role=%2B>Member&%2B>Applicant&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_group_role_with_none_group_status(
    aclient, person, person2, group, organisation
):
    person.roles[0].add_group(group=group, role="Member", is_active=True).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group, role="Member", is_active=False)],
        ),
    ]
    person2.save()
    resp = await aclient.get("/contact/search?group_role=->Member&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    resp = await aclient.get("/contact/search?group_role=-<Member&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_from_inactive_group(
    aclient, person, person2, group, organisation
):
    person.roles[0].add_group(group=group).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group, is_active=False)],
        )
    ]
    person2.save()
    resp = await aclient.get(f"/contact/search?group=<{group.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    assert data["facets"]["group"] == [{"count": 1, "label": "CNA", "pk": group.pk}]


async def test_search_from_all_status_group(
    aclient, person, person2, group, organisation
):
    group2 = Group(name="ePag").save()
    person.roles[0].add_group(group=group).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group2, is_active=False)],
        )
    ]
    person2.save()
    resp = await aclient.get(
        f"/contact/search?group=*{group.pk}&group=*{group2.pk}&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert len(data["facets"]["group"]) == 2
    assert {"count": 1, "label": group.name, "pk": group.pk} in data["facets"]["group"]
    assert {"count": 1, "label": group2.name, "pk": group2.pk} in data["facets"][
        "group"
    ]


async def test_group_intersection(aclient, person, person2, group, organisation):
    org2 = Organisation(name="other").save()
    group2 = Group(name="ePag").save()
    Role(person=person, organisation=org2, groups=[GroupBelonging(group=group2)]).save()
    person.roles[0].add_group(group=group).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[
                GroupBelonging(group=group2, is_active=False),
                GroupBelonging(group=group),
            ],
        )
    ]
    person2.save()
    resp = await aclient.get(
        f"/contact/search?group=%2B{group.pk}&group=%2B{group2.pk}&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get(
        f"/contact/search?group=%2B{group.pk}&group=%2B*{group2.pk}&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2


async def test_search_from_group_and_empty_role(
    aclient, person, person2, group, organisation
):
    person.roles[0].add_group(group=group).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group, role="Member")],
        )
    ]
    person2.save()
    resp = await aclient.get(
        f"/contact/search?group={group.pk}&group_role=&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert data["facets"]["group"] == [
        {"count": 1, "label": group.name, "pk": group.pk}
    ]
    assert data["facets"]["group_role"] == [
        {"count": 1, "label": "[Empty]", "pk": None},
    ]


async def test_search_empty_group_role_without_group(
    aclient, person, person2, group, organisation
):
    person.roles[0].add_group(group=group).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group, role="Member")],
        )
    ]
    person2.save()
    # No group, should not be selected when looking for empty group roles.
    Person(last_name="Leonard", email="leo@bird.wire", country="CA").save()
    resp = await aclient.get("/contact/search?group_role=&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert data["facets"]["group"] == [
        {"count": 1, "label": group.name, "pk": group.pk}
    ]
    assert data["facets"]["group_role"] == [
        {"count": 1, "label": "[Empty]", "pk": None},
    ]


async def test_search_from_excluded_group(aclient, organisation, person, person2):
    cef = Group(name="CEF").save()
    cna = Group(name="CNA").save()
    person.roles[0].add_group(group=cef, is_active=True).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[
                GroupBelonging(group=cef, is_active=False),
                GroupBelonging(group=cna),
            ],
        )
    ]
    person2.save()
    resp = await aclient.get(f"/contact/search?group=-{cef.pk}&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk
    resp = await aclient.get(f"/contact/search?group=-{cef.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_search_from_keyword(aclient, person, person2, keyword):
    person.keywords.append(keyword)
    person.save()
    resp = await aclient.get(f"/contact/search?keyword={keyword.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert data["facets"]["keyword"] == [
        {"count": 1, "label": "reviewed", "pk": keyword.pk}
    ]


async def test_search_can_exclude_keyword(aclient, person, person2, keyword):
    person.keywords.append(keyword)
    person.save()
    resp = await aclient.get(f"/contact/search?keyword=-{keyword.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_search_keyword_intersection(aclient, person, person2, keyword):
    keyword2 = Keyword(name="todo").save()
    person.keywords = [keyword, keyword2]
    person.save()
    person2.keywords = [keyword]
    person2.save()
    resp = await aclient.get(
        f"/contact/search?keyword=%2B{keyword.pk}&keyword=%2B{keyword2.pk}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_from_kind(aclient, organisation):
    Organisation(
        name="Another Org", email="contact@another.org", country="BE", kind=70
    ).save()
    organisation.kind = 62
    organisation.save()
    resp = await aclient.get("/contact/search?kind=62&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Bill Org"
    resp = await aclient.get("/contact/search?kind=60&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Bill Org"
    resp = await aclient.get("/contact/search?kind=60&country=BE&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    resp = await aclient.get("/contact/search?kind=60&country=FR&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 0
    resp = await aclient.get("/contact/search?kind=50&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 0


async def test_search_can_exclude_kind(aclient, organisation):
    Organisation(
        name="Another Org", email="contact@another.org", country="BE", kind=70
    ).save()
    organisation.kind = 62
    organisation.save()
    resp = await aclient.get("/contact/search?kind=-62&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Another Org"
    resp = await aclient.get("/contact/search?kind=-60&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Another Org"


async def test_search_can_exclude_person_from_org_kind(aclient, person, organisation):
    organisation.kind = 62
    organisation.save()
    resp = await aclient.get("/contact/search?kind=62&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get("/contact/search?kind=-62&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert not data["data"]


async def test_search_can_intersect_on_kind(aclient, person, organisation):
    other = Organisation(
        name="Another Org", email="contact@another.org", country="BE", kind=70
    ).save()
    Role(person=person, organisation=other).save()
    organisation.kind = 62
    organisation.save()
    resp = await aclient.get("/contact/search?kind=%2B60&kind=%2B70")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get("/contact/search?kind=60&kind=-70&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert not data["data"]


async def test_search_by_email(aclient, person):
    # Case insensitive.
    resp = await aclient.get(f"/contact/search?q={person.email.upper()}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["label"] == "Bill Mollison"


async def test_search_by_empty_email(aclient, person):
    person.email = None
    person.save()
    resp = await aclient.get("/contact/search?q=no:email")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_from_country(aclient, organisation, person, person2):
    Role.get(organisation, person).delete()
    organisation.country = "BE"
    organisation.save()
    person.country = "FR"
    person.save()
    person2.country = "RU"
    person2.save()
    resp = await aclient.get("/contact/search?country=BE&resource=organisation")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk
    resp = await aclient.get("/contact/search?country=EU")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    pks = {d["pk"] for d in data["data"]}
    assert pks == {organisation.pk, person.pk}
    resp = await aclient.get("/contact/search?country=BE&country=FR")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    pks = {d["pk"] for d in data["data"]}
    assert pks == {organisation.pk, person.pk}
    resp = await aclient.get("/contact/search?country=EUR")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 3
    resp = await aclient.get("/contact/search?country=-EU")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_search_role_from_country(aclient, organisation, person, person2):
    assert Role.get(organisation=organisation, person=person)
    organisation.country = "BE"
    organisation.save()
    person.country = "FR"
    person.save()
    person2.country = "RU"
    person2.save()
    # Search role, mixing organisation and person filters
    resp = await aclient.get("/contact/search?country=EU&kind=60")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    pks = {d["pk"] for d in data["data"]}
    assert pks == {organisation.pk, person.pk}


async def test_mixed_include_and_exclude_country(aclient, person, person2):
    person.country = "BE"
    person.save()
    person2.country = "MX"
    person2.save()
    resp = await aclient.get("/contact/search?country=-EU&country=MX")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_search_from_empty_country(aclient, organisation, person):
    organisation.country = None
    organisation.save()
    person.country = "FR"
    person.save()
    resp = await aclient.get("/contact/search?country=-")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk


async def test_search_organisation_from_membership(aclient, organisation):
    active = Organisation(
        name="Active membership",
        membership=[
            Membership(organisation=organisation, start_date="2019-01-01"),
            Membership(
                organisation=organisation,
                start_date="2018-01-01",
                end_date="2018-12-31",
            ),
        ],
    ).save()
    Organisation(
        name="Inactive membership",
        membership=[
            Membership(
                organisation=organisation,
                start_date="2018-01-01",
                end_date="2018-12-31",
            )
        ],
    ).save()
    Organisation(
        name="Withdrawn membership",
        membership=[
            Membership(organisation=organisation, start_date="2018-01-01", status="w")
        ],
    ).save()
    resp = await aclient.get(f"/contact/search?membership={organisation.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == active.pk
    assert data["facets"]["membership"] == [
        {"count": 1, "label": organisation.name, "pk": organisation.pk}
    ]


async def test_membership_intersection(aclient):
    parent1 = Organisation(name="parent1").save()
    parent2 = Organisation(name="parent2").save()
    Organisation(
        name="child 1",
        membership=[Membership(organisation=parent1, start_date="2019-01-01")],
    ).save()
    child1and2 = Organisation(
        name="Child 1 and 2",
        membership=[
            Membership(organisation=parent1, start_date="2018-01-01"),
            Membership(
                organisation=parent2,
                start_date="2018-01-01",
            ),
        ],
    ).save()
    resp = await aclient.get(
        f"/contact/search?membership=%2B{parent1.pk}&membership=%2B{parent2.pk}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == child1and2.pk
    assert data["facets"]["membership"] == [
        {"count": 1, "label": parent2.name, "pk": parent2.pk},
        {"count": 1, "label": parent1.name, "pk": parent1.pk},
    ]


async def test_search_membership_without_fees(aclient, organisation, eurordis_org):
    # Should appear.
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2017-01-01", status="f")
    ]
    organisation.save()
    Role.objects.get(organisation=organisation).delete()
    Organisation(
        name="Already paid",
        membership=[
            Membership(
                organisation=eurordis_org,
                start_date="2019-01-01",
                status="f",
                fees=[Fee(year=2019, amount=50, payment_date="2019-01-01")],
            )
        ],
    ).save()
    resp = await aclient.get(
        f"/contact/search?q=membership.fees.year:-2019&membership={eurordis_org.pk}"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk


async def test_can_search_modified_by(aclient, organisation, person, person2):
    person2.email = "foo@eurordis.org"
    person2.save()
    # Case insensitive.
    resp = await aclient.get(
        "/search?q=modified_by:FOO@eurordis.org&resource=organisation"
    )
    assert not json.loads(resp.body)["data"]
    with session.as_user(person2):
        organisation.save()
    assert organisation.modified_by == person2
    resp = await aclient.get(
        "/search?q=modified_by:FOO@eurordis.org&resource=organisation"
    )
    body = json.loads(resp.body)
    assert len(body["data"]) == 1
    assert body["data"][0]["pk"] == organisation.pk
    # With pk instead of email
    resp = await aclient.get(
        f'/search?q=modified_by:"{person2.pk}"&resource=organisation'
    )
    body = json.loads(resp.body)
    assert len(body["data"]) == 1
    assert body["data"][0]["pk"] == organisation.pk


async def test_can_search_modified_at(aclient, organisation, person, person2):
    today = datetime.strftime(helpers.utcnow(), r"%Y-%m-%d")
    resp = await aclient.get(f"/search?q=modified_at:<{today}")
    assert not json.loads(resp.body)["data"]
    organisation.modified_at = helpers.utcnow() - timedelta(days=3)
    organisation.save(ghost=True)
    resp = await aclient.get(f"/search?q=modified_at:<{today}")
    body = json.loads(resp.body)
    assert len(body["data"]) == 1
    assert body["data"][0]["pk"] == organisation.pk


async def test_can_search_date_without_operator(aclient, person):
    today = datetime.strftime(helpers.utcnow(), r"%Y-%m-%d")
    resp = await aclient.get(f"/search?q=modified_at:{today}")
    body = json.loads(resp.body)
    assert len(body["data"]) == 1
    assert body["data"][0]["pk"] == person.pk
    person.modified_at = helpers.utcnow() - timedelta(days=3)
    person.save(ghost=True)
    resp = await aclient.get(f"/search?q=modified_at:{today}")
    assert not json.loads(resp.body)["data"]


async def test_search_by_membership_start_date(aclient, organisation, eurordis_org):
    # Should appear.
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2017-01-01", status="f")
    ]
    organisation.save()
    new = Organisation(
        name="New Member",
        membership=[
            Membership(organisation=eurordis_org, start_date="2019-03-10", status="f")
        ],
    ).save()
    resp = await aclient.get(f"/contact/search?q=membership.start_date:>2019-03-01")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == new.pk


async def test_search_by_current_membership_status(aclient, organisation, eurordis_org):
    Role.objects.get(organisation=organisation).delete()
    # Should appear.
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2017-01-01", status="f")
    ]
    organisation.save()
    new = Organisation(
        name="New Member",
        membership=[
            Membership(organisation=eurordis_org, start_date="2019-03-10", status="a")
        ],
    ).save()
    # Not linked to EURORDIS.
    Organisation(
        name="New Member",
        membership=[
            Membership(organisation=organisation, start_date="2019-03-10", status="a")
        ],
    ).save()
    resp = await aclient.get(f"/contact/search?q=members:eurordis:f")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk
    resp = await aclient.get(f"/contact/search?q=members:eurordis:a")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == new.pk


async def test_search_orphan(aclient, person, organisation, person2):
    orphan = Organisation(name="Orphan").save()
    resp = await aclient.get("/contact/search?q=is:orphan")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert {orphan.pk, person2.pk} == {c["pk"] for c in data["data"]}


async def test_search_orphan_can_be_combined(aclient, person, organisation, person2):
    orphan = Organisation(name="Orphan", country="BE").save()
    person2.country = "AU"
    person2.save()
    resp = await aclient.get("/contact/search?q=is:orphan&country=BE")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == orphan.pk


async def test_search_offline(aclient, person, organisation, person2):
    person2.email = None
    person2.save()
    person3 = Person(
        first_name="Mike", last_name="Bill", email="not@null.org", country="AU"
    ).save()
    person4 = Person(
        first_name="Mike", last_name="Bill", email=None, country="AU"
    ).save()
    Role(person=person2.pk, organisation=organisation).save()
    Role(person=person3.pk, organisation=organisation).save()
    Role(person=person4.pk, organisation=organisation, email="ihave@one.org").save()
    resp = await aclient.get("/contact/search?q=is:offline")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_search_offline_can_be_combined(aclient, person, organisation, person2):
    person2.country = "BE"
    person2.email = None
    person2.save()
    person3 = Person(
        first_name="Mike", last_name="Bill", email=None, country="AU"
    ).save()
    Role(person=person2.pk, organisation=organisation).save()
    Role(person=person3.pk, organisation=organisation).save()
    resp = await aclient.get("/contact/search?q=is:offline")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    resp = await aclient.get("/contact/search?q=is:offline&country=BE")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_search_unreachable(aclient, person, organisation, person2):
    organisation.email = "ihave@one.org"
    organisation.save()
    organisation2 = Organisation(name="Unreachable").save()
    person2.email = None
    person2.save()
    person.email = None
    person.save()
    Role.get(person=person, organisation=organisation).delete()
    Role(person=person, organisation=organisation).save()  # No email
    Role(person=person2, organisation=organisation2).save()
    resp = await aclient.get("/contact/search?q=is:offline")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    resp = await aclient.get("/contact/search?q=is:unreachable")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_search_mute(aclient, person, organisation, person2):
    person2.phone = None
    person2.save()
    person3 = Person(
        first_name="Mike", last_name="Bill", phone="12345678", country="AU"
    ).save()
    person4 = Person(
        first_name="Mike", last_name="Bill", phone=None, country="AU"
    ).save()
    Role(person=person2.pk, organisation=organisation).save()
    Role(person=person3.pk, organisation=organisation).save()
    Role(person=person4.pk, organisation=organisation, phone="12345678").save()
    resp = await aclient.get("/contact/search?q=is:mute")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_orga_diseases_are_inherited_by_role(
    aclient, person, organisation, disease
):
    organisation.diseases = [disease]
    organisation.save()
    resp = await aclient.get(f"/contact/search?disease={disease.pk}")
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert data["data"][0]["pk"] == person.pk
    assert data["data"][1]["pk"] == organisation.pk


async def test_no_inherit_should_only_target_contact_without_roles(
    aclient, person, organisation, disease
):
    organisation.diseases = [disease]
    organisation.save()
    resp = await aclient.get("/contact/search?q=has:diseases")
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert data["data"][0]["pk"] == person.pk
    assert data["data"][1]["pk"] == organisation.pk
    resp = await aclient.get("/contact/search?q=has:diseases&resource=person")
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get("/contact/search?q=has:diseases%20no:inherit")
    data = json.loads(resp.body)
    assert len(data["data"]) == 0
    person.diseases = [disease]
    person.save()
    resp = await aclient.get("/contact/search?q=has:diseases%20no:inherit")
    data = json.loads(resp.body)
    assert len(data["data"]) == 1


async def test_orga_grouping_are_inherited_by_role(aclient, person, organisation, disease):
    organisation.diseases = [disease]
    organisation.save()
    resp = await aclient.get("/contact/search?grouping=bond")
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert data["data"][0]["pk"] == person.pk
    assert data["data"][1]["pk"] == organisation.pk


async def test_can_combine_inherited_properties(aclient, person, organisation, disease):
    person.country = "CA"
    person.save()
    organisation.diseases = [disease]
    organisation.save()
    resp = await aclient.get(f"/contact/search?disease={disease.pk}&country=CA")
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_event_facet_are_deduplicated(aclient, role, organisation, event):
    role.events.append(RoleToEvent(event=event))
    role.save()
    resp = await aclient.get(
        f"/contact/search?kind={organisation.kind}&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == role.person.pk
    assert data["facets"]["event"] == [
        {"count": 1, "label": event.name, "pk": event.pk}
    ]


async def test_keyword_facet_are_deduplicated(aclient, person, organisation, keyword):
    organisation.country = "BE"
    organisation.save()
    person.country = "FR"
    person.keywords = [keyword]
    person.save()
    resp = await aclient.get(f"/contact/search?keyword={keyword.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert data["facets"]["keyword"] == [
        {"count": 1, "label": keyword.name, "pk": keyword.pk}
    ]


async def test_membership_is_inherited(aclient, person, organisation, eurordis_org):
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2017-01-01", status="f")
    ]
    organisation.save()
    resp = await aclient.get(
        f"/contact/search?membership={eurordis_org.pk}&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_kind_facet_are_deduplicated(aclient, person, organisation, keyword):
    organisation2 = Organisation(kind=61, name="Bill's European Federation").save()
    organisation.kind = 60
    organisation.save()
    Role(person=person.pk, organisation=organisation2).save()
    resp = await aclient.get("/contact/search?q=bill")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 3
    assert data["facets"]["kind"] == [
        {"count": 3, "label": "Patient Organisation", "pk": 60},
        {"count": 2, "label": "European Federation", "pk": 61},
    ]


async def test_search_from_group_kind(aclient, person, person2, group, organisation):
    group2 = Group(
        name="Group Name",
        kind="ec",
    ).save()
    person.roles[0].add_group(group=group).add_group(group=group2).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group)],
        )
    ]
    person2.save()
    resp = await aclient.get("/contact/search?group_kind=ec&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert data["facets"]["group_kind"] == [
        {"count": 1, "label": "EC project", "pk": "ec"}
    ]


async def test_search_from_excluded_group_kind(
    aclient, person, person2, group, organisation
):
    group.kind = "ema"
    group.save()
    group2 = Group(name="Group Name", kind="ec").save()
    person.roles[0].add_group(group=group).add_group(group=group2).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group)],
        )
    ]
    person2.save()
    resp = await aclient.get("/contact/search?group_kind=-ec&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    assert data["facets"]["group"] == [{"count": 1, "label": group.name, "pk": "1"}]
    assert data["facets"]["group_kind"] == [
        {"count": 1, "label": "EMA Committee", "pk": "ema"}
    ]


async def test_search_from_group_kind_intersection(
    aclient, person, person2, group, organisation
):
    group.kind = "ema"
    group.save()
    group2 = Group(name="Group Name", kind="ec").save()
    person.roles[0].add_group(group=group).add_group(group=group2).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group)],
        )
    ]
    person2.save()
    resp = await aclient.get(
        "/contact/search?group_kind=%2Bec&group_kind=%2Bema&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_search_from_group_kind_with_status(
    aclient, person, person2, group, organisation
):
    group.kind = "ema"
    group.save()
    person.roles[0].add_group(group=group, is_active=True).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group, is_active=False)],
        )
    ]
    person2.save()
    resp = await aclient.get("/contact/search?group_kind=>ema&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get("/contact/search?group_kind=<ema&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    resp = await aclient.get("/contact/search?group_kind=*ema&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    # No operator so default should apply, which is is_active=True.
    resp = await aclient.get("/contact/search?group_kind=ema&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_combine_group_role_and_kind(
    aclient, person, person2, group, organisation
):
    group.kind = "epag"
    group.save()
    person.roles[0].add_group(group=group, role="Applicant").save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group, role="Member")],
        )
    ]
    person2.save()
    resp = await aclient.get("/contact/search?group_kind=epag&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    resp = await aclient.get(
        "/contact/search?group_kind=epag&group_role=Member&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    resp = await aclient.get(
        "/contact/search?group_kind=epag&group_role=-Member&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_combine_group_role_and_excluded_kind(
    aclient, person, person2, group, organisation
):
    group.kind = "epag"
    group.save()
    group2 = Group(name="Group2", kind="ec").save()
    person.roles[0].add_group(group=group, role="Member").save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group2, role="Member")],
        )
    ]
    person2.save()
    resp = await aclient.get(
        "/contact/search?group_kind=epag&group_role=Member&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    resp = await aclient.get(
        "/contact/search?group_kind=-epag&group_role=Member&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk
    resp = await aclient.get(
        "/contact/search?group_kind=-epag&group_role=-Member&resource=person"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 0


async def test_search_from_keyword_kind(aclient, person, person2, keyword):
    keyword2 = Keyword(name="keyword Name", kind="chore").save()
    person.keywords.append(keyword)
    person.keywords.append(keyword2)
    person.save()
    person2.keywords.append(keyword)
    person2.save()
    resp = await aclient.get("/contact/search?keyword_kind=chore")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["facets"]["keyword_kind"] == [
        {"count": 1, "label": "Contact database process", "pk": "chore"}
    ]


async def test_search_from_forced_keyword_kind(aclient, person, person2, keyword):
    keyword.kind = "expertise"
    keyword.save()
    keyword2 = Keyword(name="keyword Name", kind="chore").save()
    person.keywords.append(keyword)
    person.keywords.append(keyword2)
    person.save()
    person2.keywords.append(keyword)
    person2.save()
    resp = await aclient.get(
        "/contact/search?keyword_kind=%2Bchore&keyword_kind=%2Bexpertise"
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk


async def test_group_facet_should_honour_status(
    aclient, person, person2, group, organisation
):
    person.roles[0].add_group(group=group).save()
    person2.roles = [
        Role(
            organisation=organisation,
            person=person2,
            groups=[GroupBelonging(group=group, is_active=False)],
        )
    ]
    person2.save()
    resp = await aclient.get(f"/contact/search?group={group.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["facets"]["group"] == [
        {"count": 1, "label": group.name, "pk": group.pk}
    ]
    resp = await aclient.get(f"/contact/search?group=*{group.pk}&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert data["facets"]["group"] == [
        {"count": 2, "label": group.name, "pk": group.pk}
    ]


async def test_contact_has_precedence_over_role_if_both_match(aclient, person):
    person.country = "BE"
    person.save()
    for i in range(10):
        organisation = Organisation(name=f"orga{i}").save()
        Role(organisation=organisation, person=person).save()
    resp = await aclient.get("/contact/search?country=BE&q=Bill")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert not data["data"][0]["role"]
    resp = await aclient.get("/contact/search?country=BE")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert not data["data"][0]["role"]
    resp = await aclient.get("/contact/search?q=Bill")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person.pk
    assert not data["data"][0]["role"]


async def test_search_with_role_name(aclient, person, organisation, person2):
    Role(person=person2.pk, organisation=organisation, name="Lecturer").save()
    resp = await aclient.get("/contact/search?q=lecturer")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == person2.pk


async def test_search_no_voting(aclient, person, organisation):
    Role.get(person=person, organisation=organisation).delete()
    is_voting = Organisation(name="No Voting").save()
    Role(person=person, organisation=organisation, is_voting=False).save()
    Role(person=person, organisation=is_voting, is_voting=True).save()
    resp = await aclient.get("/contact/search?q=no:voting")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk


async def test_search_no_primary(aclient, person, organisation):
    Role.get(person=person, organisation=organisation).delete()
    is_primary = Organisation(name="No Voting").save()
    Role(person=person, organisation=organisation, is_primary=False).save()
    Role(person=person, organisation=is_primary, is_primary=True).save()
    resp = await aclient.get("/contact/search?q=no:primary")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == organisation.pk
