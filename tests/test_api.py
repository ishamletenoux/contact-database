import time
from datetime import date, datetime, timedelta
from io import BytesIO

import jwt
import pytest
import ujson as json
from eurordis import config
from eurordis.helpers import utcnow
from eurordis.models import (
    Disease,
    Event,
    Fee,
    Group,
    Keyword,
    Membership,
    Organisation,
    Person,
    RoleToEvent,
    GroupBelonging,
    Role,
)
from openpyxl import Workbook, load_workbook
from openpyxl.writer.excel import save_virtual_workbook

pytestmark = pytest.mark.asyncio


async def test_request_token(client, person, monkeypatch):
    sent = {}

    def send_email(*args, **kwargs):
        sent.update(kwargs)

    monkeypatch.setattr("eurordis.emails.send", send_email)
    person.email = "toto@eurordis.org"
    person.save()
    resp = await client.post("/token", body={"email": "toto@eurordis.org"})
    assert resp.status == 204
    assert sent["subject"] == "Contact Database Access"
    assert sent["to"] == person.email
    assert str(person) in sent["body"]
    person.email = "toto@rarediseasesint.org"
    person.save()
    resp = await client.post("/token", body={"email": "toto@rarediseasesint.org"})
    assert resp.status == 204
    sent.clear()
    person.email = "toto@fakeeurordis.org"
    person.save()
    resp = await client.post("/token", body={"email": "toto@fakeeurordis.org"})
    assert resp.status == 403
    assert not sent
    resp = await client.post("/token", body={"email": "unknown@foo.io"})
    assert resp.status == 403
    assert not sent


async def test_request_token_from_role_email(client, person, organisation, monkeypatch):
    sent = {}

    def send_email(*args, **kwargs):
        sent.update(kwargs)

    monkeypatch.setattr("eurordis.emails.send", send_email)
    role = Role.get(organisation=organisation.pk, person=person.pk)
    role.email = "ok@eurordis.org"
    role.save()
    resp = await client.post("/token", body={"email": "ok@eurordis.org"})
    assert resp.status == 204
    assert sent["subject"] == "Contact Database Access"
    assert sent["to"] == "ok@eurordis.org"
    assert str(person) in sent["body"]
    sent.clear()
    role.is_archived = True
    role.save()
    resp = await client.post("/token", body={"email": "ok@eurordis.org"})
    assert resp.status == 403
    assert not sent


async def test_staff_scopes(client, person, person2):
    client.login(person, email="foo@bar.baz")
    resp = await client.delete(f"/person/{person2.pk}")
    assert resp.status == 401
    client.logout()
    client.login(person, email=config.STAFF[0])
    resp = await client.delete(f"/person/{person2.pk}")
    assert resp.status == 204
    client.logout()


async def test_cant_get_person_without_auth(client, person):
    resp = await client.get(f"/person/{person.pk}")
    assert resp.status == 401


async def test_cant_get_person_expired_token(aclient, person):
    token = jwt.encode(
        {"sub": str(person.id), "exp": utcnow() - timedelta(hours=1)},
        config.JWT_SECRET,
        config.JWT_ALGORITHM,
    )
    aclient.default_headers["API-Key"] = token
    resp = await aclient.get(f"/person/{person.pk}")
    assert resp.status == 401


async def test_get_person(aclient, person, organisation):
    organisation2 = Organisation(name="A", email="contact@a.org", country="FR", kind=60)
    organisation2.roles = [Role(name="Test Role", person=person.pk, organisation=organisation)]
    organisation2.save()
    organisation3 = Organisation(name="B", email="contact@b.org", country="FR", kind=60)
    organisation3.roles = [Role(name="Test Role", person=person.pk, organisation=organisation)]
    organisation3.save()
    organisation4 = Organisation(name="C", email="contact@c.org", country="FR", kind=60)
    organisation4.roles = [Role(name="Test Role", person=person.pk, organisation=organisation, is_default=True)]
    organisation4.save()
    resp = await aclient.get(f"/person/{person.pk}")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "pk": person.pk,
        "resource": "person",
        "label": "Bill Mollison",
        "title": "mr",
        "first_name": "Bill",
        "last_name": "Mollison",
        "email": "bill@somewhere.org",
        "phone": "+9876543210",
        "languages": ["en", "fr"],
        "country": "FR",
        "cell_phone": "+611111111",
        "diseases": [],
        "keywords": [],
        "comment": None,
        "groupings": [],
        "attachments": [],
        "created_at": person.created_at.isoformat(),
        "modified_at": person.modified_at.isoformat(),
        "created_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "modified_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "uri": f"/person/{person.pk}",
        "filemaker_pk": None,
        "scopes": [],
        "roles": [
            {
                "name": "Test Role",
                "is_primary": False,
                "is_default": True,
                "is_voting": False,
                "is_archived": False,
                "person": {
                    "pk": person.pk,
                    "first_name": "Bill",
                    "last_name": "Mollison",
                    "email": "bill@somewhere.org",
                    "uri": f"/person/{person.pk}",
                    "resource": "person",
                    "label": "Bill Mollison",
                    "country": "FR"
                },
                "organisation": {
                    "pk": organisation4.pk,
                    "name": "C",
                    "uri": f"/organisation/{organisation4.pk}",
                    "label": "C",
                    "resource": "organisation",
                    "country": "FR",
                    "kind": 60
                },
                "phone": None,
                "email": None,
                "groups": [],
                "events": []
            },
            {
                "email": "bmollison@bill.org",
                "is_primary": True,
                "is_default": False,
                "is_voting": False,
                "is_archived": False,
                "organisation": {
                    "country": "BE",
                    "name": "Bill Org",
                    "pk": organisation.pk,
                    "uri": f"/organisation/{organisation.pk}",
                    "resource": "organisation",
                    "kind": 60,
                    "label": "Bill Org",
                },
                "person": {
                    "country": "FR",
                    "email": "bill@somewhere.org",
                    "resource": "person",
                    "label": "Bill Mollison",
                    "first_name": "Bill",
                    "last_name": "Mollison",
                    "pk": person.pk,
                    "uri": f"/person/{person.pk}",
                },
                "phone": "+1234567890",
                "name": "CEO",
                "groups": [],
                "events": [],
            },
            {
                "name": "Test Role",
                "is_primary": False,
                "is_default": False,
                "is_voting": False,
                "is_archived": False,
                "person": {
                    "pk": person.pk,
                    "first_name": "Bill",
                    "last_name": "Mollison",
                    "email": "bill@somewhere.org",
                    "uri": f"/person/{person.pk}",
                    "resource": "person",
                    "label": "Bill Mollison",
                    "country": "FR"
                },
                "organisation": {
                    "pk": organisation2.pk,
                    "name": "A",
                    "uri": f"/organisation/{organisation2.pk}",
                    "label": "A",
                    "resource": "organisation",
                    "country": "FR",
                    "kind": 60
                },
                "phone": None,
                "email": None,
                "groups": [],
                "events": []
            },
            {
                "name": "Test Role",
                "is_primary": False,
                "is_default": False,
                "is_voting": False,
                "is_archived": False,
                "person": {
                    "pk": person.pk,
                    "first_name": "Bill",
                    "last_name": "Mollison",
                    "email": "bill@somewhere.org",
                    "uri": f"/person/{person.pk}",
                    "resource": "person",
                    "label": "Bill Mollison",
                    "country": "FR"
                },
                "organisation": {
                    "pk": organisation3.pk,
                    "name": "B",
                    "uri": f"/organisation/{organisation3.pk}",
                    "label": "B",
                    "resource": "organisation",
                    "country": "FR",
                    "kind": 60
                },
                "phone": None,
                "email": None,
                "groups": [],
                "events": []
            }
        ],
    }


async def test_get_person_with_invalid_id(aclient):
    resp = await aclient.get("/person/foo")
    assert resp.status == 404
    body = json.loads(resp.body)
    assert body == {
        "error": "Resource not found: Contact.Person matching query does not exist."
    }


async def test_get_person_with_unknown_id(aclient):
    resp = await aclient.get("/person/5bbcd370f16dda392d53e7c8")
    assert resp.status == 404


async def test_get_profile(aclient, person):
    resp = await aclient.get("/me")
    assert resp.status == 200
    assert json.loads(resp.body)["pk"] == person.pk


async def test_post_person(aclient, person, disease):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
        "phone": "+111111111",
        "languages": ["en", "fr"],
        "country": "FR",
        "cell_phone": "+611111111",
        "diseases": [disease.pk],
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["pk"] is not None
    assert body["first_name"] == "Masanobu"
    assert "fr" in body["languages"]
    assert body["diseases"] == [
        {
            "name": "Congenital deficiency in alpha-fetoprotein",
            "label": "Congenital deficiency in alpha-fetoprotein",
            "resource": "disease",
            "orpha_number": 12345,
            "pk": disease.pk,
            "depth": 0,
            "max_depth": 8,
            "group_of_type": "disorder",
        }
    ]
    new = Person.get(body["pk"])
    assert new.created_by == person
    assert new.modified_by == person


@pytest.mark.xfail
async def test_post_person_with_event(aclient, event):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
        "phone": "+111111111",
        "languages": ["en", "fr"],
        "country": "FR",
        "cell_phone": "+611111111",
        "events": [{"event": event.pk, "role": "Staff"}],
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["events"] == [
        {
            "event": {
                "duration": 8,
                "label": "Royal Gala (19/11/2018)",
                "kind": "awards",
                "location": "Bruxelles",
                "name": "Royal Gala",
                "pk": event.pk,
                "resource": "event",
                "start_date": "2018-11-19",
            },
            "role": "Staff",
        }
    ]


async def test_can_post_person_without_email(aclient):
    body = {"title": "mr", "first_name": "Masanobu", "last_name": "Fukuoka"}
    resp = await aclient.post("/person", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    resp = await aclient.get(f"/person/{body['pk']}", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["pk"] is not None
    assert body["first_name"] == "Masanobu"
    assert body["email"] is None
    # We should be able to create more than one user without email.
    resp = await aclient.post("/person", body={"last_name": "Mister X"})
    assert resp.status == 200


async def test_can_post_person_with_null_email(aclient):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": None,
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    resp = await aclient.get(f"/person/{body['pk']}", body=body)
    body = json.loads(resp.body)
    assert body["pk"] is not None
    assert body["first_name"] == "Masanobu"
    assert body["email"] is None
    # We should be able to create more than one user with null email.
    resp = await aclient.post("/person", body={"last_name": "Mister X", "email": None})
    assert resp.status == 200


async def test_can_post_person_with_only_last_name(aclient):
    body = {"last_name": "Fukuoka"}
    resp = await aclient.post("/person", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    resp = await aclient.get(f"/person/{body['pk']}", body=body)
    body = json.loads(resp.body)
    assert body["pk"] is not None
    assert body["last_name"] == "Fukuoka"
    assert body["first_name"] is None
    assert body["email"] is None
    # We should be able to create more than one user with empty email.
    resp = await aclient.post("/person", body={"last_name": "Mister X", "email": ""})
    assert resp.status == 200


async def test_can_post_person_with_phone_as_string(aclient):
    body = {"last_name": "Fukuoka", "cell_phone": 123_456_789}
    resp = await aclient.post("/person", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["cell_phone"] == "123456789"


async def test_cannot_post_person_with_pk(aclient, person):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
        "pk": person.pk,
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 422
    body = json.loads(resp.body)
    assert body == {"error": "Unrecognized field name 'pk'"}
    dbperson = Person.get(person.pk)
    assert dbperson.first_name == person.first_name
    assert dbperson.last_name == person.last_name


async def test_put_person(aclient, person):
    body = {
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
    }
    resp = await aclient.put(f"/person/{person.pk}", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["pk"] == person.pk
    assert body["first_name"] == "Masanobu"
    assert person.title
    assert not body["title"]


async def test_put_person_with_keywords(aclient, person, keyword):
    body = {
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
        "keywords": [keyword.pk],
    }
    resp = await aclient.put(f"/person/{person.pk}", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["pk"] == person.pk
    assert body["first_name"] == "Masanobu"
    assert person.title
    assert body["keywords"] == [keyword.as_relation()]


async def test_put_person_with_invalid_keywords(aclient, person):
    body = {
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
        "keywords": ["invalid"],
    }
    resp = await aclient.put(f"/person/{person.pk}", body=body)
    assert resp.status == 422
    body = json.loads(resp.body)


async def test_put_person_with_org_only_keyword(aclient, person, keyword):
    keyword.for_person = False
    keyword.save()
    body = {
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
        "keywords": [keyword.pk],
    }
    resp = await aclient.put(f"/person/{person.pk}", body=body)
    assert resp.status == 422
    body = json.loads(resp.body)
    assert body == {"error": "Invalid keyword for Person: `reviewed`"}


async def test_put_person_with_dbrefs(aclient, person, disease, organisation):
    assert not person.diseases
    body = {
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
        "diseases": [disease.pk],
    }
    resp = await aclient.put(f"/person/{person.pk}", body=body)
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["diseases"] == [
        {
            "name": "Congenital deficiency in alpha-fetoprotein",
            "label": "Congenital deficiency in alpha-fetoprotein",
            "resource": "disease",
            "orpha_number": 12345,
            "pk": 12345,
            "depth": 0,
            "max_depth": 8,
            "group_of_type": "disorder",
        }
    ]


async def test_patch_person(aclient, disease, organisation, person2, event):
    person2.first_name = "Mike"
    person2.comment = "delete me"
    person2.save()
    assert not person2.diseases
    body = {
        "last_name": "Fukuoka",
        "email": "masanobu@somewhere.org",
        "diseases": [disease.pk],
        "comment": None,
    }
    resp = await aclient.patch(f"/person/{person2.pk}", body=body)
    assert resp.status == 200
    person = Person.get(person2.pk)
    body = json.loads(resp.body)
    assert body == person.as_resource()
    assert person.first_name == "Mike"
    assert person.diseases == [disease]
    assert person.email == "masanobu@somewhere.org"
    assert person.comment is None


async def test_patch_person_roles(aclient, person, organisation):
    Role.get(organisation=organisation, person=person).delete()
    person.roles = [
        Role(
            organisation=organisation,
            person=person,
            is_default=True,
            is_primary=False,
            is_archived=True,
            email="blah@blah.org",
        )
    ]
    person.save()
    body = {
        "roles": [
            {
                "organisation": organisation.pk,
                "email": None,
                "name": "CTO",
                "is_default": False,
                "is_voting": False,
                "is_primary": True,
                "is_archived": True,
            }
        ]
    }
    resp = await aclient.patch(f"/person/{person.pk}", body=body)
    assert resp.status == 200
    role = Role.get(organisation=organisation, person=person)
    assert role.name == "CTO"
    assert not role.is_default
    assert role.is_primary
    assert role.email is None


async def test_patch_person_updates_byat(client, organisation, person):
    person.modified_at = utcnow() - timedelta(hours=3)
    person.save(ghost=True)
    other = Person(last_name="New").save()
    client.login(other)
    resp = await client.patch(f"/person/{person.pk}", body={"email": "new@new.org"})
    assert resp.status == 200
    updated = Person.get(person.pk)
    assert updated.email == "new@new.org"
    assert updated.modified_by == other
    assert updated.created_by == person
    assert updated.created_at == person.created_at
    assert updated.modified_at != person.modified_at
    client.logout()


async def test_put_person_with_invalid_languages(aclient, person):
    body = {
        "country": None,
        "email": "miss.vincent@somewhere.com",
        "first_name": "Miss",
        "languages": ["unknown"],
        "last_name": "VINCENT",
        "phone": "123456789",
        "title": "ms",
    }
    resp = await aclient.put(f"/person/{person.pk}", body=body)
    assert resp.status == 422
    assert "languages" in json.loads(resp.body)["errors"]


async def test_get_person_history(aclient, person):
    resp = await aclient.get(f"/person/{person.pk}/history")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "data": [
            {
                "at": person.versions[0]["at"].isoformat(),
                "by": {
                    "country": "FR",
                    "email": "bill@somewhere.org",
                    "first_name": "Bill",
                    "label": "Bill Mollison",
                    "last_name": "Mollison",
                    "pk": person.pk,
                    "resource": "person",
                    "uri": f"/person/{person.pk}",
                },
                "diff": {
                    "cell_phone": {
                        "new": "+611111111",
                        "subject": "cell_phone",
                        "verb": "create",
                    },
                    "country": {
                        "new": "FR",
                        "subject": "country",
                        "verb": "create",
                    },
                    "email": {
                        "new": "bill@somewhere.org",
                        "subject": "email",
                        "verb": "create",
                    },
                    "first_name": {
                        "new": "Bill",
                        "subject": "first_name",
                        "verb": "create",
                    },
                    "languages": [
                        {"subject": "English", "value": "en", "verb": "create"},
                        {"subject": "French", "value": "fr", "verb": "create"},
                    ],
                    "last_name": {
                        "new": "Mollison",
                        "subject": "last_name",
                        "verb": "create",
                    },
                    "phone": {
                        "new": "+9876543210",
                        "subject": "phone",
                        "verb": "create",
                    },
                    "title": {"new": "mr", "subject": "title", "verb": "create"},
                },
                "of": {
                    "label": "Bill Mollison",
                    "pk": person.pk,
                    "resource": "person",
                    "uri": f"/person/{person.pk}",
                },
                "action": "create",
            },
        ]
    }
    person.email = "another@email.org"
    person.save()
    resp = await aclient.get(f"/person/{person.pk}/history")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "data": [
            {
                "at": person.versions[0]["at"].isoformat(),
                "by": {
                    "country": "FR",
                    "email": "another@email.org",
                    "first_name": "Bill",
                    "label": "Bill Mollison",
                    "last_name": "Mollison",
                    "pk": person.pk,
                    "resource": "person",
                    "uri": f"/person/{person.pk}",
                },
                "diff": {
                    "email": {
                        "new": "another@email.org",
                        "old": "bill@somewhere.org",
                        "subject": "email",
                        "verb": "update",
                    }
                },
                "of": {
                    "label": "Bill Mollison",
                    "pk": person.pk,
                    "resource": "person",
                    "uri": f"/person/{person.pk}",
                },
                "action": "update",
            },
            {
                "at": person.versions[1]["at"].isoformat(),
                "by": {
                    "country": "FR",
                    "email": "another@email.org",
                    "first_name": "Bill",
                    "label": "Bill Mollison",
                    "last_name": "Mollison",
                    "pk": person.pk,
                    "resource": "person",
                    "uri": f"/person/{person.pk}",
                },
                "diff": {
                    "cell_phone": {
                        "new": "+611111111",
                        "subject": "cell_phone",
                        "verb": "create",
                    },
                    "country": {"new": "FR", "subject": "country", "verb": "create"},
                    "email": {
                        "new": "bill@somewhere.org",
                        "subject": "email",
                        "verb": "create",
                    },
                    "first_name": {
                        "new": "Bill",
                        "subject": "first_name",
                        "verb": "create",
                    },
                    "languages": [
                        {"subject": "English", "value": "en", "verb": "create"},
                        {"subject": "French", "value": "fr", "verb": "create"},
                    ],
                    "last_name": {
                        "new": "Mollison",
                        "subject": "last_name",
                        "verb": "create",
                    },
                    "phone": {
                        "new": "+9876543210",
                        "subject": "phone",
                        "verb": "create",
                    },
                    "title": {"new": "mr", "subject": "title", "verb": "create"},
                },
                "of": {
                    "label": "Bill Mollison",
                    "pk": person.pk,
                    "resource": "person",
                    "uri": f"/person/{person.pk}",
                },
                "action": "create",
            },
        ]
    }


async def test_get_person_history_with_event(aclient, person, role):
    test_event = Event(
        name="History Test Event",
        start_date=date(2018, 11, 19),
        duration=8,
        location="Paris"
    ).save()
    role.events.append(RoleToEvent(event=test_event))
    role.save()
    resp = await aclient.get(f"/person/{person.pk}/history")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "data": [
            {
                'by': {
                    'pk': person.pk,
                    'first_name': 'Bill',
                    'last_name': 'Mollison',
                    'email': 'bill@somewhere.org',
                    'uri': f'/person/{person.pk}',
                    'resource': 'person',
                    'label': 'Bill Mollison',
                    'country': 'FR'
                },
                'of': {
                    'label': 'Bill Mollison',
                    'pk': person.pk,
                    'resource': 'person',
                    'uri': f'/person/{person.pk}'
                },
                'at': person.versions[0]["at"].isoformat(),
                'diff': {
                    'roles': [
                        {
                            'subject': 'Bill Org',
                            'verb': 'update',
                            'subordinates': [
                                {
                                    'subject': 'events',
                                    'verb': 'update',
                                    'old': [],
                                    'new': [
                                        {
                                            'event': test_event.pk,
                                            'event_name': 'History Test Event'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                'action': 'update'
            },
            {
                'by': {
                    'pk': person.pk,
                    'first_name': 'Bill',
                    'last_name': 'Mollison',
                    'email': 'bill@somewhere.org',
                    'uri': f'/person/{person.pk}',
                    'resource': 'person',
                    'label': 'Bill Mollison',
                    'country': 'FR'
                },
                'of': {
                    'label': 'Bill Mollison',
                    'pk': person.pk,
                    'resource': 'person',
                    'uri': f'/person/{person.pk}'
                },
                'at': person.versions[1]["at"].isoformat(),
                'diff': {
                    'roles': [
                        {
                            'subject': 'Bill Org',
                            'verb': 'create',
                            'new': {
                                'organisation': role.organisation.pk,
                                'organisation_name': 'Bill Org',
                                'name': 'CEO',
                                'email': 'bmollison@bill.org',
                                'phone': '+1234567890',
                                'is_primary': True,
                                'is_default': False,
                                'is_voting': False,
                                'is_archived': False,
                                'groups': [],
                                'events': []
                            }
                        }
                    ]
                },
                'action': 'update'
            },
            {
                "at": person.versions[2]["at"].isoformat(),
                "by": {
                    "country": "FR",
                    "email": "bill@somewhere.org",
                    "first_name": "Bill",
                    "label": "Bill Mollison",
                    "last_name": "Mollison",
                    "pk": person.pk,
                    "resource": "person",
                    "uri": f"/person/{person.pk}",
                },
                "diff": {
                    "cell_phone": {
                        "new": "+611111111",
                        "subject": "cell_phone",
                        "verb": "create",
                    },
                    "country": {
                        "new": "FR",
                        "subject": "country",
                        "verb": "create",
                    },
                    "email": {
                        "new": "bill@somewhere.org",
                        "subject": "email",
                        "verb": "create",
                    },
                    "first_name": {
                        "new": "Bill",
                        "subject": "first_name",
                        "verb": "create",
                    },
                    "languages": [
                        {"subject": "English", "value": "en", "verb": "create"},
                        {"subject": "French", "value": "fr", "verb": "create"},
                    ],
                    "last_name": {
                        "new": "Mollison",
                        "subject": "last_name",
                        "verb": "create",
                    },
                    "phone": {
                        "new": "+9876543210",
                        "subject": "phone",
                        "verb": "create",
                    },
                    "title": {"new": "mr", "subject": "title", "verb": "create"},
                },
                "of": {
                    "label": "Bill Mollison",
                    "pk": person.pk,
                    "resource": "person",
                    "uri": f"/person/{person.pk}",
                },
                "action": "create",
            },
        ]
    }


async def test_get_roles(aclient, organisation, person, person2, group):
    Role(
        person=person2,
        organisation=organisation,
        groups=[GroupBelonging(group=group, role="Chair")],
    ).save()
    resp = await aclient.get(f"/person/{person.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data["roles"] == [
        {
            "email": "bmollison@bill.org",
            "groups": [],
            "events": [],
            "is_primary": True,
            "is_default": False,
            "is_voting": False,
            "is_archived": False,
            "name": "CEO",
            "organisation": {
                "country": "BE",
                "kind": 60,
                "label": "Bill Org",
                "name": "Bill Org",
                "pk": "2",
                "resource": "organisation",
                "uri": "/organisation/2",
            },
            "person": {
                "country": "FR",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": "1",
                "resource": "person",
                "uri": "/person/1",
            },
            "phone": "+1234567890",
        }
    ]
    resp = await aclient.get(f"/person/{person2.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data["roles"] == [
        {
            "email": None,
            "groups": [
                {
                    "group": {
                        "description": None,
                        "kind": None,
                        "label": "CNA",
                        "name": "CNA",
                        "pk": "1",
                        "resource": "group",
                    },
                    "is_active": True,
                    "name": "CNA",
                    "role": "Chair",
                }
            ],
            "events": [],
            "is_primary": False,
            "is_default": False,
            "is_voting": False,
            "is_archived": False,
            "name": None,
            "organisation": {
                "country": "BE",
                "kind": 60,
                "label": "Bill Org",
                "name": "Bill Org",
                "pk": "2",
                "resource": "organisation",
                "uri": "/organisation/2",
            },
            "person": {
                "country": "AU",
                "email": "david@somewhere.org",
                "first_name": "David",
                "label": "David Holmgren",
                "last_name": "Holmgren",
                "pk": "3",
                "resource": "person",
                "uri": "/person/3",
            },
            "phone": None,
        }
    ]
    resp = await aclient.get(f"/organisation/{organisation.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data["roles"] == [
        {
            "email": "bmollison@bill.org",
            "groups": [],
            "events": [],
            "is_primary": True,
            "is_default": False,
            "is_voting": False,
            "is_archived": False,
            "name": "CEO",
            "organisation": {
                "country": "BE",
                "kind": 60,
                "label": "Bill Org",
                "name": "Bill Org",
                "pk": "2",
                "resource": "organisation",
                "uri": "/organisation/2",
            },
            "person": {
                "country": "FR",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": "1",
                "resource": "person",
                "uri": "/person/1",
            },
            "phone": "+1234567890",
        },
        {
            "email": None,
            "events": [],
            "groups": [
                {
                    "group": {
                        "description": None,
                        "kind": None,
                        "label": "CNA",
                        "name": "CNA",
                        "pk": "1",
                        "resource": "group",
                    },
                    "is_active": True,
                    "name": "CNA",
                    "role": "Chair",
                }
            ],
            "is_primary": False,
            "is_default": False,
            "is_voting": False,
            "is_archived": False,
            "name": None,
            "organisation": {
                "country": "BE",
                "kind": 60,
                "label": "Bill Org",
                "name": "Bill Org",
                "pk": "2",
                "resource": "organisation",
                "uri": "/organisation/2",
            },
            "person": {
                "country": "AU",
                "email": "david@somewhere.org",
                "first_name": "David",
                "label": "David Holmgren",
                "last_name": "Holmgren",
                "pk": "3",
                "resource": "person",
                "uri": "/person/3",
            },
            "phone": None,
        },
    ]


async def test_post_person_with_duplicate_email(aclient, person):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": person.email,
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 422


async def test_post_person_with_invalid_email(aclient, person):
    assert Person.objects.count() == 1
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "invalid.email.it",
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 422
    assert Person.objects.count() == 1
    assert json.loads(resp.body) == {
        "error": "Invalid email address: invalid.email.it",
        "errors": {"email": "Invalid email address: invalid.email.it"},
    }


async def test_post_person_with_non_dict_body(aclient, person):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": person.email,
    }
    resp = await aclient.post("/person", body=[body])
    assert resp.status == 400


async def test_post_person_with_invalid_field_names(aclient, person):
    body = {
        "titlez": "mr",
        "first_namez": "Masanobu",
        "last_namez": "Fukuoka",
        "emailz": person.email,
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 422


async def test_post_person_with_created_at(aclient, person):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": person.email,
        "created_at": utcnow().isoformat(),
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 422
    assert json.loads(resp.body) == {
        "error": "not an editable field",
        "errors": {"created_at": "not an editable field"},
    }


async def test_post_person_with_created_by(aclient, person):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": person.email,
        "created_by": person.pk,
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 422
    assert json.loads(resp.body) == {
        "error": "not an editable field",
        "errors": {"created_by": "not an editable field"},
    }


async def test_post_person_with_null_body(aclient, person):
    resp = await aclient.post("/person", body=None)
    assert resp.status == 400


async def test_post_person_with_empty_body(aclient, person):
    resp = await aclient.post("/person", body="{}")
    assert resp.status == 422


async def test_post_person_with_duplicate_email_but_different_case(aclient, person):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": person.email.upper(),
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 422


async def test_post_person_should_lower_case_email(aclient):
    body = {
        "title": "mr",
        "first_name": "Masanobu",
        "last_name": "Fukuoka",
        "email": "UPPER@CasE.oRg",
    }
    resp = await aclient.post("/person", body=body)
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data["email"] == "upper@case.org"
    assert Person.objects.filter(email="upper@case.org").count() == 1


async def test_delete_person(aclient, staff, person2, organisation):
    Role(person=person2, is_primary=True, organisation=organisation).save()
    assert len(organisation.roles) == 2
    resp = await aclient.delete(f"/person/{person2.pk}")
    assert resp.status == 204
    assert len(organisation.roles) == 1


async def test_cannot_delete_person_liked_to_other(aclient, staff, person, person2):
    person2.created_by = person
    person2.save()
    resp = await aclient.delete(f"/person/{person.pk}")
    assert resp.status == 422
    assert Person.get(id=person.pk)


async def test_cannot_delete_person_liked_to_organisation(
    aclient, person, staff, organisation
):
    organisation.created_by = person
    organisation.save()
    resp = await aclient.delete(f"/person/{person.pk}")
    assert resp.status == 422
    assert Person.get(id=person.pk)


async def test_cannot_delete_person_liked_to_event(aclient, person, staff, event):
    event.created_by = person
    event.save()
    resp = await aclient.delete(f"/person/{person.pk}")
    assert resp.status == 422
    assert Person.get(id=person.pk)


async def test_get_organisation(aclient, person, person2, person3, organisation):
    Role(name="Test Role", person=person3.pk, organisation=organisation).save()
    Role(name="Test Role", person=person2.pk, organisation=organisation).save()
    person4 = Person(title="mr", first_name="First", last_name="Last", phone="+33636283745", country="FR")
    Role(person=person4, organisation=organisation, is_archived=True).save()
    resp = await aclient.get(f"/organisation/{organisation.pk}")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "pk": organisation.pk,
        "name": "Bill Org",
        "resource": "organisation",
        "kind": 60,
        "label": "Bill Org",
        "uri": f"/organisation/{organisation.pk}",
        "filemaker_pk": None,
        "roles": [
            {
                "name": "CEO",
                "is_primary": True,
                "is_default": False,
                "is_voting": False,
                "is_archived": False,
                "person": {
                    "country": "FR",
                    "pk": person.pk,
                    "email": "bill@somewhere.org",
                    "resource": "person",
                    "label": "Bill Mollison",
                    "first_name": "Bill",
                    "last_name": "Mollison",
                    "uri": f"/person/{person.pk}",
                },
                "organisation": {
                    "country": "BE",
                    "name": "Bill Org",
                    "pk": organisation.pk,
                    "uri": f"/organisation/{organisation.pk}",
                    "resource": "organisation",
                    "kind": 60,
                    "label": "Bill Org",
                },
                "email": "bmollison@bill.org",
                "phone": "+1234567890",
                "groups": [],
                "events": [],
            },
            {
                "name": "Test Role",
                "is_primary": False,
                "is_default": False,
                "is_voting": False,
                "is_archived": False,
                "person": {
                    "country": "AU",
                    "pk": person2.pk,
                    "email": "david@somewhere.org",
                    "resource": "person",
                    "label": "David Holmgren",
                    "first_name": "David",
                    "last_name": "Holmgren",
                    "uri": f"/person/{person2.pk}",
                },
                "organisation": {
                    "country": "BE",
                    "name": "Bill Org",
                    "pk": organisation.pk,
                    "uri": f"/organisation/{organisation.pk}",
                    "resource": "organisation",
                    "kind": 60,
                    "label": "Bill Org",
                },
                "email": None,
                "phone": None,
                "groups": [],
                "events": [],
            },
            {
                "name": "Test Role",
                "is_primary": False,
                "is_default": False,
                "is_voting": False,
                "is_archived": False,
                "person": {
                    "country": "FR",
                    "pk": person3.pk,
                    "email": None,
                    "resource": "person",
                    "label": "Patrick Morel",
                    "first_name": "Patrick",
                    "last_name": "Morel",
                    "uri": f"/person/{person3.pk}",
                },
                "organisation": {
                    "country": "BE",
                    "name": "Bill Org",
                    "pk": organisation.pk,
                    "uri": f"/organisation/{organisation.pk}",
                    "resource": "organisation",
                    "kind": 60,
                    "label": "Bill Org",
                },
                "email": None,
                "phone": None,
                "groups": [],
                "events": [],
            }
        ],
        "acronym": None,
        "address": None,
        "postcode": None,
        "city": None,
        "country": "BE",
        "phone": None,
        "cell_phone": None,
        "email": "contact@bill.org",
        "website": None,
        "diseases": [],
        "keywords": [],
        "groupings": [],
        "attachments": [],
        "created_at": organisation.created_at.isoformat(),
        "modified_at": organisation.modified_at.isoformat(),
        "created_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "modified_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "comment": None,
        "membership": [],
        "adherents": None,
        "creation_year": None,
        "budget": None,
        "board": None,
        "paid_staff": None,
        "pharma_funding": None,
    }


async def test_post_organisation(aclient):
    resp = await aclient.post("/organisation", body={"name": "Fukuoka Inc."})
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["pk"] is not None
    assert body["name"] == "Fukuoka Inc."


async def test_post_organisation_without_name(aclient):
    resp = await aclient.post("/organisation", body={"other": "Fukuoka Inc."})
    assert resp.status == 422
    assert not Organisation.objects.count()


async def test_put_organisation(aclient, organisation):
    resp = await aclient.put(
        f"/organisation/{organisation.pk}", body={"name": "Fukuoka Inc."}
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["pk"] == organisation.pk
    assert body["name"] != organisation.name
    assert body["name"] == "Fukuoka Inc."


async def test_put_organisation_w_person_only_keyword(aclient, organisation, keyword):
    keyword.for_organisation = False
    keyword.save()
    resp = await aclient.put(
        f"/organisation/{organisation.pk}",
        body={"name": "Fukuoka Inc.", "keywords": [keyword.pk]},
    )
    assert resp.status == 422
    body = json.loads(resp.body)
    assert body == {"error": "Invalid keyword for Organisation: `reviewed`"}


async def test_put_organisation_with_group(aclient, organisation, group, person):
    assert len(organisation.roles) == 1
    resp = await aclient.put(
        f"/organisation/{organisation.pk}",
        body={
            "name": "Fukuoka Inc.",
            "roles": [
                {"groups": [{"group": group.pk, "role": "Member"}], "person": person.pk}
            ],
        },
    )
    assert resp.status == 200
    assert len(Organisation.get(organisation.pk).roles) == 1
    body = json.loads(resp.body)
    assert body["pk"] == organisation.pk
    assert body["name"] != organisation.name
    assert body["name"] == "Fukuoka Inc."
    assert body["roles"][0]["groups"] == [
        {
            "is_active": True,
            "name": "CNA",
            "role": "Member",
            "group": {
                "label": "CNA",
                "name": "CNA",
                "pk": "1",
                "resource": "group",
                "description": None,
                "kind": None,
            },
        }
    ]


async def test_post_organisation_with_group(aclient, group, person):
    resp = await aclient.post(
        "/organisation",
        body={
            "name": "Fukuoka Inc.",
            "roles": [
                {"groups": [{"group": group.pk, "role": "Member"}], "person": person.pk}
            ],
        },
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    organisation = Organisation.get(data["pk"])
    assert len(data["roles"]) == 1
    assert len(organisation.roles) == 1
    assert data["pk"] == organisation.pk
    assert data["name"] == "Fukuoka Inc."
    assert data["roles"][0]["groups"] == [
        {
            "is_active": True,
            "name": "CNA",
            "role": "Member",
            "group": {
                "label": "CNA",
                "name": "CNA",
                "pk": "1",
                "resource": "group",
                "description": None,
                "kind": None,
            },
        }
    ]


async def test_put_organisation_with_bad_group(aclient, organisation, person):
    assert len(Organisation.get(organisation.pk).roles) == 1
    resp = await aclient.put(
        f"/organisation/{organisation.pk}",
        body={
            "name": "Fukuoka Inc.",
            "roles": [
                {"person": person.pk, "groups": [{"group": "bad", "role": "Member"}]}
            ],
        },
    )
    assert resp.status == 422
    body = json.loads(resp.body)
    assert "group" in body["error"]
    assert len(Organisation.get(organisation.pk).roles) == 1


async def test_put_organisation_with_empty_group(aclient, organisation, person):
    assert len(Organisation.get(organisation.pk).roles) == 1
    resp = await aclient.put(
        f"/organisation/{organisation.pk}",
        body={
            "name": "Fukuoka Inc.",
            "roles": [{"person": person.pk, "groups": [{"role": "Member"}]}],
        },
    )
    assert resp.status == 422
    body = json.loads(resp.body)
    assert "groups" in body["errors"]
    assert len(Organisation.get(organisation.pk).roles) == 1


async def test_post_organisation_with_membership(aclient, organisation):
    resp = await aclient.post(
        "/organisation",
        body={
            "name": "Fukuoka Inc.",
            "membership": [
                {
                    "organisation": organisation.as_relation(),
                    "status": "f",
                    "start_date": "2019-01-12",
                    "fees": [
                        {"year": 2019, "payment_date": "2019-01-12", "amount": 70}
                    ],
                }
            ],
        },
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["name"] == "Fukuoka Inc."
    assert body["membership"] == [
        {
            "organisation": {
                "country": "BE",
                "kind": 60,
                "label": "Bill Org",
                "name": "Bill Org",
                "pk": organisation.pk,
                "resource": "organisation",
                "uri": f"/organisation/{organisation.pk}",
            },
            "status": "f",
            "start_date": "2019-01-12",
            "end_date": None,
            "fees": [
                {
                    "year": 2019,
                    "payment_date": "2019-01-12",
                    "amount": 70,
                    "exempted": False,
                    "receipt_sent": False,
                }
            ],
        }
    ]


async def test_post_organisation_with_membership_as_int(aclient, organisation):
    resp = await aclient.post(
        "/organisation",
        body={
            "name": "Fukuoka Inc.",
            "membership": [
                {
                    "organisation": int(organisation.pk),
                    "status": "f",
                    "start_date": "2019-01-12",
                    "fees": [
                        {"year": 2019, "payment_date": "2019-01-12", "amount": 70}
                    ],
                }
            ],
        },
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["name"] == "Fukuoka Inc."
    assert body["membership"][0]["organisation"]["pk"] == organisation.pk


async def test_cannot_post_organisation_duplicate_membership(aclient, organisation):
    resp = await aclient.post(
        "/organisation",
        body={
            "name": "Fukuoka Inc.",
            "membership": [
                {
                    "organisation": int(organisation.pk),
                    "status": "a",
                    "start_date": "2019-01-12",
                },
                {
                    "organisation": int(organisation.pk),
                    "status": "f",
                    "start_date": "2009-01-12",
                },
            ],
        },
    )
    assert resp.status == 422


async def test_patch_organisation(aclient, organisation, group, person):
    Role.get(organisation=organisation, person=person).delete()
    assert not len(Organisation.get(organisation.pk).roles)
    resp = await aclient.patch(
        f"/organisation/{organisation.pk}",
        body={
            "roles": [
                {"person": person.pk, "groups": [{"group": group.pk, "role": "Member"}]}
            ]
        },
    )
    assert resp.status == 200
    assert len(Organisation.get(organisation.pk).roles) == 1
    body = json.loads(resp.body)
    assert body["pk"] == organisation.pk
    assert body["name"] == organisation.name
    assert body["roles"][0]["person"]["pk"] == person.pk
    assert body["roles"][0]["groups"] == [
        {
            "is_active": True,
            "name": "CNA",
            "role": "Member",
            "group": {
                "label": "CNA",
                "name": "CNA",
                "pk": group.pk,
                "resource": "group",
                "description": None,
                "kind": None,
            },
        }
    ]


async def test_patch_organisation_updates_byat(client, person, organisation):
    organisation.modified_at = utcnow() - timedelta(hours=3)
    organisation.save(ghost=True)
    other = Person(last_name="New").save()
    client.login(other)
    resp = await client.patch(
        f"/organisation/{organisation.pk}", body={"email": "new@new.org"}
    )
    assert resp.status == 200
    updated = Organisation.get(organisation.pk)
    assert updated.email == "new@new.org"
    assert updated.modified_by == other
    assert updated.created_by == person
    assert updated.created_at == organisation.created_at
    assert updated.modified_at != organisation.modified_at


async def test_delete_organisation(aclient, staff, organisation):
    resp = await aclient.delete(f"/organisation/{organisation.pk}")
    assert resp.status == 204


async def test_get_event(aclient, event):
    resp = await aclient.get(f"/event/{event.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "duration": 8,
        "label": "Royal Gala (19/11/2018)",
        "location": "Bruxelles",
        "name": "Royal Gala",
        "pk": event.pk,
        "resource": "event",
        "start_date": "2018-11-19",
        "kind": "awards",
        "created_at": event.created_at.isoformat(),
        "modified_at": event.modified_at.isoformat(),
        "created_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "modified_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
    }


async def test_post_event(aclient):
    resp = await aclient.post(
        "/event",
        body={
            "duration": 6,
            "name": "Royal Dinner",
            "start_date": "2018-10-20",
            "kind": "awards",
            "location": "Barcelona",
        },
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["name"] == "Royal Dinner"
    assert body["start_date"] == "2018-10-20"
    event = Event.objects.get(pk=body["pk"])
    assert event.as_resource() == body


async def test_put_event(aclient, event):
    resp = await aclient.put(
        f"/event/{event.pk}",
        body={
            "duration": 6,
            "name": "Royal Gala",
            "start_date": "2018-11-20",
            "kind": "awards",
            "location": "Bruxelles",
        },
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["duration"] == 6
    assert body["start_date"] == "2018-11-20"


async def test_put_event_with_missing_field(aclient, event):
    resp = await aclient.put(
        f"/event/{event.pk}",
        body={
            "duration": 6,
            "name": "Royal Gala",
            "start_date": "2018-11-20",
            "kind": "awards",
        },
    )
    assert resp.status == 422
    body = json.loads(resp.body)
    assert body == {
        "error": "Field is required",
        "errors": {"location": "Field is required"},
    }


async def test_delete_event(aclient, role, staff, event):
    role.events.append(RoleToEvent(event=event, role="Speaker"))
    role.save()
    assert Role.get(person=role.person, organisation=role.organisation).events
    resp = await aclient.delete(f"/event/{event.pk}")
    assert resp.status == 204
    assert not Role.get(person=role.person, organisation=role.organisation).events


async def test_get_group(aclient, person, group):
    resp = await aclient.get(f"/group/{group.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "created_at": group.created_at.isoformat(),
        "created_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "label": "CNA",
        "modified_at": group.modified_at.isoformat(),
        "modified_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "name": "CNA",
        "pk": group.pk,
        "resource": "group",
        "description": None,
        "kind": None,
    }


async def test_get_group_list(aclient, person, group):
    resp = await aclient.get(f"/group")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "label": "CNA",
                "name": "CNA",
                "pk": "1",
                "resource": "group",
                "description": None,
                "kind": None,
            }
        ]
    }


async def test_post_group(aclient):
    resp = await aclient.post(
        "/group",
        body={
            "name": "Meditation Group",
            "description": "My 10 Tations",
        },
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["name"] == "Meditation Group"
    group = Group.objects.get(pk=body["pk"])
    assert group.as_resource() == body
    assert group.description == "My 10 Tations"


async def test_put_group(aclient, group):
    resp = await aclient.put(
        f"/group/{group.pk}",
        body={"name": "Royal Group"},
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["name"] == "Royal Group"
    group = Group.objects.get(pk=body["pk"])
    assert group.as_resource() == body


async def test_delete_group(aclient, person, group, staff, organisation):
    role = person.roles[0]
    role.groups.append(GroupBelonging(group=group, role="Member"))
    role.save()
    assert Person.get(person.pk).roles[0].groups
    resp = await aclient.delete(f"/group/{group.pk}")
    assert resp.status == 204
    assert not Person.get(person.pk).roles[0].groups


async def test_get_keyword(aclient, person, keyword):
    resp = await aclient.get(f"/keyword/{keyword.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "created_at": keyword.created_at.isoformat(),
        "created_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "label": "reviewed",
        "modified_at": keyword.modified_at.isoformat(),
        "modified_by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Mollison",
            "last_name": "Mollison",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "name": "reviewed",
        "pk": keyword.pk,
        "resource": "keyword",
        "for_person": True,
        "for_organisation": True,
        "kind": None,
    }


async def test_get_keyword_list(aclient, person, keyword):
    keyword2 = Keyword(name="deleted", for_organisation=False).save()
    resp = await aclient.get(f"/keyword")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "label": "deleted",
                "name": "deleted",
                "pk": keyword2.pk,
                "resource": "keyword",
                "for_person": True,
                "for_organisation": False,
                "kind": None,
            },
            {
                "label": "reviewed",
                "name": "reviewed",
                "pk": keyword.pk,
                "resource": "keyword",
                "for_person": True,
                "for_organisation": True,
                "kind": None,
            },
        ]
    }


async def test_post_keyword(aclient):
    resp = await aclient.post(
        "/keyword",
        body={"name": "society", "for_organisation": True, "for_person": False},
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["name"] == "society"
    assert body["for_person"] is False
    keyword = Keyword.get(body["pk"])
    assert keyword.as_resource() == body


async def test_put_keyword(aclient, keyword):
    resp = await aclient.put(
        f"/keyword/{keyword.pk}",
        body={"name": "deleted", "for_person": False, "for_organisation": True},
    )
    assert resp.status == 200
    body = json.loads(resp.body)
    assert body["name"] == "deleted"
    assert body["for_person"] is False
    keyword = keyword.get(body["pk"])
    assert keyword.as_resource() == body


async def test_delete_keyword(aclient, person, staff, keyword):
    person.keywords.append(keyword)
    person.save()
    assert Person.get(person.pk).keywords
    resp = await aclient.delete(f"/keyword/{keyword.pk}")
    assert resp.status == 204
    assert not Person.get(person.pk).keywords


async def test_complete_disease(aclient, disease):
    resp = await aclient.get("/disease/complete?q=feto")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"] == [
        {
            "name": "Congenital deficiency in alpha-fetoprotein",
            "label": "Congenital deficiency in alpha-fetoprotein",
            "resource": "disease",
            "orpha_number": 12345,
            "pk": disease.pk,
            "depth": 0,
            "max_depth": 8,
            "group_of_type": "disorder",
        }
    ]
    disease2 = Disease(
        name="Hereditary persistence of alpha-fetoprotein", orpha_number=12346
    ).save()
    resp = await aclient.get("/disease/complete?q=feto")
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert {
        "name": "Hereditary persistence of alpha-fetoprotein",
        "label": "Hereditary persistence of alpha-fetoprotein",
        "resource": "disease",
        "orpha_number": 12346,
        "pk": disease2.pk,
        "depth": 0,
        "max_depth": 0,
        "group_of_type": "disorder",
    } in data["data"]

    assert {
        "name": "Congenital deficiency in alpha-fetoprotein",
        "label": "Congenital deficiency in alpha-fetoprotein",
        "resource": "disease",
        "orpha_number": 12345,
        "pk": disease.pk,
        "depth": 0,
        "max_depth": 8,
        "group_of_type": "disorder",
    } in data["data"]
    resp = await aclient.get(f"/disease/complete?q=feto&limit=1")
    data = json.loads(resp.body)
    assert len(data["data"]) == 1


async def test_complete_contact(aclient, person, organisation):
    resp = await aclient.get("/contact/complete?q=bill")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert person.pk in [r["pk"] for r in data["data"]]
    assert organisation.pk in [r["pk"] for r in data["data"]]
    resp = await aclient.get(f"/contact/complete?q=bill&resource=person")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert person.pk in [r["pk"] for r in data["data"]]
    assert organisation.pk not in [r["pk"] for r in data["data"]]


async def test_complete_event(aclient, event):
    event.name = "Queen Gala Diner"
    event.save()
    event2 = Event(
        name="King Gala Breakfast",
        kind="awards",
        start_date="2019/03/02",
        duration=1,
        location="Brussels",
    ).save()
    resp = await aclient.get("/event/complete?q=gala")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert event.pk in [r["pk"] for r in data["data"]]
    assert event2.pk in [r["pk"] for r in data["data"]]
    resp = await aclient.get("/event/complete?q=queen")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == event.pk


async def test_complete_group(aclient, group):
    group.name = "Magical Group"
    group.save()
    group2 = Group(name="Magical People").save()
    resp = await aclient.get("/group/complete?q=magic")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 2
    assert group.pk in [r["pk"] for r in data["data"]]
    assert group2.pk in [r["pk"] for r in data["data"]]
    resp = await aclient.get("/group/complete?q=people")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert len(data["data"]) == 1
    assert data["data"][0]["pk"] == group2.pk


async def test_complete_grouping(aclient):
    resp = await aclient.get("/grouping/complete?q=cancer")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {"pk": "euracan", "label": "Adult Solid Cancer", "resource": "grouping"},
            {"pk": "paedcan", "label": "Paediatric cancer", "resource": "grouping"},
        ]
    }


async def test_complete_keyword(aclient, keyword):
    keyword.name = "RDI"
    keyword.for_person = False
    keyword.save()
    keyword2 = Keyword(name="RDI member", for_organisation=False).save()
    resp = await aclient.get("/keyword/complete?q=rdi")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "for_organisation": True,
                "for_person": False,
                "label": "RDI",
                "name": "RDI",
                "pk": keyword.pk,
                "resource": "keyword",
                "kind": None,
            },
            {
                "for_organisation": False,
                "for_person": True,
                "label": "RDI member",
                "name": "RDI member",
                "pk": keyword2.pk,
                "resource": "keyword",
                "kind": None,
            },
        ]
    }
    resp = await aclient.get("/keyword/complete?q=rdi&for_organisation=0")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "for_organisation": False,
                "for_person": True,
                "label": "RDI member",
                "name": "RDI member",
                "pk": keyword2.pk,
                "resource": "keyword",
                "kind": None,
            }
        ]
    }


async def test_complete_country(aclient):
    resp = await aclient.get("/country/complete?q=bel")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {"label": "Belgium", "pk": "BE", "resource": "country"},
            {"label": "Belarus", "pk": "BY", "resource": "country"},
            {"label": "Belize", "pk": "BZ", "resource": "country"},
        ]
    }


async def test_export(aclient, person, organisation, person2, keyword):
    organisation.name = "Bill Org, Inc."  # Add a comma.
    organisation.acronym = "YOLO"
    organisation.address = "Rue de la Reine"
    organisation.city = "Bruxelles"
    organisation.save()
    person.languages = ["fr", "it"]
    person.keywords = [keyword.pk]
    person.country = "FR"
    person.comment = "A comment"
    person.save()
    organisation2 = Organisation(
        name="Country Test Org",
        country="AU",
        kind=60,
    )
    organisation2.roles = [
        Role(
            name="CEO",
            is_primary=True,
            person=person.pk,
            email="ceo@countrytest.org"
        )
    ]
    organisation2.save()
    no_country_organisation = Organisation(
        name="No Country Test Org",
        kind=60,
    )
    no_country_organisation.roles = [
        Role(
            name="CEO",
            is_primary=True,
            person=person.pk,
            email="ceo@countrytest.org"
        )
    ]
    no_country_organisation.save()
    person.reload()
    resources = {
        "resources": [
            {"person": person.pk, "organisation": organisation.pk},
            {"person": person2.pk},
            {"organisation": organisation.pk},
            {"organisation": organisation2.pk},
            {"organisation": no_country_organisation.pk}
        ]
    }
    resp = await aclient.post("/contact/export.csv", body=resources)
    assert resp.status == 200
    assert resp.body.decode() == (
        """pk,first_name,last_name,email,title,phone,cell_phone,languages,country_label,country,diseases,keywords,groups,groupings,kind,role,is_primary,is_default,is_voting,organisation,acronym,eurordis_membership,rdi_membership,comment,website,address,postcode,city,created_by,created_at,modified_by,modified_at\r\n"""
        f"""{person.pk},Bill,Mollison,bmollison@bill.org,Mr,+9876543210,+611111111,"fr,it",France,FR,,reviewed,,,Patient Organisation,CEO,1,,,"Bill Org, Inc.",YOLO,,,A comment,,Rue de la Reine,,Bruxelles,{person.email},{person.created_at.isoformat()},{person.email},{person.modified_at.isoformat()}\r\n"""
        f"""{person2.pk},David,Holmgren,david@somewhere.org,Mr,+9876543234,,"de,en",Australia,AU,,,,,,,,,,,,,,,,,,,{person.email},{person2.created_at.isoformat()},{person.email},{person2.modified_at.isoformat()}\r\n"""
        f"""{organisation.pk},,,contact@bill.org,,,,,Belgium,BE,,,,,Patient Organisation,,,,,"Bill Org, Inc.",YOLO,,,,,Rue de la Reine,,Bruxelles,{person.email},{organisation.created_at.isoformat()},{person.email},{organisation.modified_at.isoformat()}\r\n"""
        f"""{organisation2.pk},Bill,Mollison,ceo@countrytest.org,Mr,+9876543210,+611111111,"fr,it",Australia,AU,,reviewed,,,Patient Organisation,CEO,1,,,Country Test Org,,,,A comment,,,,,{person.email},{organisation2.created_at.isoformat()},{person.email},{organisation2.modified_at.isoformat()}\r\n"""
        f"""{no_country_organisation.pk},Bill,Mollison,ceo@countrytest.org,Mr,+9876543210,+611111111,"fr,it",France,FR,,reviewed,,,Patient Organisation,CEO,1,,,No Country Test Org,,,,A comment,,,,,{person.email},{no_country_organisation.created_at.isoformat()},{person.email},{no_country_organisation.modified_at.isoformat()}\r\n"""
    )
    resp = await aclient.post("/contact/export.tsv", body=resources)
    assert resp.status == 200
    assert resp.body.decode() == (
        """pk\tfirst_name\tlast_name\temail\ttitle\tphone\tcell_phone\tlanguages\tcountry_label\tcountry\tdiseases\tkeywords\tgroups\tgroupings\tkind\trole\tis_primary\tis_default\tis_voting\torganisation\tacronym\teurordis_membership\trdi_membership\tcomment\twebsite\taddress\tpostcode\tcity\tcreated_by\tcreated_at\tmodified_by\tmodified_at\r\n"""
        f"""{person.pk}\tBill\tMollison\tbmollison@bill.org\tMr\t+9876543210\t+611111111\tfr,it\tFrance\tFR\t\treviewed\t\t\tPatient Organisation\tCEO\t1\t\t\tBill Org, Inc.\tYOLO\t\t\tA comment\t\tRue de la Reine\t\tBruxelles\t{person.email}\t{person.created_at.isoformat()}\t{person.email}\t{person.modified_at.isoformat()}\r\n"""
        f"""{person2.pk}\tDavid\tHolmgren\tdavid@somewhere.org\tMr\t+9876543234\t\tde,en\tAustralia\tAU\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{person.email}\t{person2.created_at.isoformat()}\t{person.email}\t{person2.modified_at.isoformat()}\r\n"""
        f"""{organisation.pk}\t\t\tcontact@bill.org\t\t\t\t\tBelgium\tBE\t\t\t\t\tPatient Organisation\t\t\t\t\tBill Org, Inc.\tYOLO\t\t\t\t\tRue de la Reine\t\tBruxelles\t{person.email}\t{organisation.created_at.isoformat()}\t{person.email}\t{organisation.modified_at.isoformat()}\r\n"""
        f"""{organisation2.pk}\tBill\tMollison\tceo@countrytest.org\tMr\t+9876543210\t+611111111\tfr,it\tAustralia\tAU\t\treviewed\t\t\tPatient Organisation\tCEO\t1\t\t\tCountry Test Org\t\t\t\tA comment\t\t\t\t\t{person.email}\t{organisation2.created_at.isoformat()}\t{person.email}\t{organisation2.modified_at.isoformat()}\r\n"""
        f"""{no_country_organisation.pk}\tBill\tMollison\tceo@countrytest.org\tMr\t+9876543210\t+611111111\tfr,it\tFrance\tFR\t\treviewed\t\t\tPatient Organisation\tCEO\t1\t\t\tNo Country Test Org\t\t\t\tA comment\t\t\t\t\t{person.email}\t{no_country_organisation.created_at.isoformat()}\t{person.email}\t{no_country_organisation.modified_at.isoformat()}\r\n"""
    )
    resp = await aclient.post("/contact/export.xlsx", body=resources)
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "first_name",
            "last_name",
            "email",
            "title",
            "phone",
            "cell_phone",
            "languages",
            "country_label",
            "country",
            "diseases",
            "keywords",
            "groups",
            "groupings",
            "kind",
            "role",
            "is_primary",
            "is_default",
            "is_voting",
            "organisation",
            "acronym",
            "eurordis_membership",
            "rdi_membership",
            "comment",
            "website",
            "address",
            "postcode",
            "city",
            "created_by",
            "created_at",
            "modified_by",
            "modified_at",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            "bmollison@bill.org",
            "Mr",
            "+9876543210",
            "+611111111",
            "fr,it",
            "France",
            "FR",
            None,
            "reviewed",
            None,
            None,
            "Patient Organisation",
            "CEO",
            "1",
            None,
            None,
            "Bill Org, Inc.",
            "YOLO",
            None,
            None,
            "A comment",
            None,
            "Rue de la Reine",
            None,
            "Bruxelles",
            person.created_by.email,
            person.created_at.isoformat(),
            person.modified_by.email,
            person.modified_at.isoformat(),
        ),
        (
            person2.pk,
            "David",
            "Holmgren",
            "david@somewhere.org",
            "Mr",
            "+9876543234",
            None,
            "de,en",
            "Australia",
            "AU",
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            person.created_by.email,
            person2.created_at.isoformat(),
            person.modified_by.email,
            person2.modified_at.isoformat(),
        ),
        (
            organisation.pk,
            None,
            None,
            "contact@bill.org",
            None,
            None,
            None,
            None,
            "Belgium",
            "BE",
            None,
            None,
            None,
            None,
            "Patient Organisation",
            None,
            None,
            None,
            None,
            "Bill Org, Inc.",
            "YOLO",
            None,
            None,
            None,
            None,
            "Rue de la Reine",
            None,
            "Bruxelles",
            organisation.created_by.email,
            organisation.created_at.isoformat(),
            organisation.modified_by.email,
            organisation.modified_at.isoformat(),
        ),
        (
            organisation2.pk,
            "Bill",
            "Mollison",
            "ceo@countrytest.org",
            "Mr",
            "+9876543210",
            "+611111111",
            "fr,it",
            "Australia",
            "AU",
            None,
            "reviewed",
            None,
            None,
            "Patient Organisation",
            "CEO",
            "1",
            None,
            None,
            "Country Test Org",
            None,
            None,
            None,
            "A comment",
            None,
            None,
            None,
            None,
            person.email,
            organisation2.created_at.isoformat(),
            person.email,
            organisation2.modified_at.isoformat(),
        ),
        (
            no_country_organisation.pk,
            "Bill",
            "Mollison",
            "ceo@countrytest.org",
            "Mr",
            "+9876543210",
            "+611111111",
            "fr,it",
            "France",
            "FR",
            None,
            "reviewed",
            None,
            None,
            "Patient Organisation",
            "CEO",
            "1",
            None,
            None,
            "No Country Test Org",
            None,
            None,
            None,
            "A comment",
            None,
            None,
            None,
            None,
            person.email,
            no_country_organisation.created_at.isoformat(),
            person.email,
            no_country_organisation.modified_at.isoformat(),
        ),
    ]

    resp = await aclient.post("/contact/export.json", body=resources)
    assert resp.status == 400
    assert json.loads(resp.body) == {"error": "Unsupported format: json"}


async def test_export_with_groups(aclient, person, person2, organisation, group):
    Role.get(organisation, person).add_group(group=group).save()
    Role(
        organisation=organisation,
        person=person2,
        is_default=True,
        is_voting=True,
        groups=[GroupBelonging(group=group, is_active=False)],
    ).save()
    resources = {
        "resources": [
            {"person": person.pk, "organisation": organisation.pk},
            {"person": person2.pk, "organisation": organisation.pk},
        ]
    }
    resp = await aclient.post("/contact/export.csv", body=resources)
    assert resp.status == 200
    assert resp.body.decode() == (
        """pk,first_name,last_name,email,title,phone,cell_phone,languages,country_label,country,diseases,keywords,groups,groupings,kind,role,is_primary,is_default,is_voting,organisation,acronym,eurordis_membership,rdi_membership,comment,website,address,postcode,city,created_by,created_at,modified_by,modified_at\r\n"""
        f"""1,Bill,Mollison,bmollison@bill.org,Mr,+9876543210,+611111111,"en,fr",France,FR,,,CNA,,Patient Organisation,CEO,1,,,Bill Org,,,,,,,,,bill@somewhere.org,{person.created_at.isoformat()},bill@somewhere.org,{person.modified_at.isoformat()}\r\n"""
        f"""2,David,Holmgren,david@somewhere.org,Mr,+9876543234,,"de,en",Australia,AU,,,,,Patient Organisation,,,1,1,Bill Org,,,,,,,,,bill@somewhere.org,{person2.created_at.isoformat()},bill@somewhere.org,{person2.modified_at.isoformat()}\r\n"""
    )


async def test_export_to_mailchimp(aclient, person, person2, mock_http):
    calls = mock_http()
    person.email = "ispeakitalian@somewhere.org"
    person.languages = ["fr", "it"]
    person.save()
    person2.email = "ispeakenglish@somewhere.org"
    person2.languages = []
    person2.save()
    body = {
        "resources": [{"person": person.pk}, {"person": person2.pk}],
        "name": "super tag tag",
        "languages": ["en", "it", "es"],
    }
    resp = await aclient.post("/contact/mailchimp", body=body)
    assert resp.status == 200
    assert json.loads(resp.body) == {"report": "Created 2 tags"}
    assert len(calls) == 2
    assert calls[0]["kwargs"]["json"] == {
        "name": "super tag tag_en",
        "static_segment": ["ispeakenglish@somewhere.org"],
    }

    assert calls[1]["kwargs"]["json"] == {
        "name": "super tag tag_it",
        "static_segment": ["ispeakitalian@somewhere.org"],
    }


async def test_basket(aclient, person, organisation, person2):
    resp = await aclient.post(
        "/basket",
        body={
            "resources": [
                {"person": person.pk, "organisation": organisation.pk},
                {"person": person2.pk},
            ]
        },
    )
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "data": [
            {
                "pk": person.pk,
                "email": "bmollison@bill.org",
                "phone": "+9876543210",
                "country_label": "France",
                "country": "FR",
                "title": "Mr",
                "first_name": "Bill",
                "last_name": "Mollison",
                "cell_phone": "+611111111",
                "languages": "en,fr",
                "person_pk": person.pk,
                "role": "CEO",
                "kind": "Patient Organisation",
                "organisation": "Bill Org",
                "organisation_pk": organisation.pk,
                "is_primary": "1",
                "created_by": person.created_by.email,
                "created_at": person.created_at.isoformat(),
                "modified_by": person.modified_by.email,
                "modified_at": person.modified_at.isoformat(),
            },
            {
                "pk": person2.pk,
                "email": "david@somewhere.org",
                "phone": "+9876543234",
                "country": "AU",
                "country_label": "Australia",
                "title": "Mr",
                "first_name": "David",
                "last_name": "Holmgren",
                "languages": "de,en",
                "person_pk": person2.pk,
                "created_by": person.created_by.email,
                "created_at": person2.created_at.isoformat(),
                "modified_by": person.modified_by.email,
                "modified_at": person2.modified_at.isoformat(),
            },
        ]
    }


async def test_get_config(aclient):
    resp = await aclient.get("/config")
    assert resp.status == 200
    body = json.loads(resp.body)
    assert "GROUPING" in body
    assert "EVENT_ROLES" in body
    assert body["GROUPING"][0]["key"]


async def test_complete_all(aclient, disease, person, event):
    event.name = "Belgium Royal Gala"
    event.save()
    disease.name = "Prune belly syndrome"
    disease.save()
    person.last_name = "Belmondo"
    person.save()
    resp = await aclient.get("/complete?q=bel")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {"pk": "BE", "label": "Belgium", "resource": "country", "score": 0.9},
            {"pk": "BY", "label": "Belarus", "resource": "country", "score": 0.9},
            {"pk": "BZ", "label": "Belize", "resource": "country", "score": 0.9},
            {
                "pk": person.pk,
                "first_name": "Bill",
                "last_name": "Belmondo",
                "email": "bill@somewhere.org",
                "uri": f"/person/{person.pk}",
                "resource": "person",
                "label": "Bill Belmondo",
                "country": "FR",
                "score": 0.9,
                "role": None,
            },
            {
                "duration": 8,
                "kind": "awards",
                "label": "Belgium Royal Gala (19/11/2018)",
                "location": "Bruxelles",
                "name": "Belgium Royal Gala",
                "pk": event.pk,
                "resource": "event",
                "score": 0.9,
                "start_date": "2018-11-19",
            },
            {
                "pk": 12345,
                "resource": "disease",
                "orpha_number": 12345,
                "name": "Prune belly syndrome",
                "label": "Prune belly syndrome",
                "score": 0.72,
                "resource": "disease",
                "depth": 0,
                "max_depth": 8,
                "group_of_type": "disorder",
            },
        ]
    }


async def test_complete_all_with_orpha_number(aclient, disease):
    resp = await aclient.get(f"/complete?q={disease.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "pk": 12345,
                "resource": "disease",
                "orpha_number": 12345,
                "name": "Congenital deficiency in alpha-fetoprotein",
                "label": "Congenital deficiency in alpha-fetoprotein",
                "score": 1,
                "depth": 0,
                "max_depth": 8,
                "group_of_type": "disorder",
            }
        ]
    }


async def test_complete_all_with_website(aclient, organisation):
    organisation.website = "https://www.thepouetorganisation.org/"
    organisation.save()
    resp = await aclient.get("/complete?q=http://thepouetorganisation.org")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data["data"][0]["pk"] == organisation.pk


async def test_complete_all_with_email(aclient, person):
    # Case insensitive.
    resp = await aclient.get(f"/complete?q={person.email.upper()}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "pk": person.pk,
                "first_name": "Bill",
                "last_name": "Mollison",
                "email": "bill@somewhere.org",
                "uri": f"/person/{person.pk}",
                "resource": "person",
                "label": "Bill Mollison",
                "country": "FR",
                "score": 1,
            }
        ]
    }


async def test_complete_all_with_role_email(aclient, person, organisation):
    role = Role.objects.get(person=person, organisation=organisation)
    # Case insensitive.
    resp = await aclient.get(f"/complete?q={role.email.upper()}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "pk": person.pk,
                "first_name": "Bill",
                "last_name": "Mollison",
                "email": "bill@somewhere.org",
                "uri": f"/person/{person.pk}",
                "resource": "person",
                "label": "Bill Mollison",
                "country": "FR",
                "score": 1,
            }
        ]
    }


async def test_complete_all_with_synonyms(aclient, disease):
    disease.name = "Langerhans histiocytosis"
    disease.synonyms = ["Langerhans granulomatosis"]
    disease.save()
    resp = await aclient.get("/complete?q=granulomatosis")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "pk": 12345,
                "resource": "disease",
                "orpha_number": 12345,
                "name": "Langerhans histiocytosis",
                "label": "Langerhans histiocytosis (Langerhans granulomatosis)",
                "score": 0.68,
                "depth": 0,
                "max_depth": 8,
                "group_of_type": "disorder",
            }
        ]
    }


async def test_import_persons(aclient, person, staff, keyword):
    config.TOBEREVIEWED_KEYWORD = keyword.pk
    wb = Workbook()
    ws = wb.active
    ws.append(["first_name", "last_name", "email", "country"])
    ws.append([person.first_name, person.last_name, person.email, "IT"])
    ws.append(["Nat", "King", "nat@king.cole", "US"])
    resp = await aclient.post(
        "/person/import", files={"data": save_virtual_workbook(wb)}
    )
    assert resp.status == 200
    new = Person.objects.get(email="nat@king.cole")
    assert json.loads(resp.body) == [
        {
            "data": {
                "country": "FR",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "label": "Bill Mollison",
                "last_name": "Mollison",
                "pk": person.pk,
                "resource": "person",
                "uri": f"/person/{person.pk}",
            },
            "raw": {
                "country": "IT",
                "email": "bill@somewhere.org",
                "first_name": "Bill",
                "last_name": "Mollison",
            },
            "report": {},
            "status": "found",
        },
        {
            "data": {
                "country": "US",
                "email": "nat@king.cole",
                "first_name": "Nat",
                "label": "Nat King",
                "last_name": "King",
                "pk": new.pk,
                "resource": "person",
                "uri": f"/person/{new.pk}",
            },
            "raw": {
                "country": "US",
                "email": "nat@king.cole",
                "first_name": "Nat",
                "last_name": "King",
            },
            "report": {},
            "status": "created",
        },
    ]


async def test_import_role_to_event(
    aclient, person, person2, role, event, organisation, staff, keyword
):
    role.email = "email@role.com"
    role.save()
    organisation.email = "email@org.org"
    organisation.save()
    Role(
        organisation=role.organisation,
        person=person2,
    ).save()
    wb = Workbook()
    ws = wb.active
    ws.append(["first_name", "last_name", "email", "country"])
    ws.append([person.first_name, person.last_name, role.email, "IT"])
    ws.append([person2.first_name, person2.last_name, "person2@org.org", "IT"])
    ws.append(["Nat", "King", "nat@org.org", "US"])
    ws.append(["Thelo", "Monk", "thelo@monk.org", "US"])
    resp = await aclient.post(
        f"/event/{event.pk}/import", files={"data": save_virtual_workbook(wb)}
    )
    assert resp.status == 200
    report = json.loads(resp.body)
    assert report[3]["report"] == {
        "error": "No organisation matched. Person created but not linked to event."
    }
    assert report[3]["status"] == "error"
    new_role = Role.objects.get(email="nat@org.org")
    role.reload()
    role2 = Role.get(person=person2, organisation=organisation)
    assert len(new_role.events) == 1
    assert new_role.attended(event)
    assert new_role.person.first_name == "Nat"
    assert new_role.person.last_name == "King"
    assert new_role.person.country == "US"
    assert len(role.events) == 1
    assert role.attended(event)
    assert len(role2.events) == 1
    assert role2.attended(event)


async def test_import_persons_to_event_with_role(
    aclient, person, staff, event, role, keyword
):
    role.email = "email@role.com"
    role.save()
    wb = Workbook()
    ws = wb.active
    ws.append(["first_name", "last_name", "email", "country", "role"])
    ws.append([person.first_name, person.last_name, role.email, "IT", "Staff"])
    resp = await aclient.post(
        f"/event/{event.pk}/import", files={"data": save_virtual_workbook(wb)}
    )
    assert resp.status == 200
    role.reload()
    assert len(role.events) == 1
    assert role.events[0].role == "Staff"
    assert role.attended(event)


async def test_import_to_event_should_update_role(
    aclient, person, staff, event, keyword, role
):
    role.events = [RoleToEvent(event=event, role="Staff")]
    role.save()
    wb = Workbook()
    ws = wb.active
    ws.append(["first_name", "last_name", "email", "country", "role"])
    ws.append([person.first_name, person.last_name, person.email, "IT", "Speaker"])
    resp = await aclient.post(
        f"/event/{event.pk}/import", files={"data": save_virtual_workbook(wb)}
    )
    assert resp.status == 200
    role.reload()
    assert len(role.events) == 1
    assert role.events[0].role == "Speaker"


async def test_import_twice_same_person_to_event(
    aclient, person, staff, event, keyword, role
):
    wb = Workbook()
    ws = wb.active
    ws.append(["first_name", "last_name", "email", "role"])
    ws.append([person.first_name, person.last_name, role.email, "Staff"])
    ws.append([person.first_name, person.last_name, role.email, "Speaker"])
    resp = await aclient.post(
        f"/event/{event.pk}/import", files={"data": save_virtual_workbook(wb)}
    )
    assert resp.status == 200
    role.reload()
    assert len(role.events) == 1
    assert role.events[0].role == "Speaker"


async def test_import_persons_with_bad_file(aclient, person, staff):
    resp = await aclient.post("/person/import", files={"data": "invalid"})
    assert resp.status == 422
    assert json.loads(resp.body) == {"error": "Can't read file. Is that an XLSX?"}


async def test_import_to_event(aclient, person, staff, event, keyword, role):
    wb = Workbook()
    ws = wb.active
    ws.append(["first_name", "last_name", "email", "role"])
    ws.append([person.first_name, person.last_name, role.email, "Speaker"])
    resp = await aclient.post(
        f"/event/{event.pk}/import", files={"data": save_virtual_workbook(wb)}
    )
    assert resp.status == 200
    role.reload()
    assert len(role.events) == 1
    assert role.events[0].role == "Speaker"


async def test_export_event_attendees(
    aclient, person, role, organisation, event, keyword, person2
):
    person.email = None
    person.save()
    role.events.append(RoleToEvent(event=event, role="Exhibitor"))
    role.save()
    Role(
        person=person2, organisation=organisation, events=[RoleToEvent(event=event)]
    ).save()
    resp = await aclient.get(f"/event/{event.pk}/export")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        ("pk", "first_name", "last_name", "email", "role", "organisation"),
        (person.pk, "Bill", "Mollison", "bmollison@bill.org", "Exhibitor", "Bill Org"),
        (person2.pk, "David", "Holmgren", "david@somewhere.org", None, "Bill Org"),
    ]


async def test_get_disease(aclient, disease):
    disease.synonyms = ["A", "B"]
    disease.save()
    resp = await aclient.get(f"/disease/{disease.pk}")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "depth": 0,
        "label": "Congenital deficiency in alpha-fetoprotein",
        "max_depth": 8,
        "name": "Congenital deficiency in alpha-fetoprotein",
        "orpha_number": 12345,
        "pk": 12345,
        "resource": "disease",
        "synonyms": ["A", "B"],
        "roots": [],
        "groupings": [
            {"name": "bond", "validated": False},
            {"name": "eye", "validated": False},
        ],
        "group_of_type": "disorder",
        "disorder_type": "Disease",
        "status": "active",
    }


async def test_update_disease_grouping(aclient, disease):
    disease.groupings = [{"name": "bond"}, {"name": "cranio"}]
    disease.save()
    resp = await aclient.patch(
        f"/disease/{disease.pk}",
        body={
            "groupings": [
                {"name": "endo"},
                {"name": "bond", "validated": True},
                {"name": "ernica"},
            ]
        },
    )
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "depth": 0,
        "label": "Congenital deficiency in alpha-fetoprotein",
        "max_depth": 8,
        "name": "Congenital deficiency in alpha-fetoprotein",
        "orpha_number": 12345,
        "pk": 12345,
        "resource": "disease",
        "synonyms": [],
        "roots": [],
        "groupings": [
            {"name": "endo", "validated": False},
            {"name": "bond", "validated": True},
            {"name": "ernica", "validated": False},
        ],
        "group_of_type": "disorder",
        "disorder_type": "Disease",
        "status": "active",
    }


async def test_cannot_patch_disease_fields_other_than_grouping(aclient, disease):
    disease.groupings = [{"name": "bond"}, {"name": "cranio"}]
    disease.save()
    resp = await aclient.patch(
        f"/disease/{disease.pk}",
        body={"groupings": ["endo", "bond", "ernica"], "name": "New Name"},
    )
    assert resp.status == 422


async def test_get_disease_history(aclient, disease):
    resp = await aclient.get(f"/disease/{disease.pk}/history")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "data": [
            {
                "action": "create",
                "at": disease.created_at.isoformat(),
                "by": {
                    "country": "FR",
                    "email": "bill@somewhere.org",
                    "first_name": "Bill",
                    "label": "Bill Mollison",
                    "last_name": "Mollison",
                    "pk": "1",
                    "resource": "person",
                    "uri": "/person/1",
                },
                "diff": {
                    "groupings": {
                        "new": [
                            {"name": "bond", "validated": False},
                            {"name": "eye", "validated": False},
                        ],
                        "subject": "groupings",
                        "verb": "create",
                    }
                },
                "of": {
                    "label": "Congenital deficiency in alpha-fetoprotein (12345)",
                    "pk": 12345,
                    "resource": "disease",
                    "uri": "/disease/12345",
                },
            }
        ],
    }
    disease.groupings = [{"name": "bond"}, {"name": "cranio"}]
    disease.save()
    resp = await aclient.get(f"/disease/{disease.pk}/history")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "data": [
            {
                "action": "update",
                "at": disease.modified_at.isoformat(),
                "by": {
                    "country": "FR",
                    "email": "bill@somewhere.org",
                    "first_name": "Bill",
                    "label": "Bill Mollison",
                    "last_name": "Mollison",
                    "pk": "1",
                    "resource": "person",
                    "uri": "/person/1",
                },
                "diff": {
                    "groupings": {
                        "new": [
                            {"name": "bond", "validated": False},
                            {"name": "cranio", "validated": False},
                        ],
                        "old": [
                            {"name": "bond", "validated": False},
                            {"name": "eye", "validated": False},
                        ],
                        "subject": "groupings",
                        "verb": "update",
                    }
                },
                "of": {
                    "label": "Congenital deficiency in alpha-fetoprotein (12345)",
                    "pk": 12345,
                    "resource": "disease",
                    "uri": "/disease/12345",
                },
            },
            {
                "action": "create",
                "at": disease.created_at.isoformat(),
                "by": {
                    "country": "FR",
                    "email": "bill@somewhere.org",
                    "first_name": "Bill",
                    "label": "Bill Mollison",
                    "last_name": "Mollison",
                    "pk": "1",
                    "resource": "person",
                    "uri": "/person/1",
                },
                "diff": {
                    "groupings": {
                        "new": [
                            {"name": "bond", "validated": False},
                            {"name": "eye", "validated": False},
                        ],
                        "subject": "groupings",
                        "verb": "create",
                    }
                },
                "of": {
                    "label": "Congenital deficiency in alpha-fetoprotein (12345)",
                    "pk": 12345,
                    "resource": "disease",
                    "uri": "/disease/12345",
                },
            },
        ],
    }


async def test_history(aclient, person, organisation):
    person.last_name = "Foo"
    person.save()
    organisation.name = "Bar"
    organisation.save()
    resp = await aclient.get("/history")
    data = json.loads(resp.body)["data"]
    assert len(data) == 6
    for d in data:
        del d["at"]  # Too unstable to test here.
    # Person last_name update.
    assert {
        "by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Foo",
            "last_name": "Foo",
            "pk": person.pk,
            "resource": "person",
            "uri": f"/person/{person.pk}",
        },
        "diff": {
            "last_name": {
                "new": "Foo",
                "old": "Mollison",
                "subject": "last_name",
                "verb": "update",
            },
        },
        "of": {
            "label": "Bill Foo",
            "pk": person.pk,
            "resource": "person",
            "uri": f"/person/{person.pk}",
        },
        "action": "update",
    } in data
    # Person roles update.
    assert {
        "by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Foo",
            "last_name": "Foo",
            "pk": person.pk,
            "resource": "person",
            "uri": f"/person/{person.pk}",
        },
        "diff": {
            "roles": [
                {
                    "new": {
                        "email": "bmollison@bill.org",
                        "groups": [],
                        "events": [],
                        "is_primary": True,
                        "is_default": False,
                        "is_voting": False,
                        "is_archived": False,
                        "name": "CEO",
                        "organisation": "2",
                        "organisation_name": "Bar",
                        "phone": "+1234567890",
                    },
                    "subject": "Bill Org",
                    "verb": "create",
                }
            ],
        },
        "of": {
            "label": "Bill Foo",
            "pk": person.pk,
            "resource": "person",
            "uri": f"/person/{person.pk}",
        },
        "action": "update",
    } in data
    # Organisation name update.
    assert {
        "by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Foo",
            "last_name": "Foo",
            "pk": person.pk,
            "resource": "person",
            "uri": f"/person/{person.pk}",
        },
        "diff": {
            "name": {
                "new": "Bar",
                "old": "Bill Org",
                "subject": "name",
                "verb": "update",
            },
        },
        "of": {
            "label": "Bar",
            "pk": organisation.pk,
            "resource": "organisation",
            "uri": f"/organisation/{organisation.pk}",
        },
        "action": "update",
    } in data
    # Organisation roles update.
    assert {
        "by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Foo",
            "last_name": "Foo",
            "pk": person.pk,
            "resource": "person",
            "uri": f"/person/{person.pk}",
        },
        "diff": {
            "roles": [
                {
                    "new": {
                        "email": "bmollison@bill.org",
                        "groups": [],
                        "events": [],
                        "is_primary": True,
                        "is_default": False,
                        "is_voting": False,
                        "is_archived": False,
                        "name": "CEO",
                        "person": "1",
                        "phone": "+1234567890",
                    },
                    "subject": "Bill Mollison",
                    "verb": "create",
                }
            ],
        },
        "of": {
            "label": "Bar",
            "pk": organisation.pk,
            "resource": "organisation",
            "uri": f"/organisation/{organisation.pk}",
        },
        "action": "update",
    } in data
    # Person create.
    assert {
        "by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Foo",
            "last_name": "Foo",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "diff": {
            "cell_phone": {
                "new": "+611111111",
                "subject": "cell_phone",
                "verb": "create",
            },
            "country": {"new": "FR", "subject": "country", "verb": "create"},
            "email": {
                "new": "bill@somewhere.org",
                "subject": "email",
                "verb": "create",
            },
            "first_name": {"new": "Bill", "subject": "first_name", "verb": "create"},
            "languages": [
                {"subject": "English", "value": "en", "verb": "create"},
                {"subject": "French", "value": "fr", "verb": "create"},
            ],
            "last_name": {"new": "Mollison", "subject": "last_name", "verb": "create"},
            "phone": {"new": "+9876543210", "subject": "phone", "verb": "create"},
            "title": {"new": "mr", "subject": "title", "verb": "create"},
        },
        "action": "create",
        "of": {
            "label": "Bill Foo",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
    } in data
    # Organisation create.
    assert {
        "by": {
            "country": "FR",
            "email": "bill@somewhere.org",
            "first_name": "Bill",
            "label": "Bill Foo",
            "last_name": "Foo",
            "pk": "1",
            "resource": "person",
            "uri": "/person/1",
        },
        "diff": {
            "country": {"new": "BE", "subject": "country", "verb": "create"},
            "email": {"new": "contact@bill.org", "subject": "email", "verb": "create"},
            "kind": {
                "new": "Patient Organisation",
                "old": None,
                "subject": "kind",
                "verb": "create",
            },
            "name": {"new": "Bill Org", "subject": "name", "verb": "create"},
        },
        "action": "create",
        "of": {
            "label": "Bar",
            "pk": "2",
            "resource": "organisation",
            "uri": "/organisation/2",
        },
    } in data
    resp = await aclient.get("/history?limit=1")
    body = json.loads(resp.body)
    assert len(body["data"]) == 1


async def test_history_with_deleted_object(aclient, person, organisation):
    organisation.name = "Bar"
    organisation.save()
    assert organisation.roles
    organisation.delete()
    resp = await aclient.get("/history")
    diffs = [d["diff"] for d in json.loads(resp.body)["data"]]
    assert len(diffs) == 7
    # Organisation has been deleted
    assert {
        "country": {"old": "BE", "subject": "country", "verb": "delete"},
        "email": {"old": "contact@bill.org", "subject": "email", "verb": "delete"},
        "kind": {
            "new": None,
            "old": "Patient Organisation",
            "subject": "kind",
            "verb": "delete",
        },
        "name": {"old": "Bar", "subject": "name", "verb": "delete"},
        "roles": [
            {
                "old": {
                    "email": "bmollison@bill.org",
                    "is_primary": True,
                    "is_default": False,
                    "is_voting": False,
                    "is_archived": False,
                    "name": "CEO",
                    "person": "1",
                    "phone": "+1234567890",
                    "groups": [],
                    "events": [],
                },
                "subject": "Bill Mollison",
                "verb": "delete",
            }
        ],
    } in diffs
    # Person is updated as its roles has been changed
    assert {
        "roles": [
            {
                "old": {
                    "email": "bmollison@bill.org",
                    "groups": [],
                    "events": [],
                    "is_primary": True,
                    "is_default": False,
                    "is_voting": False,
                    "is_archived": False,
                    "name": "CEO",
                    "organisation": "2",
                    "phone": "+1234567890",
                },
                "subject": "Bill Org",
                "verb": "delete",
            }
        ]
    } in diffs


async def test_history_can_be_filtered_by_action(aclient, person, organisation):
    organisation.name = "Bar"
    organisation.save()
    organisation.delete()
    resp = await aclient.get("/history")
    body = json.loads(resp.body)
    assert len(body["data"]) == 7
    resp = await aclient.get("/history?action=delete")
    body = json.loads(resp.body)
    assert len(body["data"]) == 1
    resp = await aclient.get("/history?action=create")
    body = json.loads(resp.body)
    assert len(body["data"]) == 2
    resp = await aclient.get("/history?action=update")
    body = json.loads(resp.body)
    assert len(body["data"]) == 4


async def test_history_can_be_filtered_by_field(aclient, person, organisation, disease):
    person.diseases = [disease]
    person.save()
    organisation.diseases = [disease]
    organisation.save()
    person.first_name = "foo"
    person.save()
    resp = await aclient.get("/history")
    body = json.loads(resp.body)
    assert len(body["data"]) == 8
    resp = await aclient.get("/history?field=diseases")
    body = json.loads(resp.body)
    assert len(body["data"]) == 2


async def test_history_export(aclient, person, organisation):
    config.ACCESS_EMAIL_DOMAINS = "@special.org"
    role = person.roles[0]
    role.email = "bill@special.org"
    role.save()
    person.first_name = "newnew"
    person.save()
    resp = await aclient.get("/history/export")
    assert resp.status == 200
    body = resp.body.decode()
    assert body.startswith("by;at;action\r\n")
    assert f"bill@special.org;{list(person.versions)[-1].at};create\r\n" in body
    assert f"bill@special.org;{list(organisation.versions)[-1].at};create\r\n" in body
    assert f"bill@special.org;{list(organisation.versions)[0].at};update\r\n" in body
    assert f"bill@special.org;{list(person.versions)[1].at};update\r\n" in body
    assert f"bill@special.org;{list(person.versions)[0].at};update\r\n" in body


async def test_groups_mass_edit(aclient, person, person2, group, organisation):
    body = {
        "resources": [
            {"person": person.pk, "organisation": organisation.pk},
            {"person": person2.pk, "organisation": organisation.pk},
        ],
        "field": "groups",
        "operator": "push",
        "value": {"group": group.pk, "role": "Member"},
    }
    resp = await aclient.patch("/basket", body=body)
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == [
        {"raw": {"person": "1", "organisation": "3"}, "status": "success"},
        {
            "raw": {"person": "2", "organisation": "3"},
            "status": "error",
            "error": "Role does not exist",
        },
    ]
    assert Role.get(organisation.pk, person.pk).groups == [
        GroupBelonging(group=group, role="Member")
    ]


async def test_groups_mass_edit_invalid_root_key(
    aclient, person, person2, organisation
):
    body = {
        "resources": [
            {"person": person.pk, "organisation": organisation.pk},
            {"person": person2.pk, "organisation": organisation.pk},
        ],
        "field": "groups",
        "value": {"pk": "cna", "role": "Member"},
    }
    resp = await aclient.patch("/basket", body=body)
    assert resp.status == 400


async def test_kind_mass_edit(aclient, person, organisation):
    assert organisation.kind != 61
    body = {
        "resources": [{"person": person.pk}, {"organisation": organisation.pk}],
        "field": "kind",
        "operator": "set",
        "value": "61",
    }
    resp = await aclient.patch("/basket", body=body)
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == [
        {"raw": {"person": "1"}, "status": "error", "error": "Invalid field kind"},
        {"raw": {"organisation": "2"}, "status": "success"},
    ]
    assert Organisation.get(organisation.pk).kind == 61


async def test_fees_report(aclient, organisation, eurordis_org):
    member = Organisation(
        name="Active membership",
        filemaker_pk="co34",
        country="BE",
        membership=[
            Membership(
                organisation=eurordis_org,
                start_date="2018-01-01",
                end_date="2018-12-31",
                fees=[Fee(year=2018, amount=50, payment_date=date(2018, 2, 13))],
                status="f",
            )
        ],
    ).save()
    resp = await aclient.get("/report/fees?year=2018")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        ("id", "old id", "name", "country", "fee", "payment date", "status"),
        (member.pk, "co34", "Active membership", "BE", 50, "2018-02-13", "Full"),
    ]
    resp = await aclient.get("/report/fees?year=2017")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        ("id", "old id", "name", "country", "fee", "payment date", "status")
    ]


async def test_fees_receipt(aclient, organisation, eurordis_org, person, monkeypatch):
    called = False

    def send_email(to, subject, body, attachment, sender):
        nonlocal called
        called = True
        assert sender == config.FROM_EMAIL_RECEIPTS
        assert attachment
        assert len(attachment.read())
        assert attachment.filename == "receipt_2018.pdf"
        assert attachment.content_type == "application/pdf"
        assert (
            body
            == """Dear Bill Mollison,

Following the reception of your contribution 2018, we are pleased to address your receipt for payment.

Best regards,

Annie Rahajarizafy
Accounting Manager

EURORDIS - Rare Diseases Europe
Paris Office - Plateforme Maladies Rares - 96 rue Didot 75014 Paris - France
Phone : +33 1 56 53 13 65
"""
        )

    monkeypatch.setattr("eurordis.emails.send", send_email)

    member = Organisation(
        name="Active membership",
        country="BE",
        membership=[
            Membership(
                organisation=eurordis_org,
                start_date="2018-01-01",
                fees=[Fee(year=2018, amount=50, payment_date=date(2018, 2, 13))],
                status="f",
            )
        ],
    ).save()
    Role(person=person, organisation=member).save()

    # Non staff call is unauthorized
    resp = await aclient.post("/send-receipt", {})
    assert resp.status == 401
    aclient.logout()

    # Log as staff
    aclient.login(person, email=config.STAFF[0])
    resp = await aclient.post(
        "/send-receipt", {"person": person.pk, "organisation": member.pk, "year": 2018}
    )
    assert resp.status == 204, resp.body
    assert called is True
    member.reload()
    assert (
        list(member.attachments.values())[0].as_relation()["filename"]
        == "receipt_2018.pdf"
    )
    assert list(member.attachments.values())[0].as_relation()["receipt"] == 2018
    assert member.organisation_membership(eurordis_org).fee_for(2018).receipt_sent


async def test_dump_person(aclient, person, organisation, person2, disease, keyword):
    person.languages = ["fr", "it"]
    person.keywords = [keyword.pk]
    person.country = "FR"
    person.comment = "A comment"
    person.diseases = [disease]
    person.save()
    resp = await aclient.get("/dump/person")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "first_name",
            "last_name",
            "email",
            "country",
            "country_label",
            "created_at",
            "modified_at",
            "created_by",
            "modified_by",
            "title",
            "phone",
            "cell_phone",
            "languages",
            "groupings",
            "diseases",
            "keywords",
            "comment",
            "filemaker_pk",
            "scopes",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            "bill@somewhere.org",
            "FR",
            "France",
            person.created_at.isoformat(),
            person.modified_at.isoformat(),
            "bill@somewhere.org",
            "bill@somewhere.org",
            "Mr",
            "+9876543210",
            "+611111111",
            "fr,it",
            "Bone,Eye",
            "12345",
            "reviewed",
            "A comment",
            None,
            None,
        ),
        (
            person2.pk,
            "David",
            "Holmgren",
            "david@somewhere.org",
            "AU",
            "Australia",
            person2.created_at.isoformat(),
            person2.modified_at.isoformat(),
            "bill@somewhere.org",
            "bill@somewhere.org",
            "Mr",
            "+9876543234",
            None,
            "de,en",
            None,
            None,
            None,
            None,
            None,
            None,
        ),
    ]


async def test_dump_organisation(aclient, person, organisation, disease, eurordis_org, rdi_org):
    organisation.diseases = [disease]
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2019-01-13", status="f"),
        Membership(organisation=rdi_org, start_date="2019-01-13", status="a")
    ]
    organisation.save()
    resp = await aclient.get("/dump/organisation")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "country",
            "country_label",
            "kind",
            "created_at",
            "modified_at",
            "created_by",
            "modified_by",
            "email",
            "phone",
            "cell_phone",
            "acronym",
            "address",
            "postcode",
            "city",
            "website",
            "groupings",
            "diseases",
            "keywords",
            "eurordis_membership",
            "rdi_membership",
            "comment",
            "filemaker_pk",
            "adherents",
            "budget",
            "creation_year",
            "board",
            "paid_staff",
            "pharma_funding",
        ),
        (
            organisation.pk,
            "Bill Org",
            "BE",
            "Belgium",
            "Patient Organisation",
            organisation.created_at.isoformat(),
            organisation.modified_at.isoformat(),
            "bill@somewhere.org",
            "bill@somewhere.org",
            "contact@bill.org",
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            "Bone,Eye",
            "12345",
            None,
            "Full",
            "Associate",
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
        ),
        (
            eurordis_org.pk,
            'Eurordis', 'FR',
            'France',
            None,
            eurordis_org.created_at.isoformat(),
            eurordis_org.modified_at.isoformat(),
            'bill@somewhere.org',
            'bill@somewhere.org',
            'eurordis@pouet.org',
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            'so4146',
            None,
            None,
            None,
            None,
            None,
            None
        ),
        (
            rdi_org.pk,
            'Rare Diseases International',
            'FR',
            'France',
            None,
            rdi_org.created_at.isoformat(),
            rdi_org.modified_at.isoformat(),
            'bill@somewhere.org',
            'bill@somewhere.org',
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            'so7746',
            None,
            None,
            None,
            None,
            None,
            None
        )
    ]


@pytest.mark.xfail
async def test_dump_person_with_event(aclient, person, event):
    person.events.append(RoleToEvent(event=event))
    person.save()
    resp = await aclient.get("/dump/person")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "first_name",
            "last_name",
            "email",
            "country",
            "country_label",
            "created_at",
            "modified_at",
            "created_by",
            "modified_by",
            "title",
            "phone",
            "cell_phone",
            "languages",
            "groupings",
            "diseases",
            "keywords",
            "comment",
            "events",
            "filemaker_pk",
            "scopes",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            "bill@somewhere.org",
            "FR",
            "France",
            person.created_at.isoformat(),
            person.modified_at.isoformat(),
            "bill@somewhere.org",
            "bill@somewhere.org",
            "Mr",
            "+9876543210",
            "+611111111",
            "en,fr",
            None,
            None,
            None,
            None,
            "Royal Gala (2018-11-19)",
            None,
            None,
        ),
    ]


@pytest.mark.xfail
async def test_dump_person_with_group(aclient, person, group):
    person.groups.append(GroupBelonging(group=group, role="Member"))
    person.save()
    resp = await aclient.get("/dump/person")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "first_name",
            "last_name",
            "email",
            "country",
            "country_label",
            "created_at",
            "modified_at",
            "created_by",
            "modified_by",
            "title",
            "phone",
            "cell_phone",
            "languages",
            "groupings",
            "diseases",
            "keywords",
            "comment",
            "groups",
            "events",
            "filemaker_pk",
            "scopes",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            "bill@somewhere.org",
            "FR",
            "France",
            person.created_at.isoformat(),
            person.modified_at.isoformat(),
            "bill@somewhere.org",
            "bill@somewhere.org",
            "Mr",
            "+9876543210",
            "+611111111",
            "en,fr",
            None,
            None,
            None,
            None,
            "CNA/Member",
            None,
            None,
            None,
        ),
    ]


async def test_dump_person_without_email(aclient, person):
    person.email = None
    person.save()
    resp = await aclient.get("/dump/person")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "first_name",
            "last_name",
            "email",
            "country",
            "country_label",
            "created_at",
            "modified_at",
            "created_by",
            "modified_by",
            "title",
            "phone",
            "cell_phone",
            "languages",
            "groupings",
            "diseases",
            "keywords",
            "comment",
            "filemaker_pk",
            "scopes",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            None,
            "FR",
            "France",
            person.created_at.isoformat(),
            person.modified_at.isoformat(),
            "Bill Mollison",
            "Bill Mollison",
            "Mr",
            "+9876543210",
            "+611111111",
            "en,fr",
            None,
            None,
            None,
            None,
            None,
            None,
        ),
    ]


async def test_dump_event(aclient, role, event):
    role.events.append(RoleToEvent(event=event))
    role.save()
    resp = await aclient.get("/dump/event")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "kind",
            "start_date",
            "duration",
            "location",
            "created_at",
            "modified_at",
            "created_by",
            "modified_by",
            "attendees",
        ),
        (
            "1",
            "Royal Gala",
            "Awards ceremony",
            event.start_date.isoformat(),
            "8",
            "Bruxelles",
            event.created_at.isoformat(),
            event.modified_at.isoformat(),
            "bill@somewhere.org",
            "bill@somewhere.org",
            "1",
        ),
    ]


async def test_dump_group(aclient, group, person, organisation):
    group.kind = "epag"
    group.save()
    person.roles[0].add_group(group=group, role="Member").save()
    resp = await aclient.get("/dump/group")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "description",
            "kind",
            "created_at",
            "modified_at",
            "created_by",
            "modified_by",
            "active contacts",
            "inactive contacts",
            "all contacts",
        ),
        (
            "1",
            "CNA",
            None,
            "ERN - European Reference Network",
            group.created_at.isoformat(),
            group.modified_at.isoformat(),
            "bill@somewhere.org",
            "bill@somewhere.org",
            "1",
            None,
            "1",
        ),
    ]


async def test_dump_diseases(aclient, disease, person, organisation):
    Disease(
        orpha_number=60,
        name="Alpha-1-antitrypsin deficiency",
        groupings=[{"name": "rareliver"}, {"name": "lung", "validated": True}],
    ).save()
    resp = await aclient.get("/dump/disease")
    assert resp.status == 200
    assert resp.body.split(b"\r\n")[:-1] == [
        b"OrphaNumber;Name;Groupings;Validated Groupings;Organisations;Persons;Total;Group of Type;Status;Covered",
        b"12345;Congenital deficiency in alpha-fetoprotein;bond,eye;;0;0;0;Disorder;active;",
        b"60;Alpha-1-antitrypsin deficiency;rareliver,lung;lung;0;0;0;Disorder;active;",
    ]


async def test_group_role_report(aclient, group, person, organisation):
    group.kind = "epag"
    group.save()
    person.roles[0].add_group(group=group, role="Member").save()
    resp = await aclient.get("/report/group-roles.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "person_pk",
            "first_name",
            "last_name",
            "email",
            "country",
            "organisation_pk",
            "organisation",
            "organisation_kind",
            "group_name",
            "group_role",
            "group_kind",
            "is_active",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            "bmollison@bill.org",
            "France",
            organisation.pk,
            "Bill Org",
            "Patient Organisation",
            "CNA",
            "Member",
            "ERN - European Reference Network",
            True,
        ),
    ]


async def test_dump_keyword(aclient, keyword, person):
    keyword.kind = "expertise"
    keyword.save()
    person.keywords = [keyword]
    person.save()
    resp = await aclient.get("/dump/keyword")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "for_person",
            "for_organisation",
            "kind",
            "created_at",
            "modified_at",
            "created_by",
            "modified_by",
            "count",
        ),
        (
            "1",
            "reviewed",
            "1",
            "1",
            "Expertise",
            keyword.created_at.isoformat(),
            keyword.modified_at.isoformat(),
            "bill@somewhere.org",
            "bill@somewhere.org",
            "1",
        ),
    ]


async def test_dump_roles(aclient, person, organisation, group):
    person.roles[0].add_group(group=group).save()
    resp = await aclient.get("/dump/role")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "person_pk",
            "first_name",
            "last_name",
            "email",
            "role",
            "phone",
            "is_primary",
            "is_default",
            "is_voting",
            "organisation_pk",
            "organisation",
            "kind",
            "groups",
        ),
        (
            person.pk,
            "Bill",
            "Mollison",
            "bmollison@bill.org",
            "CEO",
            "+1234567890",
            "1",
            None,
            None,
            organisation.pk,
            "Bill Org",
            "Patient Organisation",
            "CNA",
        ),
    ]


# Deleted on Sept. 11 2022
# TODO: completely remove this endpoint on Mar. 11 2023 (6 months)
# async def test_get_eurordis_members(person, eurordis_org, organisation, client):
#     organisation.membership = [
#         Membership(organisation=eurordis_org, start_date="2019-01-13", status="f")
#     ]
#     organisation.save()
#     resp = await client.get("/eurordis-members")
#     assert json.loads(resp.body) == {
#         "data": [
#             {
#                 "country": "BE",
#                 "email": "contact@bill.org",
#                 "eurordis_status": "Full",
#                 "kind": 60,
#                 "label": "Bill Org",
#                 "name": "Bill Org",
#                 "pk": "3",
#                 "resource": "organisation",
#                 "uri": f"/organisation/{organisation.pk}",
#                 "website": None,
#             }
#         ]
#     }


async def test_members_report(client, person, eurordis_org):
    o1 = Organisation(
        name="My Organisation",
        country="BE",
        email="mail@mail.com",
        membership=[
            Membership(start_date="1997-09-01", status="f", organisation=eurordis_org)
        ],
    ).save()
    o2 = Organisation(
        name="My Other Organisation",
        country="CA",
        website="https://web.site",
        membership=[
            Membership(start_date="2012-09-01", status="a", organisation=eurordis_org)
        ],
    ).save()
    resp = await client.get("/report/members.json")
    assert resp.status == 200
    data = json.loads(resp.body)
    assert data == {
        "data": [
            {
                "country": "BE",
                "country_label": "Belgium",
                "diseases": [],
                "email": "mail@mail.com",
                "is_federation": False,
                "is_national_alliance": False,
                "name": "My Organisation",
                "phone": None,
                "pk": o1.pk,
                "status": "Full",
                "website": None,
            },
            {
                "country": "CA",
                "country_label": "Canada",
                "diseases": [],
                "email": None,
                "is_federation": False,
                "is_national_alliance": False,
                "name": "My Other Organisation",
                "phone": None,
                "pk": o2.pk,
                "status": "Associate",
                "website": "https://web.site",
            },
        ]
    }


async def test_eurordis_voting_contact_xlsx_report(aclient, person, person2, eurordis_org):
    org1 = Organisation(
        name="My Organisation",
        country="BE",
        email="mail@mail.com",
        membership=[
            Membership(start_date="1997-09-01", status="f", organisation=eurordis_org)
        ],
    ).save()
    org1.roles = [
        Role(
            person=person.pk,
            organisation=org1.pk,
            is_voting=True
        ),
        Role(
            person=person2.pk,
            organisation=org1.pk
        )
    ]
    org1.save()
    org2 = Organisation(
        name="My Other Organisation",
        country="CA",
        website="https://web.site",
        membership=[
            Membership(start_date="2012-09-01", status="a", organisation=eurordis_org)
        ],
    ).save()
    org2.roles = [
        Role(
            person=person.pk,
            organisation=org2.pk,
            is_voting=True
        )
    ]
    org2.save()
    resp = await aclient.get("/report/all-voting-contacts.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        ('person_pk', 'first_name', 'last_name', 'email', 'role', 'country', 'phone', 'is_primary', 'is_default', 'is_voting', 'organisation_pk', 'organisation', 'kind', 'groups', 'eurordis_membership'),
        (person.pk, 'Bill', 'Mollison', 'bill@somewhere.org', None, 'France', None, None, None, '1', org1.pk, 'My Organisation', None, None, 'Full'),
        (person.pk, 'Bill', 'Mollison', 'bill@somewhere.org', None, 'France', None, None, None, '1', org2.pk, 'My Other Organisation', None, None, 'Associate')
    ]


async def test_eurordis_members_xlsx_report(aclient, person, eurordis_org):
    Organisation(
        name="My Organisation",
        country="BE",
        email="mail@mail.com",
        membership=[
            Membership(start_date="1997-09-01", status="f", organisation=eurordis_org)
        ],
    ).save()
    Organisation(
        name="My Other Organisation",
        country="CA",
        website="https://web.site",
        membership=[
            Membership(start_date="2012-09-01", status="a", organisation=eurordis_org)
        ],
    ).save()
    resp = await aclient.get("/report/eurordis-members.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        ("Belgium", "My Organisation", None, "mail@mail.com", "Full Member"),
        (
            "Canada",
            "My Other Organisation",
            "https://web.site",
            None,
            "Associate Member",
        ),
    ]


async def test_rdi_members_xlsx_report(aclient, person, rdi_org):
    Organisation(
        name="My Organisation",
        country="BE",
        email="mail@mail.com",
        membership=[
            Membership(start_date="1997-09-01", status="f", organisation=rdi_org)
        ],
    ).save()
    Organisation(
        name="My Other Organisation",
        country="CA",
        website="https://web.site",
        membership=[
            Membership(start_date="2012-09-01", status="a", organisation=rdi_org)
        ],
    ).save()
    resp = await aclient.get("/report/rdi-members.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        ("Belgium", "My Organisation", None, "mail@mail.com", "Full Member"),
        (
            "Canada",
            "My Other Organisation",
            "https://web.site",
            None,
            "Associate Member",
        ),
    ]


async def test_eurordis_members_status_report(aclient, person, eurordis_org):
    Organisation(
        name="My Organisation",
        country="BE",
        email="mail@mail.com",
        membership=[
            Membership(start_date="1997-09-01", status="f", organisation=eurordis_org)
        ],
    ).save()
    Organisation(
        name="My Other Organisation",
        country="CA",
        website="https://web.site",
        membership=[
            Membership(
                start_date="2012-09-01",
                status="a",
                organisation=eurordis_org,
                fees=[
                    Fee(
                        year=2019,
                        amount=123,
                        payment_date=date(2019, 2, 13),
                        receipt_sent=True,
                    )
                ],
            )
        ],
    ).save()
    resp = await aclient.get("/report/eurordis-members-status.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "email",
            "phone",
            "country",
            "is national alliance",
            "is federation",
            "adherents",
            "budget",
            "creation year",
            "board",
            "paid staff",
            "pharma %",
            "status",
            "since",
            "last payment date",
            "last payment amount",
            "receipt sent",
        ),
        (
            "3",
            "My Organisation",
            "mail@mail.com",
            None,
            "BE",
            False,
            False,
            None,
            None,
            None,
            None,
            None,
            None,
            "Full",
            datetime(1997, 9, 1),
            None,
            None,
            None,
        ),
        (
            "4",
            "My Other Organisation",
            None,
            None,
            "CA",
            False,
            False,
            None,
            None,
            None,
            None,
            None,
            None,
            "Associate",
            datetime(2012, 9, 1),
            datetime(2019, 2, 13),
            123,
            True,
        ),
    ]


async def test_rdi_members_status_report(aclient, person, rdi_org):
    Organisation(
        name="My Organisation",
        country="BE",
        email="mail@mail.com",
        membership=[
            Membership(start_date="1997-09-01", status="f", organisation=rdi_org)
        ],
    ).save()
    Organisation(
        name="My Other Organisation",
        country="CA",
        website="https://web.site",
        membership=[
            Membership(
                start_date="2012-09-01",
                status="a",
                organisation=rdi_org,
                fees=[
                    Fee(
                        year=2019,
                        amount=123,
                        payment_date=date(2019, 2, 13),
                        receipt_sent=True,
                    )
                ],
            )
        ],
    ).save()
    resp = await aclient.get("/report/rdi-members-status.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "email",
            "phone",
            "country",
            "is national alliance",
            "is federation",
            "adherents",
            "budget",
            "creation year",
            "board",
            "paid staff",
            "pharma %",
            "status",
            "since",
            "last payment date",
            "last payment amount",
            "receipt sent",
        ),
        (
            "3",
            "My Organisation",
            "mail@mail.com",
            None,
            "BE",
            False,
            False,
            None,
            None,
            None,
            None,
            None,
            None,
            "Full",
            datetime(1997, 9, 1),
            None,
            None,
            None,
        ),
        (
            "4",
            "My Other Organisation",
            None,
            None,
            "CA",
            False,
            False,
            None,
            None,
            None,
            None,
            None,
            None,
            "Associate",
            datetime(2012, 9, 1),
            datetime(2019, 2, 13),
            123,
            True,
        ),
    ]


async def test_full_eurordis_members_without_voting(aclient, person, eurordis_org):
    organisation = Organisation(
        name="My Organisation",
        membership=[
            Membership(
                start_date="1997-09-01",
                status="f",
                organisation=eurordis_org,
            ),
        ],
    )
    organisation.roles=[
        Role(
            person=person.pk,
            organisation=organisation,
        ),
    ]
    organisation.save()
    resp = await aclient.get("/report/full-eurordis-members-without-voting.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "status",
        ),
        (
            "3",
            "My Organisation",
            "Full",
        ),
    ]


async def test_withdrawn_eurordis_members_report(aclient, person, eurordis_org):
    Organisation(
        name="My Organisation",
        membership=[
            Membership(
                start_date="1997-09-01",
                end_date="2017-08-31",
                status="f",
                organisation=eurordis_org,
            ),
            Membership(start_date="2017-09-01", status="w", organisation=eurordis_org),
        ],
    ).save()
    Organisation(
        name="My Other Organisation",
        country="CA",
        website="https://web.site",
        membership=[
            Membership(
                start_date="2012-09-01",
                status="w",
                organisation=eurordis_org,
            )
        ],
    ).save()
    resp = await aclient.get("/report/withdrawn-eurordis-members.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "at",
        ),
        (
            "3",
            "My Organisation",
            datetime(2017, 9, 1),
        ),
        (
            "4",
            "My Other Organisation",
            datetime(2012, 9, 1),
        ),
    ]


async def test_withdrawn_rdi_members_report(aclient, person, rdi_org):
    Organisation(
        name="My Organisation",
        membership=[
            Membership(
                start_date="1997-09-01",
                end_date="2017-08-31",
                status="f",
                organisation=rdi_org,
            ),
            Membership(start_date="2017-09-01", status="w", organisation=rdi_org),
        ],
    ).save()
    Organisation(
        name="My Other Organisation",
        country="CA",
        website="https://web.site",
        membership=[
            Membership(
                start_date="2012-09-01",
                status="w",
                organisation=rdi_org,
            )
        ],
    ).save()
    resp = await aclient.get("/report/withdrawn-rdi-members.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values) == [
        (
            "pk",
            "name",
            "at",
        ),
        (
            "3",
            "My Organisation",
            datetime(2017, 9, 1),
        ),
        (
            "4",
            "My Other Organisation",
            datetime(2012, 9, 1),
        ),
    ]


async def test_eurordis_members_stats(aclient, person, eurordis_org):
    def add_member(start, end=None, status="f", fees={"1998": 50, "1999": 65}):
        fees = [Fee(year=year, amount=amount) for year, amount in fees.items()]
        return Organisation(
            name="xxx",
            membership=[
                Membership(
                    start_date=start,
                    end_date=end,
                    status=status,
                    fees=fees,
                    organisation=eurordis_org,
                )
            ],
        ).save()

    add_member(start="1997-03-14")
    add_member(start="1998-03-14")
    withdrawn = add_member(start="1997-06-14", end="1999-04-14")
    withdrawn.membership.append(Membership(status="w", start_date="1999-04-14"))
    resp = await aclient.get("/report/eurordis-members-stats.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values)[0] == (
        "year",
        "total",
        "withdrawn",
        "new",
        "full",
        "associate",
        "unpaid",
        "never paid",
        "exempted",
        "paid",
        "% paid + exempted",
        "total fees",
    )
    assert list(wb.active.values)[1] == (1997, 2, 0, 2, 2, 0, 2, 2, 0, 0, 0, 0)
    assert list(wb.active.values)[2] == (1998, 3, 0, 1, 3, 0, 0, 0, 0, 3, 100, 150)
    assert list(wb.active.values)[3] == (1999, 2, 0, 0, 2, 0, 0, 0, 0, 2, 100, 130)


async def test_rdi_members_stats(aclient, person, rdi_org):
    def add_member(start, end=None, status="f", fees={"1998": 50, "1999": 65}):
        fees = [Fee(year=year, amount=amount) for year, amount in fees.items()]
        return Organisation(
            name="xxx",
            membership=[
                Membership(
                    start_date=start,
                    end_date=end,
                    status=status,
                    fees=fees,
                    organisation=rdi_org,
                )
            ],
        ).save()

    add_member(start="1997-03-14")
    add_member(start="1998-03-14", status="a")
    withdrawn = add_member(start="1997-06-14", end="1999-04-14")
    withdrawn.membership.append(Membership(status="w", start_date="1999-04-14"))
    resp = await aclient.get("/report/rdi-members-stats.xlsx")
    assert resp.status == 200
    wb = load_workbook(filename=BytesIO(resp.body))
    assert list(wb.active.values)[0] == (
        "year",
        "total",
        "withdrawn",
        "new",
        "full",
        "associate",
        "unpaid",
        "never paid",
        "exempted",
        "paid",
        "% paid + exempted",
        "total fees",
    )
    assert list(wb.active.values)[1] == (1997, 2, 0, 2, 2, 0, 2, 2, 0, 0, 0, 0)
    assert list(wb.active.values)[2] == (1998, 3, 0, 1, 2, 1, 0, 0, 0, 3, 100, 150)
    assert list(wb.active.values)[3] == (1999, 2, 0, 0, 1, 1, 0, 0, 0, 2, 100, 130)


async def test_populate_basket(aclient, person, organisation, person2):
    resp = await aclient.put(
        "/basket", body={"refs": [person.pk, organisation.pk, person2.pk]}
    )
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "not_found": ["2"],
        "resources": [{"person": "1"}, {"person": "3"}],
    }


async def test_ghost_mode_can_be_set_from_header(aclient, organisation):
    modified_at = organisation.modified_at

    time.sleep(1)
    resp = await aclient.post("/organisation", body={"name": "Fukuoka Inc."})
    assert resp.status == 200

    organisation.reload()
    assert modified_at == organisation.modified_at


async def test_activity(aclient):
    now = utcnow()
    resp = await aclient.post(
        "/activity",
        body={
            "at": now.isoformat(),
            "by": "valid@email.org",
            "location": "/somewhere",
            "extra": "some extra data",
        },
    )
    assert resp.status == 204
    resp = await aclient.get("/activity")
    assert resp.status == 200
    assert json.loads(resp.body) == {
        "data": [
            {
                "at": now.timestamp(),
                "by": "valid@email.org",
                "location": "/somewhere",
                "extra": "some extra data",
            }
        ]
    }
    await aclient.post(
        "/activity",
        body={
            "at": now.isoformat(),
            "by": "valid@email.org",
            "location": "/somewhere",
            "extra": "some extra data",
        },
    )
    resp = await aclient.get("/activity?limit=1")
    assert resp.status == 200
    assert len(json.loads(resp.body)["data"]) == 1


async def test_activity_as_csv(aclient):
    now = utcnow()
    resp = await aclient.post(
        "/activity",
        body={
            "at": now.isoformat(),
            "by": {"pk": "22", "label": "foobar"},
            "location": "/somewhere",
        },
    )
    assert resp.status == 204
    resp = await aclient.get(
        "/activity.csv",
    )
    assert resp.status == 200
    assert resp.body.decode().startswith("pk;name;at;location\r\n22;foobar;")
    assert resp.body.decode().endswith(";/somewhere\r\n")


async def test_search_activity_as_csv(aclient):
    now = utcnow()
    resp = await aclient.post(
        "/activity",
        body={
            "at": now.isoformat(),
            "by": {"pk": "22", "label": "foobar"},
            "location": "/?q=epag&group=%3C5|ePAG%20-%20European%20Patient%20Advocacy%20Group&showFacets=true&resource=person",
        },
    )
    assert resp.status == 204
    resp = await aclient.get(
        "/search-activity.csv",
    )
    assert resp.status == 200
    lines = resp.body.decode().split("\r\n")
    assert lines[0] == "pk;name;at;key;value;label"
    assert lines[1].startswith("22;foobar;")
    assert lines[1].endswith(";q;epag;epag")
    assert lines[2].startswith("22;foobar;")
    assert lines[2].endswith(";group;<5;ePAG - European Patient Advocacy Group")
    assert lines[3].startswith("22;foobar;")
    assert lines[3].endswith(";showFacets;true;true")
    assert lines[4].startswith("22;foobar;")
    assert lines[4].endswith(";resource;person;person")


async def test_upload_attachment(aclient, person):
    resp = await aclient.post(
        f"/contact/{person.pk}/attachments",
        files={"data": (b"1234", "myfile.pdf")},
    )
    assert resp.status == 200
    assert list(Person.get(person.pk).attachments.values())[0].read() == b"1234"
    resp = await aclient.get(f"/person/{person.pk}")
    data = json.loads(resp.body)
    assert data["attachments"][0]["id"]
    assert data["attachments"][0]["filename"] == "myfile.pdf"
    assert data["attachments"][0]["content_type"] == "application/pdf"


async def test_download_attachment(aclient, person):
    f = BytesIO(b"1234567")
    f.filename = "foo"
    f.content_type = "f/bar"
    attachment = person.add_attachment(f)
    person.save()
    resp = await aclient.get(f"/contact/{person.pk}/attachments/badvalue")
    assert resp.status == 404
    assert json.loads(resp.body) == {"error": "No attachment found with id badvalue"}
    resp = await aclient.get(f"/contact/{person.pk}/attachments/{attachment.id}")
    assert resp.status == 200
    assert resp.body == b"1234567"
    assert resp.headers["Content-Length"] == 7


async def test_delete_attachment(aclient, person):
    f = BytesIO(b"1234567")
    f.filename = "foo"
    f.content_type = "f/bar"
    attachment = person.add_attachment(f)
    person.save()
    await aclient.delete(f"/contact/{person.pk}/attachments/{attachment.id}")
    assert len(Person.get(person.pk).attachments) == 0


async def test_compare_grouping(aclient, disease):
    Disease(
        orpha_number=60,
        name="Alpha-1-antitrypsin deficiency",
        groupings=[{"name": "rareliver"}, {"name": "lung"}],
    ).save()
    resp = await aclient.post(
        "/compare-groupings",
        files={
            "data": (
                "orphacode=grouping\r\n60=Rare Hepatic ; Rare Pulmonary\r\n320=Rare Endocrine ; Rare Renal\r\n",
                "Grouping.txt",
            )
        },
    )
    assert resp.status == 200
    csv = resp.body.decode().splitlines()
    assert csv == [
        "orphanumber;ours;others;status;'+ours;'+others;name;group of type",
        "60;lung,rareliver;lung,rareliver;=;;;Alpha-1-antitrypsin deficiency;disorder",
        "320;;endo,erknet;not in RD-CODE;;endo,erknet;;",
        "12345;bond,eye;;only in CDB;bond,eye;;Congenital deficiency in alpha-fetoprotein;disorder",
    ]


async def test_readonly_mode(aclient, monkeypatch):
    monkeypatch.setattr(config, "READONLY", True)
    resp = await aclient.get("/search?q=pat")
    assert resp.status == 200
    resp = await aclient.post("/person/import")
    assert resp.status == 405
    assert json.loads(resp.body) == {"error": "Contact Database is readonly"}


async def test_epag_members(aclient, group, person, organisation):
    config.EPAG_GROUP = group.pk
    group2 = Group(name="ERN - whatever", kind="epag").save()
    person.roles[0].add_group(group=group).save()
    person.roles[0].add_group(group=group2, role="Patient Representative").save()
    resp = await aclient.get("/report/epag-members.json")
    assert resp.status == 200
    assert json.loads(resp.body) == [
        {
            "country": "FR",
            "ern": "ERN - whatever",
            "fullname": "Bill Mollison",
            "organisation": "Bill Org",
            "website": None,
        }
    ]
