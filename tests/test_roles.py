import pytest
from mongoengine.errors import NotUniqueError

from eurordis.models import Organisation, Person, Role, GroupBelonging


def test_save_person_dont_create_empty_version(person, organisation):
    count = len(person.versions)
    assert person.roles
    person.roles = person.roles[:]
    person.first_name = "Coco"
    person.save()
    assert len(person.versions) == count + 1


def test_save_person_roles_with_new_person(organisation):
    new_person = Person(last_name="Duchenne")
    new_person.roles = [Role(person=new_person, organisation=organisation)]
    new_person.save()
    assert Role.get(person=new_person, organisation=organisation)
    assert len(new_person.versions) == 2
    assert new_person.versions[0]["diff"] == {
        "roles": [
            {
                "new": {
                    "events": [],
                    "groups": [],
                    "is_primary": False,
                    "is_default": False,
                    "is_voting": False,
                    "is_archived": False,
                    "organisation": "2",
                    "name": None,
                    "phone": None,
                    "email": None,
                },
                "subject": "Bill Org",
                "verb": "create",
            }
        ]
    }


def test_add_person_role(organisation, person2):
    count = len(person2.versions)
    assert not Role.objects.filter(person=person2, organisation=organisation)
    person2.roles = [Role(person=person2, organisation=organisation)]
    person2.save()
    assert Role.get(person=person2, organisation=organisation)
    assert len(person2.versions) == count + 1
    assert person2.versions[0]["diff"] == {
        "roles": [
            {
                "new": {
                    "events": [],
                    "groups": [],
                    "is_primary": False,
                    "is_default": False,
                    "is_voting": False,
                    "is_archived": False,
                    "organisation": "2",
                    "name": None,
                    "phone": None,
                    "email": None,
                },
                "subject": "Bill Org",
                "verb": "create",
            }
        ]
    }


def test_update_person_role(person, organisation):
    count = len(person.versions)
    role = person.roles[0]
    assert role.is_primary
    role.is_primary = False
    person.roles = [role]
    person.save()
    assert Role.get(person=person, organisation=organisation).is_primary is False
    assert len(person.versions) == count + 1


def test_delete_person_role(person, organisation):
    count = len(person.versions)
    assert Role.objects.filter(person=person, organisation=organisation)
    person.roles = []
    person.save()
    assert not Role.objects.filter(person=person, organisation=organisation)
    assert len(person.versions) == count + 1


def test_update_person_role_with_multiple_roles(person, organisation):
    org2 = Organisation(name="another").save()
    org3 = Organisation(name="yet another").save()
    person.roles = [
        Role(person=person, organisation=organisation, name="primary"),
        Role(person=person, organisation=org2, name="second"),
        Role(person=person, organisation=org3, name="third", is_primary=False),
    ]
    person.save()
    count = len(person.versions)
    assert Role.objects.filter(person=person).count() == 3
    person.roles = [
        Role(person=person, organisation=organisation, name="primary"),
        Role(person=person, organisation=org3, name="update", is_primary=True),
    ]
    person.save()
    assert Role.objects.filter(person=person).count() == 2

    role = Role.get(person=person, organisation=org3)
    assert role.is_primary is True
    assert role.name == "update"

    role = Role.get(person=person, organisation=organisation)
    assert role.name == "primary"
    assert role.is_primary is False

    assert len(person.versions) == count + 1


def test_save_organisation_roles_with_new_organisation(person):
    new_org = Organisation(name="Duchenne")
    new_org.roles = [Role(person=person, organisation=new_org)]
    new_org.save()
    assert Role.get(person=person, organisation=new_org)


def test_add_organisation_role(person):
    org2 = Organisation(name="Duchenne").save()
    assert not Role.objects.filter(person=person, organisation=org2)
    org2.roles = [Role(person=person, organisation=org2)]
    org2.save()
    assert Role.get(person=person, organisation=org2)


def test_update_organisation_role(person, organisation):
    role = organisation.roles[0]
    assert role.is_primary
    role.is_primary = False
    organisation.roles = [role]
    organisation.save()
    assert Role.get(person=person, organisation=organisation).is_primary is False


def test_update_organisation_role_with_multiple_roles(person, person2, organisation):
    person3 = Person(last_name="Magalon").save()
    organisation.roles = [
        Role(person=person, organisation=organisation, name="primary"),
        Role(person=person2, organisation=organisation, name="second"),
        Role(person=person3, organisation=organisation, name="third", is_primary=False),
    ]
    organisation.save()
    assert Role.objects.filter(organisation=organisation).count() == 3
    organisation.roles = [
        Role(person=person, organisation=organisation, name="primary"),
        Role(person=person3, organisation=organisation, name="update", is_primary=True),
    ]
    organisation.save()
    assert Role.objects.filter(organisation=organisation).count() == 2

    role = Role.get(person=person3, organisation=organisation)
    assert role.is_primary is True
    assert role.name == "update"

    role = Role.get(person=person, organisation=organisation)
    assert role.name == "primary"
    assert role.is_primary is False


def test_delete_group(person, group, organisation):
    assert len(person.versions) == 2
    assert len(organisation.versions) == 2
    roles = person.roles
    roles[0].groups.append(GroupBelonging(group=group, role="Member"))
    person.roles = roles[:]  # Force saving.
    person.save()
    assert Person.get(person.pk).roles[0].groups
    assert Organisation.get(organisation.pk).roles[0].groups
    group.delete()
    assert not Person.get(person.pk).roles[0].groups
    assert not Organisation.get(organisation.pk).roles[0].groups
    assert len(person.versions) == 4
    # Group deleted
    assert person.versions[0]["diff"] == {
        "roles": [
            {
                "subject": "Bill Org",
                "verb": "update",
                "subordinates": [
                    {
                        "subject": "groups",
                        "verb": "delete",
                        "old": [{"is_active": True, "group": "1", "role": "Member"}],
                    }
                ],
            }
        ]
    }
    # Group added
    assert person.versions[1].diff == {
        "roles": [
            {
                "subject": "Bill Org",
                "verb": "update",
                "subordinates": [
                    {
                        "subject": "groups",
                        "verb": "update",
                        "old": [],
                        "new": [{"is_active": True, "group": "1", "role": "Member"}],
                    }
                ],
            }
        ]
    }
    assert len(organisation.versions) == 4
    # Group deleted.
    assert organisation.versions[0].diff == {
        "roles": [
            {
                "subject": "Bill Mollison",
                "subordinates": [
                    {
                        "old": [{"group": "1", "is_active": True, "role": "Member"}],
                        "subject": "groups",
                        "verb": "delete",
                    }
                ],
                "verb": "update",
            }
        ]
    }
    assert organisation.versions[1].diff == {
        "roles": [
            {
                "subject": "Bill Mollison",
                "subordinates": [
                    {
                        "new": [{"group": "1", "is_active": True, "role": "Member"}],
                        "old": [],
                        "subject": "groups",
                        "verb": "update",
                    }
                ],
                "verb": "update",
            }
        ]
    }


def test_cannot_add_twice_is_default(person, organisation):
    Role.get(person=person, organisation=organisation).delete()
    other = Organisation(name="other").save()
    person.roles = [
        Role(person=person, organisation=organisation, is_default=True),
        Role(person=person, organisation=other, is_default=True),
    ]
    with pytest.raises(NotUniqueError):
        person.save()


def test_cannot_add_twice_is_voting(person, person2, organisation):
    Role.get(person=person, organisation=organisation).delete()
    person.roles = [
        Role(person=person, organisation=organisation, is_voting=True),
    ]
    person.save()
    person2.roles = [
        Role(person=person2, organisation=organisation, is_voting=True),
    ]
    with pytest.raises(NotUniqueError):
        person2.save()
