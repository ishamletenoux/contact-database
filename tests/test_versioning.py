import time

import pytest
from eurordis.models import Membership, Organisation, Person, Role, Version
from mongoengine.errors import ValidationError


def test_version_should_be_created_at_creation():
    assert not Version.objects.count()
    inst = Person(first_name="Erri", last_name="De Luca", email="erri@deluca.it").save()
    assert Version.objects.count() == 1
    assert len(inst.history) == 1
    assert inst.history[0].diff == {
        "email": {"new": "erri@deluca.it", "subject": "email", "verb": "create"},
        "first_name": {"new": "Erri", "subject": "first_name", "verb": "create"},
        "last_name": {"new": "De Luca", "subject": "last_name", "verb": "create"},
    }


def test_diff_is_not_computed_for_first_version_of_object_imported_from_filemaker():
    assert not Version.objects.count()
    inst = Person(
        first_name="Erri",
        last_name="De Luca",
        email="erri@deluca.it",
        filemaker_pk="co23",
    ).save()
    assert Version.objects.count() == 1
    assert len(inst.history) == 0
    inst.first_name = "Enrico"
    inst.save()
    assert len(inst.history) == 1
    assert inst.history[0].diff == {
        "first_name": {
            "new": "Enrico",
            "old": "Erri",
            "subject": "first_name",
            "verb": "update",
        }
    }


def test_no_version_created_on_validation_error(person):
    assert Version.objects.count() == 1
    person.email = "invalid"
    with pytest.raises(ValidationError):
        person.save()
    assert len(person.versions) == 1
    assert Version.objects.count() == 1


def test_version_should_be_backuped_when_updating(person):
    old = Person.get(person.pk)
    assert Version.objects.count() == 1
    time.sleep(1)
    person.save()
    assert old.version != person.version
    # Nothing changed
    assert Version.objects.count() == 1
    person.first_name = "changed"
    person.save()
    assert old.version != person.version
    assert Version.objects.count() == 2


def test_can_save_twice_same_instance(person):
    version = person.version
    person.save()
    assert person.version == version + 1
    person.save()
    assert person.version == version + 2


def test_version_diff_should_keep_track_of_simple_fields(person):
    person.first_name = "Freddy"
    person.email = None
    person.save()
    assert len(person.history) == 2
    assert person.history[0].diff == {
        "email": {"old": "bill@somewhere.org", "subject": "email", "verb": "delete"},
        "first_name": {
            "new": "Freddy",
            "old": "Bill",
            "subject": "first_name",
            "verb": "update",
        },
    }


def test_version_diff_should_skip_empty_diffs(person):
    assert len(person.history) == 1
    person.save()
    assert len(person.history) == 1


def test_diff_should_keep_track_of_list_fields():
    person = Person(last_name="Chaussette").save()
    person.languages = ["fr", "ru"]
    person.save()
    assert len(person.history) == 2
    assert person.history[0].diff == {
        "languages": [
            {"subject": "French", "value": "fr", "verb": "create"},
            {"subject": "Russian", "value": "ru", "verb": "create"},
        ]
    }
    person.languages = ["fr", "it"]
    person.save()
    assert len(person.history) == 3
    assert person.history[0].diff == {
        "languages": [
            {"subject": "Russian", "value": "ru", "verb": "delete"},
            {"subject": "Italian", "value": "it", "verb": "create"},
        ]
    }


def test_diff_should_keep_track_of_embedded_fields(person, eurordis_org):
    organisation = Organisation(
        name="member",
        membership=[Membership(organisation=eurordis_org, start_date="2019-01-13")],
    ).save()
    assert len(organisation.history) == 1
    organisation.membership = [
        Membership(
            organisation=eurordis_org, start_date="2019-01-13", end_date="2019-03-19"
        )
    ]
    organisation.save()
    assert len(organisation.history) == 2
    assert organisation.history[0].diff == {
        "membership": [
            {
                "subject": "Eurordis",
                "subordinates": [
                    {
                        "new": "2019-03-19",
                        "subject": "end_date",
                        "verb": "create",
                    }
                ],
                "verb": "update",
            }
        ]
    }


def test_diff_should_not_consider_falsy_values(organisation):
    organisation.comment = ""
    organisation.save()
    organisation.replace({"name": "delete all other fields"})
    assert organisation.history[0].diff == {
        "country": {"old": "BE", "subject": "country", "verb": "delete"},
        "email": {"old": "contact@bill.org", "subject": "email", "verb": "delete"},
        "kind": {
            "new": None,
            "old": "Patient Organisation",
            "subject": "kind",
            "verb": "delete",
        },
        "name": {
            "new": "delete all other fields",
            "old": "Bill Org",
            "subject": "name",
            "verb": "update",
        },
    }


def test_diff_with_diseases_field(organisation, disease):
    organisation.diseases = [disease]
    organisation.save()
    assert len(organisation.history) == 3
    assert organisation.history[0].diff == {
        "diseases": [
            {
                "subject": "Congenital deficiency in alpha-fetoprotein",
                "value": 12345,
                "verb": "create",
            }
        ]
    }


def test_diff_event(person, event):
    old_name = event.name
    assert len(event.history) == 1
    event.name = "New name"
    event.save()
    assert len(event.history) == 2
    assert event.history[0].diff == {
        "name": {
            "new": "New name",
            "old": old_name,
            "subject": "name",
            "verb": "update",
        }
    }


def test_history(person, organisation):
    assert len(list(Version.history())) == 4
    old_name = person.last_name
    person.last_name = "New Name"
    person.save()
    diff = list(Version.history())[0]
    assert diff == person.history[0]
    assert diff.diff == {
        "last_name": {
            "new": "New Name",
            "old": old_name,
            "subject": "last_name",
            "verb": "update",
        }
    }
    time.sleep(1)
    old_name = organisation.name
    organisation.name = "Best Name Ever"
    organisation.save()
    assert len(list(Version.history())) == 6
    diff = list(Version.history())[0]
    assert diff == organisation.history[0]
    assert diff.diff == {
        "name": {
            "new": "Best Name Ever",
            "old": old_name,
            "subject": "name",
            "verb": "update",
        }
    }


def test_history_should_skip_empty_diffs(person, organisation):
    assert len(list(Version.history())) == 4
    person.save()
    assert len(list(Version.history())) == 4


def test_history_should_include_roles_diffs(person, person2, organisation):
    assert len(person2.versions) == 1
    assert len(organisation.versions) == 2
    Role(person=person2, organisation=organisation).save()
    assert len(person2.versions) == 2
    assert len(organisation.versions) == 3
    assert len(person2.history) == 2
    assert person2.history[0].diff == {
        "roles": [
            {
                "new": {
                    "is_primary": False,
                    "is_default": False,
                    "is_voting": False,
                    "is_archived": False,
                    "name": None,
                    "phone": None,
                    "email": None,
                    "organisation": organisation.pk,
                    "groups": [],
                    "events": [],
                },
                "subject": organisation.label,
                "verb": "create",
            }
        ]
    }
    assert len(organisation.history) == 3
    assert organisation.history[0].diff == {
        "roles": [
            {
                "new": {
                    "is_primary": False,
                    "is_default": False,
                    "is_voting": False,
                    "is_archived": False,
                    "name": None,
                    "phone": None,
                    "email": None,
                    "person": person2.pk,
                    "groups": [],
                    "events": [],
                },
                "subject": person2.label,
                "verb": "create",
            }
        ]
    }


def test_history_should_include_roles_updates(person, organisation):
    assert len(person.versions) == 2
    assert len(organisation.versions) == 2
    role = Role.get(person=person, organisation=organisation)
    role.is_primary = not role.is_primary
    role.save()
    assert len(person.versions) == 3
    assert len(person.history) == 3
    assert person.history[0].diff == {
        "roles": [
            {
                "subject": "Bill Org",
                "subordinates": [
                    {
                        "new": role.is_primary,
                        "old": not role.is_primary,
                        "subject": "is_primary",
                        "verb": "update",
                    }
                ],
                "verb": "update",
            }
        ]
    }
    assert len(organisation.versions) == 3
    assert len(organisation.history) == 3
    assert organisation.history[0].diff == {
        "roles": [
            {
                "subject": "Bill Mollison",
                "subordinates": [
                    {
                        "new": role.is_primary,
                        "old": not role.is_primary,
                        "subject": "is_primary",
                        "verb": "update",
                    }
                ],
                "verb": "update",
            }
        ]
    }
    role.delete()
    assert len(person.versions) == 4
    assert len(person.history) == 4
    assert person.history[0].diff == {
        "roles": [
            {
                "old": {
                    "email": "bmollison@bill.org",
                    "is_primary": False,
                    "is_default": False,
                    "is_voting": False,
                    "is_archived": False,
                    "name": "CEO",
                    "organisation": organisation.pk,
                    "phone": "+1234567890",
                    "groups": [],
                    "events": [],
                },
                "subject": "Bill Org",
                "verb": "delete",
            }
        ]
    }
    assert len(organisation.versions) == 4
    assert len(organisation.history) == 4
    assert organisation.history[0].diff == {
        "roles": [
            {
                "old": {
                    "email": "bmollison@bill.org",
                    "is_primary": False,
                    "is_default": False,
                    "is_voting": False,
                    "is_archived": False,
                    "name": "CEO",
                    "person": person.pk,
                    "phone": "+1234567890",
                    "groups": [],
                    "events": [],
                },
                "subject": "Bill Mollison",
                "verb": "delete",
            }
        ]
    }
