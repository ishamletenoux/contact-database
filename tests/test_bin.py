from datetime import timedelta
from pathlib import Path

import pytest
from eurordis import config
from eurordis.bin import hard_delete, import_extra_diseases
from eurordis.helpers import utcnow
from eurordis.models import Contact, Disease, Organisation


@pytest.fixture(autouse=True, scope="function")
def module_setup_teardown(keyword):
    config.TOBEDELETED_KEYWORD = keyword.pk


def test_hard_delete(bot, person, organisation, person2, keyword):
    person2.keywords = [keyword.pk]
    person2.save()
    organisation.keywords = [keyword.pk]
    organisation.save()
    Contact.objects(pk__in=[person2.pk, organisation.pk]).update(
        modified_at=utcnow() - timedelta(days=8)
    )
    hard_delete()
    contacts = Contact.objects.all()
    assert len(contacts) == 2
    assert {bot.pk, person.pk} == set([c.pk for c in contacts])


def test_hard_delete_cant_deleted_linked_person(bot, person, organisation, keyword):
    assert Contact.objects.count() == 3
    person.keywords = [keyword.pk]
    person.save()
    Contact.objects(pk=person.pk).update(modified_at=utcnow() - timedelta(days=8))
    hard_delete()
    assert Contact.objects.count() == 3


def test_hard_delete_do_not_deleted_recent_soft_delete(
    bot, person, organisation, keyword
):
    assert Contact.objects.count() == 3
    organisation.keywords = [keyword.pk]
    organisation.save()
    Contact.objects(pk=organisation.pk).update(modified_at=utcnow() - timedelta(days=6))
    hard_delete()
    assert Contact.objects.count() == 3


def test_import_extra_diseases(disease, organisation):
    config.ROOT = Path(__file__).parent
    config.NON_RARE_ORPHA_NUMBER = disease.pk
    d1 = Disease(roots=[disease], ancestors=[disease], orpha_number=1, name="D1").save()
    Disease(roots=[disease], ancestors=[disease], orpha_number=2, name="D2").save()
    d3 = Disease(roots=[disease], ancestors=[disease], orpha_number=3, name="D3").save()
    d4 = Disease(roots=[disease], ancestors=[disease], orpha_number=4, name="D4").save()
    organisation.diseases = [d4]
    organisation.save()
    assert Disease.objects.count() == 5
    import_extra_diseases()
    assert Disease.objects.count() == 5
    d1.reload()
    d3.reload()
    disease.reload()
    assert d1.name == "new name"
    assert d1.status == "non rare"
    assert d3.name == "D3"
    with pytest.raises(Disease.DoesNotExist):
        Disease.get(2)
    assert Disease.get(4)  # Not delete, because linked to some contact.
    created = Disease.get(5)
    assert created.name == "D5"
    assert created.status == "obsolete"
    assert created.roots == [disease]
    assert created.ancestors == [disease]
    assert created in disease.descendants
