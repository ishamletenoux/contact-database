import time
from datetime import datetime
from io import BytesIO

import pytest
from eurordis import config, helpers, session
from eurordis.models import (
    Contact,
    Event,
    Fee,
    Membership,
    Organisation,
    Person,
    RoleToEvent,
    Role,
)
from mongoengine.errors import ValidationError


def test_init_person(organisation):
    person = Person(first_name="Cat", last_name="Power", email="cat@power.org").save()
    assert person.email == "cat@power.org"
    assert person.last_name == "Power"
    assert person.version == 1
    person.save()
    assert person.version == 2


def test_can_create_person_with_upper_title(organisation):
    person = Person(last_name="Power", title="Ms").save()
    assert person.title == "ms"


def test_get_email_person(person):
    org1 = Organisation(
        name="Org 1",
    ).save()
    role1 = Role(
        person=person,
        organisation=org1,
        is_default=True
    ).save()
    org2 = Organisation(
        name="Org 2",
    ).save()
    role2 = Role(
        person=person,
        organisation=org2,
        is_primary=True,
        email="contact@primary.org"
    ).save()
    org3 = Organisation(
        name="Org 3",
    ).save()
    role3 = Role(
        person=person,
        organisation=org3,
        is_primary=True,
        email="contact@role.org"
    ).save()
    assert person.get_email() == "bill@somewhere.org"
    person.email = None
    person.save()
    assert person.get_email() == "contact@primary.org"
    role1.email = "contact@default.org"
    role1.save()
    assert person.get_email() == "contact@default.org"
    role1.email = None
    role1.save()
    role2.email = None
    role2.save()
    assert person.get_email() == "contact@role.org"


def test_save_contact_update_modified(organisation):
    modified_at = organisation.modified_at

    # WHEN
    time.sleep(1)
    organisation.save()

    # THEN
    organisation.reload()
    assert modified_at < organisation.modified_at


def test_ghost_save_do_not_update_modified_at(organisation):
    modified_at = organisation.modified_at
    assert len(organisation.versions) == 2

    # WHEN
    time.sleep(1)
    organisation.name = "changed"
    organisation.save(ghost=True)

    # THEN
    organisation.reload()
    assert modified_at == organisation.modified_at
    # But version should be created
    assert len(organisation.versions) == 3
    assert organisation.versions[0].ghost is True


def test_organisation_roles(person, organisation, person2):
    assert len(organisation.roles) == 1
    Role(person=person2, is_primary=False, organisation=organisation).save()
    assert len(organisation.roles) == 2
    assert organisation.roles[0].person == person
    assert organisation.roles[1].person == person2


def test_organisation_roles_are_sorted(organisation, person):
    role = Role.get(organisation=organisation.pk, person=person.pk)
    role.delete()
    assert len(organisation.roles) == 0
    person1 = Person(first_name="Cat", last_name="AAA", email="aaa@power.org").save()
    person2 = Person(first_name="Cat", last_name="BBB", email="bbb@power.org").save()
    person3 = Person(first_name="Cat", last_name="CCC", email="ccc@power.org").save()
    person4 = Person(first_name="Cat", last_name="DDD", email="ddd@power.org").save()
    Role(person=person1, is_primary=False, organisation=organisation).save()
    Role(person=person2, is_primary=True, organisation=organisation).save()
    Role(person=person3, is_primary=True, organisation=organisation).save()
    Role(person=person4, is_primary=False, organisation=organisation).save()
    assert len(organisation.roles) == 4
    assert organisation.roles[0].person == person2
    assert organisation.roles[1].person == person3
    assert organisation.roles[2].person == person1
    assert organisation.roles[3].person == person4


def test_organisation_roles_should_be_removed_with_person(organisation, person2):
    Role(person=person2, is_primary=True, organisation=organisation).save()
    assert len(organisation.roles) == 2
    person2.delete()
    assert len(organisation.roles) == 1


def test_search_contact(person, organisation):
    assert person in Contact.objects.search("bill")
    assert organisation in Contact.objects.search("bill")
    assert person in Person.objects.search("bill")
    assert organisation not in Person.objects.search("bill")
    assert person in Person.objects.search("bil mol")


def test_organisation_grouping(organisation, disease):
    assert not organisation.groupings
    organisation.diseases.append(disease)
    organisation.save()
    assert set(organisation.groupings) == {"bond", "eye"}


def test_role_reset_value(organisation, person2):
    role = Role(organisation=organisation, person=person2, name="Foo").save()
    assert role.name == "Foo"
    role.name = None
    role.save()
    assert Role.get(organisation=organisation, person=person2).name is None


def test_ft_computation(organisation):
    organisation.acronym = "ARMGH"
    organisation.save()
    assert organisation.ft == [
        {"score": 1, "trigrams": ["^bi", "bil", "^or", "org"]},
        {"score": 0.9, "trigrams": ["^ar", "arm", "rmg"]},
    ]


def test_delete_event(role, event):
    other = Event(
        name="Dummy Awards",
        kind="awards",
        start_date=datetime(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()
    role.events.append(RoleToEvent(event=event, role="Staff"))
    role.events.append(RoleToEvent(event=other, role="Speaker"))
    role.save()
    role.reload()
    assert len(role.events) == 2
    event.delete()
    role.reload()
    assert len(role.events) == 1
    role.reload()
    assert role.events[0].event == other


def test_cannot_create_duplicate_events(role, event):
    role.events.append(RoleToEvent(event=event, role="Staff"))
    role.events.append(RoleToEvent(event=event, role="Speaker"))
    role.save()
    assert Role.get(person=role.person, organisation=role.organisation).events == [
        RoleToEvent(event=event, role="Speaker")
    ]


def test_cannot_create_duplicate_disease_relation(person, event, disease):
    person.diseases = [disease, disease]
    person.save()
    assert Person.get(person.pk).diseases == [disease]


def test_delete_organisation_with_members(organisation):
    Organisation(
        name="member",
        membership=[Membership(organisation=organisation, start_date="2019-01-13")],
    ).save()
    with pytest.raises(ValueError):
        organisation.delete()
    assert Organisation.get(id=organisation.pk)


def test_can_create_duplicate_active_membership(organisation, eurordis_org):
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2019-01-13")
    ]
    organisation.save()
    organisation.membership = [
        Membership(
            organisation=eurordis_org, start_date="2009-01-13", end_date="2019-01-12"
        ),
        Membership(organisation=eurordis_org, start_date="2019-01-13"),
    ]
    organisation.save()
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2009-01-13"),
        Membership(organisation=eurordis_org, start_date="2019-01-13"),
    ]
    with pytest.raises(ValidationError):
        organisation.save()


def test_cannot_create_membership_to_null_organisation(organisation):
    organisation.membership = [Membership(organisation=None, start_date="2019-01-13")]
    with pytest.raises(ValidationError):
        organisation.save()


def test_membership_sort_fees(organisation):
    organisation.membership = [
        Membership(
            organisation=organisation,
            start_date="2019-01-13",
            fees=[
                Fee(year=2010, amount=10, payment_date="2010-01-01"),
                Fee(year=2019, amount=50, payment_date="2019-09-09"),
            ],
        )
    ]
    organisation.save()
    assert organisation.membership[0].fees[0].year == 2019


def test_replace_do_not_reset_readonly_fields(person):
    person.filemaker_pk = "co222"
    person.save()
    created_by = person.created_by
    created_at = person.created_at
    version = person.version
    filemaker_pk = person.filemaker_pk
    assert created_by
    assert created_at
    assert version
    assert filemaker_pk
    person.replace({"last_name": "Davince"})
    assert person.created_at == created_at
    assert person.created_by == created_by
    assert person.version == version + 1
    assert person.filemaker_pk == filemaker_pk


def test_empty_listfield_is_uniformized(person):
    person.languages = []
    person.save()
    assert Person.get(id=person.pk).languages == []
    person.languages = None
    person.save()
    assert Person.get(id=person.pk).languages == []
    person.replace({"last_name": "foo", "languages": None})
    assert Person.get(id=person.pk).languages == []
    person.replace({"last_name": "foo"})
    assert Person.get(id=person.pk).languages == []


def test_email_should_be_lowercased(person):
    person.email = "UPPER@CaSe.oRg"
    person.save()
    person.reload()
    assert person.email == "upper@case.org"


def test_scopes(person):
    assert person.compute_scopes("foo@bar.org") == []
    assert person.compute_scopes(config.STAFF[0]) == ["delete", "import", "admin"]


def test_membership_is_active_to(organisation):
    other = Organisation(
        name="member",
        membership=[Membership(organisation=organisation, start_date="2019-01-13")],
    ).save()
    assert other.membership[0].is_active_to(organisation)
    other = Organisation(
        name="member",
        membership=[
            Membership(organisation=organisation, start_date="2019-01-13", status="w")
        ],
    ).save()
    assert not other.membership[0].is_active_to(organisation)
    other = Organisation(
        name="member",
        membership=[
            Membership(
                organisation=organisation,
                start_date="2019-01-13",
                end_date="2019-03-27",
                status="f",
            )
        ],
    ).save()
    assert not other.membership[0].is_active_to(organisation)


def test_dates_are_aware(person):
    assert person.created_at.tzinfo
    fromdb = Person.get(person.pk)
    assert fromdb.created_at.tzinfo


def test_can_set_creation_date_to_current(organisation):
    organisation.creation_year = helpers.utcnow().year
    organisation.save()


def test_get_eurordis_members(person, eurordis_org, organisation):
    assert not Organisation.organisation_members(eurordis_org)
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2019-01-13", status="f")
    ]
    organisation.save()
    assert list(Organisation.organisation_members(eurordis_org)) == [organisation]


def test_get_rdi_members(person, rdi_org, organisation):
    assert not Organisation.organisation_members(rdi_org)
    organisation.membership = [
        Membership(organisation=rdi_org, start_date="2019-01-13", status="f")
    ]
    organisation.save()
    assert list(Organisation.organisation_members(rdi_org)) == [organisation]


def test_eurordis_status(person, eurordis_org, organisation):
    assert organisation.organisation_membership_status(eurordis_org) is None
    organisation.membership = [
        Membership(organisation=eurordis_org, start_date="2019-01-13", status="f")
    ]
    organisation.save()
    assert organisation.organisation_membership_status(eurordis_org) == "Full"


def test_rdi_status(person, rdi_org, organisation):
    assert organisation.organisation_membership_status(rdi_org) is None
    organisation.membership = [
        Membership(organisation=rdi_org, start_date="2019-01-13", status="f")
    ]
    organisation.save()
    assert organisation.organisation_membership_status(rdi_org) == "Full"


def test_cant_delete_person_if_version_exist(organisation, person, person2):
    with session.as_user(person2):
        organisation.name = "changed"
        organisation.save()
    # Now we set modified_by as "person", but person2 still has a version in history.
    with session.as_user(person):
        organisation.save()
    with pytest.raises(ValueError):
        person2.delete()
    assert Organisation.get(id=organisation.pk)


def test_empty_event_role_is_cast_to_none(role, event):
    collection = Role._get_collection()
    role.events.append(RoleToEvent(event=event, role=""))
    role.save()
    assert (
        Role.get(person=role.person, organisation=role.organisation).events[0].role
        is None
    )
    assert "role" not in collection.find_one({"_id": role.pk})["events"][0]
    role.events.append(RoleToEvent(event=event, role=None))
    role.save()
    assert (
        Role.get(person=role.person, organisation=role.organisation).events[0].role
        is None
    )
    assert "role" not in collection.find_one({"_id": role.pk})["events"][0]
    role.events.append(RoleToEvent(event=event))
    role.save()
    assert (
        Role.get(person=role.person, organisation=role.organisation).events[0].role
        is None
    )
    assert "role" not in collection.find_one({"_id": role.pk})["events"][0]


@pytest.mark.xfail
def test_count_save_queries(person, queries):
    queries.clear()
    assert not len(queries)
    person.first_name = "changed"
    person.save()
    # Person(last_name="pouet").save()
    # 1. Update version counter
    # 2. Do the upate
    # 3. Select roles
    # 4. Update the search index collection
    # 5. Find on contacts ??
    assert len(queries) == 8


def test_can_add_attachments_to_contacts(person):
    assert not person.attachments
    f = BytesIO(b"1234567")
    f.filename = "foo"
    f.content_type = "f/bar"
    attachment = person.add_attachment(f, size=199)
    person.save()
    assert person.attachments
    assert Person.get(person.pk).attachments
    assert Person.get(person.pk).attachments[attachment.id].read() == b"1234567"
    assert Person.get(person.pk).attachments[attachment.id].content_type == "f/bar"
    assert Person.get(person.pk).attachments[attachment.id].size == 199
