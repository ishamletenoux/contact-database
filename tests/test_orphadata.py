from pathlib import Path

from eurordis import orphadata
from eurordis.models import Disease


def test_import_orphadata():
    orphadata.ROOT = Path(__file__).parent / "data" / "orphadata"
    orphadata.import_orphadata()
    assert Disease.objects.count() == 0
    orphadata.import_orphadata(commit=True)
    assert Disease.objects.count() == 30
    root = Disease.objects.get(pk=98050)
    assert root.name == "Rare allergic disease"
    assert root.synonyms == ["Rare allergy"]
    assert root.depth == 0
    assert root.max_depth == 4
    assert not root.ancestors
    assert root.roots == [root]
    assert len(root.descendants) == 29
    leaf = Disease.objects.get(pk=99906)
    assert leaf.name == "Farmer's lung disease"
    assert leaf.max_depth == root.max_depth
    assert leaf.depth == leaf.max_depth
    assert not leaf.descendants
    assert len(leaf.ancestors) == leaf.depth
    assert leaf.roots == [root]
    node = Disease.objects.get(pk=98052)
    assert node.name == "Rare allergic respiratory disease"
    assert node.max_depth == root.max_depth
    assert node.depth == 1
    assert len(node.ancestors) == node.depth
    assert len(node.descendants) == 7
    assert node.synonyms == ["Rare respiratory allergy"]
    assert node.roots == [root]
