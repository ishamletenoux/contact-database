import os
from datetime import date

import pytest
from eurordis import app as eurordis_app
from eurordis import configure, helpers, jwt_token, session
from eurordis.bin import create_indexes
from eurordis.models import (
    Disease,
    Event,
    Group,
    Keyword,
    Organisation,
    Person,
    Resource,
    Role,
    Version,
)
from eurordis.search.back import compute_index_max_scores
from mongoengine.connection import get_db
from pymongo import monitoring
from roll.testing import Client as BaseClient


def pytest_configure(config):
    os.environ["EURORDIS_DBNAME"] = "test_eurordis"
    os.environ["EURORDIS_MAILCHIMP_APIKEY"] = "blah-us20"
    os.environ["MAILCHIMP_LIST_EURORDIS_MEMBERS"] = "foo"
    os.environ["EURORDIS_MAILCHIMP_LIST_ALL"] = ""
    configure()
    get_db().client.drop_database("test_eurordis")
    create_indexes()
    compute_index_max_scores()
    session.user_id.set("1")


def pytest_runtest_setup(item):
    assert get_db().name == "test_eurordis"
    for cls in [Person, Organisation, Disease, Event, Version, Group, Keyword, Role]:
        cls.objects.delete()
    get_db().get_collection("mongoengine.counters").drop()
    get_db().get_collection("counter").drop()
    get_db().get_collection("activity").drop()
    get_db().get_collection("search").drop()
    Resource.get_name.cache_clear()
    Person.get_name.cache_clear()


@pytest.fixture
def app():  # Requested by Roll testing utilities.
    return eurordis_app


class Client(BaseClient):
    def login(self, person, email=None):
        token, _ = jwt_token(person, email or person.email)
        self.default_headers["API-Key"] = token

    def logout(self):
        try:
            del self.default_headers["API-Key"]
        except KeyError:
            pass


@pytest.fixture
def client(app, event_loop):
    app.loop = event_loop
    app.loop.run_until_complete(app.startup())
    yield Client(app)
    app.loop.run_until_complete(app.shutdown())


@pytest.fixture
def aclient(client, person):
    client.login(person)
    yield client
    client.logout()


@pytest.fixture
def bot():
    from eurordis import config

    bot = Person(
        title="mr",
        first_name="Pol",
        last_name="Bot",
        email=config.BOT_EMAIL,
        created_at=helpers.utcnow(),
        modified_at=helpers.utcnow(),
    )
    bot.created_by = bot
    bot.modified_by = bot
    bot.save()
    return bot


@pytest.fixture
def person():
    person = Person(
        title="mr",
        first_name="Bill",
        last_name="Mollison",
        email="bill@somewhere.org",
        phone="+9876543210",
        languages=["en", "fr"],
        country="FR",
        cell_phone="+611111111",
        created_at=helpers.utcnow(),
        modified_at=helpers.utcnow(),
    )
    person.created_by = person
    person.modified_by = person
    person.save()
    return person


@pytest.fixture
def person2(person):
    return Person(
        title="mr",
        first_name="David",
        last_name="Holmgren",
        email="david@somewhere.org",
        phone="+9876543234",
        languages=["en", "de"],
        country="AU",
    ).save()


@pytest.fixture
def person3(person):
    return Person(
        title="mr",
        first_name="Patrick",
        last_name="Morel",
        phone="+9198457501",
        languages=["en", "fr"],
        country="FR",
    ).save()


@pytest.fixture
def staff(person, client):
    new = Person(
        title="mr",
        first_name="Staff",
        last_name="Staff",
        email="staff@eurordis.org",
        phone="+9876543234",
        languages=["fr", "de"],
        country="BE",
    ).save()
    from eurordis import config

    client.login(new, config.STAFF[0])
    return new


@pytest.fixture
def organisation(person):
    org = Organisation(
        name="Bill Org",
        email="contact@bill.org",
        country="BE",
        kind=60,
    )
    org.roles = [
        Role(
            name="CEO",
            is_primary=True,
            person=person.pk,
            organisation=org,
            email="bmollison@bill.org",
            phone="+1234567890",
        )
    ]
    org.save()
    person.reload()  # Reload modified_at.
    return org


@pytest.fixture
def role(person, organisation):
    return Role.get(person=person, organisation=organisation)


@pytest.fixture
def role2(person2, organisation):
    return Role(person=person2, organisation=organisation).save()


@pytest.fixture
def eurordis_org():
    from eurordis import config

    org = Organisation(
        name="Eurordis", filemaker_pk="so4146", email="eurordis@pouet.org", country="FR"
    ).save()
    config.EURORDIS_PK = org.pk
    return org


@pytest.fixture
def rdi_org():
    from eurordis import config

    org = Organisation(
        name="Rare Diseases International", filemaker_pk="so7746", country="FR"
    ).save()
    config.RDI_PK = org.pk
    return org


@pytest.fixture
def disease(person):
    return Disease(
        name="Congenital deficiency in alpha-fetoprotein",
        orpha_number="12345",
        groupings=[{"name": "bond"}, {"name": "eye"}],
        max_depth=8,
    ).save()


@pytest.fixture
def event():
    return Event(
        name="Royal Gala",
        kind="awards",
        start_date=date(2018, 11, 19),
        duration=8,
        location="Bruxelles",
    ).save()


@pytest.fixture
def group():
    return Group(name="CNA").save()


@pytest.fixture
def keyword():
    return Keyword(name="reviewed", for_person=True, for_organisation=True).save()


@pytest.fixture
def mock_http(monkeypatch):
    class Response:
        def __init__(self, status_code, content):
            self.status_code = status_code
            self.content = content
            self.ok = status_code == 200

        def json(self):
            return self.content

    calls = []
    stack = []

    def _(status_code=200, content=""):
        def request(*args, **kwargs):
            try:
                status_code, content = stack.pop(0)
            except IndexError:
                status_code, content = 200, ""
            calls.append({"args": args, "kwargs": kwargs})
            return Response(status_code, content)

        if not stack:  # First call
            monkeypatch.setattr("requests.request", request)
        stack.append((status_code, content))
        return calls

    return _


@pytest.fixture
def queries():
    return query_logger


class QueryLogger(monitoring.CommandListener):

    def __init__(self):
        self.queries = []
        self._verbose = False

    def started(self, event):
        self.append(event.command)

    def succeeded(self, event):
        pass

    def failed(self, event):
        pass

    def __len__(self):
        return self.queries.__len__()

    def __getitem__(self, item):
        return self.queries[item]

    def __str__(self):
        return str(self.queries)

    def __repr__(self):
        return repr(self.queries)

    def clear(self):
        self.queries = []

    def verbose(self):
        self._verbose = True

    def append(self, command):
        (action, collection), *_ = command.items()
        command = command.to_dict()
        if self._verbose:
            print(command)
        query = {
            "action": action,
            "collection": collection,
            "filter": command.get("filter") or command.get("query"),
            "documents": command.get("documents"),
            "update": command.get("updates") or command.get("update"),
        }
        self.queries.append(query)


query_logger = QueryLogger()

monitoring.register(query_logger)
