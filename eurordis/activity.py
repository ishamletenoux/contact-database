import csv
import io
from datetime import datetime
from urllib.parse import urlparse, parse_qs

from dateutil.parser import parse as parse_date
from mongoengine.connection import get_db

COLLECTION = "activity"


def log(by, location, **extra):
    at = extra.pop("at", None)
    at = parse_date(at) if at else datetime.now()
    collection = get_db()[COLLECTION]
    collection.insert_one({"by": by, "at": at, "location": location, **extra})


def get(limit=None):
    collection = get_db()[COLLECTION]
    qs = collection.find({}, {"_id": False}).sort("at", -1)
    if limit:
        qs = qs.limit(limit)
    return list(qs)


def as_csv():
    output = io.StringIO()
    writer = csv.DictWriter(
        output, fieldnames=["pk", "name", "at", "location"], delimiter=";"
    )
    writer.writeheader()
    for row in get():
        if not row.get("by"):
            continue
        writer.writerow(
            {
                "pk": row["by"]["pk"],
                "name": row["by"]["label"],
                "at": row["at"].isoformat(),
                "location": row["location"],
            }
        )
    output.seek(0)
    return output.read()


def search_as_csv():
    output = io.StringIO()
    writer = csv.DictWriter(
        output, fieldnames=["pk", "name", "at", "key", "value", "label"], delimiter=";"
    )
    writer.writeheader()
    for row in get():
        location = row["location"]
        if not row.get("by") or not location.startswith("/?"):
            continue
        parsed = urlparse(location)
        if not parsed.query:
            continue
        query = parse_qs(parsed.query)
        for key, blob in query.items():
            values = blob[0].split("||")
            for value in values:
                try:
                    pk, label = value.split("|")
                except ValueError:
                    pk = label = value
                writer.writerow(
                    {
                        "pk": row["by"]["pk"],
                        "name": row["by"]["label"],
                        "at": row["at"].isoformat(),
                        "key": key,
                        "value": pk,
                        "label": label
                    }
                )
    output.seek(0)
    return output.read()
