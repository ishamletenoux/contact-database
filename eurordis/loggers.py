import logging

logger = logging.getLogger("eurordis")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())
