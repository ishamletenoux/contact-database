import csv
import zipfile
from pathlib import Path
from urllib.request import urlretrieve

from datetime import datetime, timedelta
from lxml import etree
from minicli import cli
from progressist import ProgressBar

from . import config
from .models import Contact, Disease, Version
from .search.back import compute_index_max_scores

ROOT = config.DATA_ROOT / "orphadata"
REMOTE = "http://www.orphadata.org/data/RD-CODE/Packs/Orphanet_Nomenclature_Pack_EN.zip"

GROUP_OF_TYPES_MAP = {
    "Malformation syndrome": "disorder",
    "Morphological anomaly": "disorder",
    "Clinical syndrome": "disorder",
    "Histopathological subtype": "subtype",
    "Clinical subtype": "subtype",
    "Category": "group",
    "Disease": "disorder",
    "Etiological subtype": "subtype",
    "Particular clinical situation in a disease or syndrome": "disorder",
    "Biological anomaly": "disorder",
    "Clinical group": "group",
}

STATUS_MAP = {
    "Inactive": "inactive",
    "Inactive: Deprecated": "deprecated",
    "Inactive: Obsolete": "obsolete",
    "Inactive: Non rare disease in Europe": "non rare",
    "Active": "active",
}

DISORDER_TYPES_MAP = {
    "21394": "Disease",
    "21401": "Malformation syndrome",
    "21408": "Biological anomaly",
    "21415": "Morphological anomaly",
    "21422": "Clinical syndrome",
    "21429": "Particular clinical situation in a disease or syndrome",
    "21436": "Clinical group",
    "21443": "Etiological subtype",
    "21450": "Clinical subtype",
    "21457": "Histopathological subtype",
    "21464": "No type",
    "36561": "Category",
}

EXTRA_NOMENCLATURE = {
    602659: {"name": "Pediatric cancer", "disorder_type": "Category"},
    602663: {
        "name": "Pediatric cancer of hematopoietic and lymphoid tissues",
        "disorder_type": "Category",
    },
    602677: {"name": "Pediatric bone cancer", "disorder_type": "Category"},
    602679: {"name": "Pediatric soft tissue cancer", "disorder_type": "Category"},
    602683: {"name": "Pediatric renal cancer", "disorder_type": "Category"},
    602685: {"name": "Pediatric digestive cancer", "disorder_type": "Category"},
    602691: {"name": "Pediatric respiratory cancer", "disorder_type": "Category"},
    602693: {"name": "Pediatric nervous system cancer", "disorder_type": "Category"},
    602697: {"name": "Pediatric gynecological cancer", "disorder_type": "Category"},
    602699: {"name": "Pediatric eye cancer", "disorder_type": "Category"},
    602701: {
        "name": "Pediatric cancer of testis and paratestis",
        "disorder_type": "Category",
    },
    602703: {
        "name": "Pediatric cancer of endocrine glands",
        "disorder_type": "Category",
    },
    602711: {"name": "Rare virus associated cancer", "disorder_type": "Category"},
    602715: {"name": "Pediatric head and neck cancer", "disorder_type": "Category"},
    602661: {"name": "Pediatric germ cell cancer", "disorder_type": "Category"},
    602665: {"name": "Pediatric myeloid cancer", "disorder_type": "Category"},
    602669: {"name": "Pediatric lymphoid cancer", "disorder_type": "Category"},
    602681: {"name": "Pediatric soft tissue sarcoma", "disorder_type": "Category"},
    602687: {
        "name": "Pediatric hepatic and biliary tract cancer",
        "disorder_type": "Category",
    },
    602689: {"name": "Pediatric cancer of pancreas", "disorder_type": "Category"},
    602695: {
        "name": "Pediatric cancer of neuroepithelial tissue",
        "disorder_type": "Category",
    },
    602705: {"name": "Pediatric neuroendocrine cancer", "disorder_type": "Category"},
    602707: {"name": "Pediatric thyroid carcinoma", "disorder_type": "Category"},
    602709: {
        "name": "Pediatric adrenal/paraganglial cancer",
        "disorder_type": "Category",
    },
    602713: {"name": "Epstein-Barr Virus-related cancer", "disorder_type": "Category"},
    602667: {"name": "Pediatric acute myeloid leukemia", "disorder_type": "Category"},
    602671: {"name": "Pediatric lymphoma", "disorder_type": "Category"},
    602673: {
        "name": "Pediatric B-cell non-Hodgkin lymphoma",
        "disorder_type": "Category",
    },
    602675: {
        "name": "Pediatric T-cell non-Hodgkin lymphoma",
        "disorder_type": "Category",
    },
    641375: {"name": "B-lymphoblastic leukemia/lymphoma with t(17;19)", "disorder_type": "Category"},
    641372: {"name": "B-lymphoblastic leukemia/lymphoma with t(7;9)(q11.2;p13.2)", "disorder_type": "Category"}
}


def download(filename, force=False):
    ROOT.mkdir(exist_ok=True)
    path = ROOT / filename
    if force or not path.exists():
        url = f"http://www.orphadata.org/data/xml/{filename}"
        print(f"Downloading {url}")
        urlretrieve(url, path)


# To use in case import_orphadata removed groupings
# Ex: If import_orphadata is ran on 2023-07-25, run `eurordis fix-lost-groupings 2023-07-25` to backup
@cli
def fix_lost_groupings(date):
    print("Warning ! This command could take about 5 minutes...")
    start_date = datetime.strptime(date, "%Y-%m-%d")
    end_date = start_date + timedelta(days=1) - timedelta(seconds=1)
    versions = Version.objects.filter(at__gte=start_date, at__lte=end_date, action="update")
    bar = ProgressBar(prefix=f"Analyzing changes on {date}", total=versions.count())
    for version in bar.iter(versions):
        if (
            version.of.document_type == Disease
            and "groupings" in version.diff.keys()
            and all(key in version.diff["groupings"] for key in ["old", "new"])
            and len(version.diff["groupings"]["new"]) == 0
            and len(version.diff["groupings"]["old"]) > 0
        ):
            disease = version.of.fetch()
            previous_groupings = version.diff["groupings"]["old"]
            disease.groupings = previous_groupings
            disease.save()
    return date


@cli
def download_orphadata():
    ROOT.mkdir(exist_ok=True)
    main = ROOT / "orphadata.zip"
    urlretrieve(REMOTE, main)
    for path in ROOT.glob("*.xml"):
        path.unlink()
    with zipfile.ZipFile(main) as z:
        for path in z.namelist():
            if path.endswith(".xml"):
                print(path)
                dest = ROOT / Path(path).name
                dest.write_bytes(z.read(path))
    main.unlink()


@cli
def import_orphadata(commit=False, report=Path("/tmp/orphanet-report.csv")):
    """Import ORPHAnomenclature.

    Get data from http://www.orphadata.org/cgi-bin/ORPHAnomenclature.html.
    Unzip and put ORPHAnomenclature_en.xml in data/orphadata.
    Then unzip the classification folder and put all files in data/orphadata too.
    """
    print("Run orphadata import")

    compute_index_max_scores()

    existing = set(
        Disease.objects.filter(ancestors__ne=config.NON_RARE_ORPHA_NUMBER).scalar("pk")
    )
    used = set(d.pk for d in Contact.objects.distinct("diseases"))

    # 1. Load all classification trees.
    files = list(ROOT.glob("ORPHAclassification*"))
    bar = ProgressBar(prefix="Loading trees", total=len(files))
    trees = {}
    for file_ in bar.iter(files):
        id_ = file_.name.split("_", maxsplit=2)[1]
        trees[id_] = etree.parse(str(file_))

    # 2. Load all diseases.
    diseases = {}
    root = etree.parse(str(ROOT / "ORPHAnomenclature_en.xml"))
    disorders = root.xpath("//Disorder")
    bar = ProgressBar(prefix="Loading diseases", total=len(disorders))
    for disorder in bar.iter(disorders):
        name = disorder.find("Name").text
        disorder_type = disorder.find("DisorderType").attrib["id"]
        disorder_type = DISORDER_TYPES_MAP[disorder_type]
        group_of_type = GROUP_OF_TYPES_MAP[disorder_type]
        status = disorder.find("Totalstatus").text
        status = STATUS_MAP[status]
        number = int(disorder.find("OrphaCode").text)
        synonyms = [s.text for s in disorder.findall("SynonymList/Synonym")]
        diseases[number] = Disease(
            name=name,
            orpha_number=number,
            synonyms=synonyms,
            group_of_type=group_of_type,
            disorder_type=disorder_type,
            status=status,
        )
    # 2 bis. Load extra diseases.
    for number, data in EXTRA_NOMENCLATURE.items():
        diseases[number] = Disease(orpha_number=number, group_of_type="group", **data)

    # 3. Compute ancestors and descendants.
    for id_, xml in trees.items():
        root = xml.find("//ClassificationNode")
        bar = ProgressBar(
            prefix=f"Compute tree {id_}",
            total=len(xml.xpath("//Disorder")),
            template="{prefix} {animation} {percent} ({done}/{total}) ETA: {eta} {elapsed}",
        )
        walk(root, diseases, bar=bar)

    # Exclude diseases not in any classification.
    to_save = [d for d in diseases.values() if d.ancestors or d.descendants]

    # 4. Save relations.
    if commit:
        bar = ProgressBar(prefix="Saving…", total=len(to_save))
        for disease in bar.iter(to_save):
            disease.save()
    else:
        print("Dry run mode, nothing has been saved!")

    new = set(d.pk for d in to_save) - existing
    old = existing - set(d.pk for d in to_save) - {config.NON_RARE_ORPHA_NUMBER}
    with Path(report).open("w") as f:
        writer = csv.writer(f, delimiter=";")
        changed = new | old
        writer.writerow(
            [
                "Name",
                "Status",
                "Group of Type",
                "Disorder Type",
                "Used by",
                "Grouping",
                "Ancestors",
            ]
        )
        for pk in changed:
            try:
                # May not exist if we are not in commit mode.
                disease = Disease.get(pk)
            except Disease.DoesNotExist:
                disease = diseases[pk]
            status = "added" if pk in new else "removed"
            by = ""
            if pk in used:
                by = ",".join(
                    Contact.objects(diseases=pk).scalar("pk").no_dereference()
                )
            ancestors = [str(d) for d in disease.ancestors]
            writer.writerow(
                [
                    str(disease),
                    status,
                    disease.group_of_type,
                    disease.disorder_type,
                    by,
                    "|".join(e.name for e in disease.groupings),
                    "|".join(ancestors),
                ]
            )
    print(f"Report saved to {report}")
    to_remove = old - used
    for pk in to_remove:
        disease = Disease.get(pk)
        if commit:
            if Disease.objects(ancestors=pk):
                print(f"Cannot delete {disease}: still referenced as ancestor")
                continue
            disease.delete()


def walk(node, diseases, bar, parent=None, root=None):
    orphanumber = node.find("OrphaCode")
    if orphanumber is None:
        orphanumber = node.find("Disorder/OrphaCode")
    if orphanumber is None:
        orphanumber = node.find("ClassificationNodeChildList/ClassificationNode/Disorder/OrphaCode")
    number = int(orphanumber.text)
    try:
        disease = diseases[number]
    except KeyError:
        print(f"Missing disease {number} in nomenclature")
        return
    if not root:
        root = disease
    if root not in disease.roots:
        disease.roots.append(root)
    if parent:
        depth = parent.depth + 1
        disease.depth = max(disease.depth, depth)
        disease.max_depth = max(disease.max_depth, depth)
        parent.max_depth = max(parent.max_depth, depth)
        parent.descendants.append(disease)
        disease.ancestors.append(parent)
        ancestors = list(set(disease.ancestors + parent.ancestors))
        disease.ancestors = ancestors
        for ancestor in parent.ancestors:
            ancestor.max_depth = max(ancestor.max_depth, depth)
            if disease not in ancestor.descendants:
                ancestor.descendants.append(disease)
    children = node.findall("ClassificationNodeChildList/ClassificationNode")
    for node in children:
        walk(node, diseases, parent=disease, bar=bar, root=root)
    bar.update()
