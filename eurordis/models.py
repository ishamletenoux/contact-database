import re
from collections import Counter
from datetime import date, datetime
from functools import lru_cache
from copy import deepcopy

import pymongo
from bson.errors import InvalidId
from bson.objectid import ObjectId
from mongoengine import Document, EmbeddedDocument, QuerySet, fields
from mongoengine.base.metaclasses import TopLevelDocumentMetaclass
from mongoengine.errors import DoesNotExist, ValidationError

from . import config, constants, session
from .diff import Diff
from .fields import (
    BooleanField,
    DateField,
    DateTimeField,
    EmailField,
    EmbeddedDocumentListField,
    ReferenceField,
    SequenceField,
    SimpleDictField,
    StringField,
    FileField,
    GridFSProxy,
)
from .helpers import utcnow, as_date
from .search.back import Searchable, SearchableQueryset, deindex, index, make_ft


class Serializable:
    def serialize(self, fields):
        out = {}
        for name in fields:
            field = getattr(self.__class__, name, None)
            if not field:
                raise ValueError(f"Unknown field `{name}` for `{self}`")
            value = getattr(self, name)
            if value is not None:
                if isinstance(value, Document):
                    value = value.as_relation()
                elif isinstance(value, ObjectId):
                    value = str(value)
                elif isinstance(value, (datetime, date)):
                    value = value.isoformat()
                elif name == "attachments":
                    value = [v.as_relation() for k, v in value.items()]
                elif isinstance(value, (list, QuerySet)):
                    if hasattr(value, "all"):
                        value = value.all()
                    value = [
                        i.as_relation() if hasattr(i, "as_relation") else i
                        for i in value
                    ]
            out[name] = value
        return out

    def as_resource(self):
        return self.serialize(self.resource_fields)

    def as_relation(self, *extra):
        return self.serialize(self.relation_fields + list(extra))


class HttpLayer:
    @property
    def uri(self):
        return f"/{self.resource}/{self.pk}"

    @property
    def resource(self):
        return self.resource_name()

    @property
    def label(self):
        return str(self)

    @classmethod
    def resource_name(cls):
        return cls.__name__.lower()

    def ensure_editable_fields(self, data):
        for key in data:
            if key not in self.editable_fields:
                raise ValueError(f"{key} is not an editable field")


class MetaResource(TopLevelDocumentMetaclass):
    def __new__(cls, name, bases, attrs):
        new = super().__new__(cls, name, bases, attrs)
        new.editable_fields = [
            n for n, f in new._fields.items() if getattr(f, "editable", False)
        ]
        return new


class Resource(Serializable, HttpLayer, Document, metaclass=MetaResource):
    meta = {"abstract": True, "strict": False, "auto_create_index": False}
    resource_fields = []
    virtual_fields = []

    def __repr__(self):
        return f"<{self.__class__.__name__} {str(self)} - {self.pk}>"

    def __str__(self):
        return str(self.pk)

    @classmethod
    def get(cls, id):
        try:
            return cls.objects.get(pk=id)
        except InvalidId:
            raise cls.DoesNotExist

    def replace(self, data):
        pk = self.pk
        for key in list(self._data.keys()):
            if key in self.editable_fields:
                del self._data[key]
        # Help mongoengine compute the changes diff (see BaseDocument._delta).
        self._changed_fields = list(self._fields.keys())
        self.pk = pk
        self.simple_update(data)
        return self

    def simple_update(self, data):
        # MongoEngine update() fails because we use SequenceField as pks.
        for key, value in data.items():
            if key in self._fields:
                field = self._fields[key]
                setattr(self, key, field.to_python(value))
            elif key in self.virtual_fields:
                loader = self.virtual_fields[key]
                setattr(self, key, loader(value))
        self.save()
        return self

    @classmethod
    def from_document(cls, son):
        assert isinstance(son, dict), "Invalid document"
        for key in son.keys():
            if key in cls.virtual_fields:
                continue
            if key not in cls._fields:
                raise ValueError(f"Unrecognized field name '{key}'")
            if key not in cls.editable_fields:
                raise ValidationError(
                    errors={key: ValidationError("not an editable field")}
                )
        # Remove the extraneous key before passing the values to MongoEngine, otherwise
        # it will complain, as we are not using DynamicDocument.
        virtual_fields = {k: son.pop(k) for k in cls.virtual_fields if k in son}
        inst = cls._from_son(son, created=True)
        for key, value in virtual_fields.items():
            loader = cls.virtual_fields[key]
            setattr(inst, key, loader(value))
        return inst

    @classmethod
    @lru_cache()
    def get_name(cls, pk):
        try:
            return cls._get_collection().find_one({"_id": pk}, {"name": 1})["name"]
        except TypeError:  # Query returns None if object does not exist.
            return str(pk)


class ContactQuerySet(SearchableQueryset):
    def from_disease(self, pattern):
        diseases = Disease.extrapolate_search_pattern(pattern)
        return self.filter(diseases__in=diseases)

    def filter_resource(self, resource):
        for model in self._document.__subclasses__():
            if resource == model.__name__.lower():
                return self.filter(_cls=model._class_name)
        else:
            raise ValueError(f"Unknown resource name: `{resource}`")


class Fee(Serializable, EmbeddedDocument):
    relation_fields = resource_fields = [
        "year",
        "amount",
        "payment_date",
        "exempted",
        "receipt_sent",
    ]
    YEARS = range(1997, utcnow().year + 2)
    year = fields.IntField(choices=YEARS, required=True)
    amount = fields.FloatField(default=0)
    payment_date = DateField()
    exempted = fields.BooleanField(default=False)
    receipt_sent = fields.BooleanField(default=False)

    def __repr__(self):
        if self.exempted:
            payment = "exempted"
        else:
            payment = as_date(self.payment_date)
        return f"<{self.__class__.__name__}: {self.year} ({payment} - {self.amount}€)>"


class Membership(Serializable, EmbeddedDocument):
    relation_fields = resource_fields = [
        "organisation",
        "status",
        "start_date",
        "end_date",
        "fees",
    ]
    organisation = ReferenceField("Organisation", required=True)
    status = StringField(choices=constants.MEMBERSHIP_STATUSES, required=False)
    start_date = DateField(required=False)
    end_date = DateField(required=False)
    fees = fields.EmbeddedDocumentListField(Fee)

    def __repr__(self):
        if self.status:
            end_date = self.end_date or "now"
            start_date = self.start_date
            return f"<{self.__class__.__name__}: {self.organisation.name} - {start_date} -> {end_date} ({self.status})>"
        else:
            return f"<{self.__class__.__name__}: {self.organisation.name}>"

    def is_active_to(self, to):
        return not self.end_date and self.status != "w" and self.organisation == to

    @property
    def last_fee(self):
        if self.fees:
            return sorted(self.fees, key=lambda x: x.payment_date, reverse=True)[0]

    def fee_for(self, year):
        for fee in self.fees:
            if fee.year == year:
                return fee


class GroupBelonging(Serializable, EmbeddedDocument):
    ROLES = constants.GROUP_ROLES
    relation_fields = resource_fields = ["group", "name", "role", "is_active"]
    is_active = fields.BooleanField(default=True)
    group = ReferenceField("Group", required=True)
    role = StringField(required=False, choices=ROLES)

    @property
    def name(self):
        return str(self.group)

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self.name} ({self.role})>"


class RoleToEvent(Serializable, EmbeddedDocument):
    relation_fields = resource_fields = ["event", "role"]
    event = ReferenceField("Event")
    role = StringField(required=False)

    def clean(self):
        self.role = self.role or None


class ByAt:
    resource_fields = ["created_at", "modified_at", "created_by", "modified_by"]

    created_at = DateTimeField(default=utcnow, required=True)
    modified_at = DateTimeField(required=True)
    created_by = ReferenceField("Person", required=True)
    modified_by = ReferenceField("Person", required=True)
    filemaker_pk = StringField(unique=True, required=False, null=False)
    version = fields.IntField(default=0)

    def save(self, *args, **kwargs):
        ghost = kwargs.pop("ghost", session.ghost.get(False))
        if not ghost:
            self.modified_at = utcnow()
            user_id = Contact.modified_by.to_python(session.user_id.get())
            self.modified_by = user_id
            if not self.created_by:
                self.created_by = user_id
        self.version = Version.incr_for(self)
        inst = super().save(*args, **kwargs)
        Version.backup(self, ghost=ghost)
        return inst

    @property
    def versions(self):
        return Version.objects(of=self).order_by("-version")

    @property
    def last_version(self):
        return Version.objects(of=self).order_by("-version").first()

    @property
    def history(self):
        qs = self.versions.filter(ghost__ne=True).limit(10)
        if self.filemaker_pk:
            qs = qs.filter(version__gt=1)
        return qs

    def backup(self):
        return self._get_collection().find_one({"_id": self.pk})


def load_roles(roles):
    out = []
    for what in roles:
        if isinstance(what, Role):
            out.append(what)
        if isinstance(what, dict):
            out.append(Role(**what))
    return out


class Contact(ByAt, Searchable, Resource):
    indexes = [
        (
            [("_cls", pymongo.ASCENDING), ("diseases", pymongo.ASCENDING)],
            {"name": "diseases"},
        ),
        (
            [("email", pymongo.ASCENDING)],
            {"name": "email", "unique": True, "sparse": True},
        ),
        (
            [("filemaker_pk", pymongo.ASCENDING)],
            {"name": "filemaker_pk", "unique": True, "sparse": True},
        ),
        (
            [
                ("_cls", pymongo.TEXT),
                ("first_name", pymongo.TEXT),
                ("last_name", pymongo.TEXT),
                ("name", pymongo.TEXT),
            ],
            {"name": "text_index"},
        ),
    ]
    label_fields = Searchable.label_fields + [("comment", 0.4)]
    meta = {"allow_inheritance": True, "queryset_class": ContactQuerySet}
    virtual_fields = {"roles": load_roles}
    roles_to_save = []
    id = SequenceField(required=True, primary_key=True)
    diseases = fields.ListField(ReferenceField("Disease"), editable=True)
    email = EmailField(null=False, required=False, editable=True)
    phone = StringField(required=False, editable=True)
    cell_phone = StringField(required=False, editable=True)
    comment = StringField(required=False, editable=True)
    country = StringField(
        required=False,
        min_length=2,
        max_length=2,
        choices=constants.COUNTRIES,
        editable=True,
    )
    # Denormalize Disease grouping, to make search easier and more consitent with other
    # fields.
    groupings = fields.ListField(StringField(choices=constants.GROUPING))
    attachments = fields.MapField(FileField())

    @property
    def roles(self):
        return self.get_roles()

    # TODO: replace me with a diff with DB at save time.
    @roles.setter
    def roles(self, values: list):
        """
        Detect roles changes for post-saving only those changed, updated and removed.
        """
        self.roles_to_save = []
        # Role is a relation, but we get only the destination from the API, make sure
        # to have "self" reference in it, too.
        for value in values:
            setattr(value, self.__class__.__name__.lower(), self)
        old = {(r.person, r.organisation): r for r in self.roles}
        new = {(r.person, r.organisation) for r in values}
        for pair in set(old.keys()) - new:
            self.roles_to_save.append({"role": old[pair], "action": "delete"})
        for role in values:
            match = None
            action = None
            pair = (role.person, role.organisation)
            if pair in old:
                match = old[pair]
            if not match or not self.pk:
                action = "create"
            elif match._data != role._data:
                action = "update"
            if action:
                self.roles_to_save.append({"role": role, "action": action})

    @property
    def groups(self):
        # [word for sentence in text for word in sentence]
        return [g for role in self.roles for g in role.groups]

    @property
    def country_label(self):
        return constants.COUNTRIES.get(self.country)

    def add_attachment(self, file, **props):
        proxy = GridFSProxy()
        props["filename"] = file.filename
        props["content_type"] = file.content_type
        proxy.put(file, **props)
        self.attachments[proxy.id] = proxy
        return proxy

    def clean(self):
        self.email = self.email or None
        self.keywords = list(set(self.keywords or []))
        try:
            for keyword in self.keywords:
                keyword.pk  # A DBRef? so not a valid dereferenced keyword.
        except AttributeError:
            raise ValueError(f"Invalid keyword {keyword}")
        self.diseases = list(set(self.diseases or []))

    def delete(self, *args, **kwargs):
        if (
            Event.objects(
                __raw__={"$or": [{"created_by": self.pk}, {"modified_by": self.pk}]}
            )
            or Contact.objects(
                __raw__={
                    "$or": [
                        {"created_by": self.pk},
                        {"modified_by": self.pk},
                        {"membership.organisation": self.pk},
                    ]
                }
            )
            or Version.objects(by=self)
        ):
            raise ValueError(
                "Either an organisation that has members or a contact who has been a "
                "user of the database."
            )
        user_id = Contact.modified_by.to_python(session.user_id.get())
        self.modified_by = user_id
        for role in self.roles:
            role.delete(related=role.related_to(self))
        super().delete(*args, **kwargs)
        Version.mark_deleted(self)

    def save(self, *args, **kwargs):
        diseases = self.diseases or []
        self.groupings = list(set(rel.name for d in diseases for rel in d.groupings))
        # Backup twice only at creation time
        if self._created and self.roles_to_save:
            # Save instance before saving m2m. This will create an extra call to backup,
            # hence create an extra version.
            super().save(*args, **kwargs)
        self.save_roles()
        return super().save(*args, **kwargs)

    def save_roles(self):
        for save_role in self.roles_to_save:
            action = save_role["action"]
            role = save_role["role"]
            if action in ("create", "update"):
                try:
                    instance = Role.get(
                        person=role.person, organisation=role.organisation
                    )
                except Role.DoesNotExist:
                    pass
                else:
                    role.pk = instance.pk
                role.save(related=role.related_to(self))
            if action == "delete":
                role.delete(related=role.related_to(self))
        # Do not save roles again if we call save twice on same instance.
        self.roles_to_save = []

    def index(self, cascade):
        super().index(cascade)
        if cascade:
            for role in self.roles:
                role.index(cascade=False)

    def backup(self):
        raw = super().backup()
        resource = self.resource
        # TODO use self.roles here ?
        roles = Role._get_collection().find(
            {resource: self.pk}, {"_id": False, resource: False}
        )
        raw["roles"] = list(roles)
        return raw

    def inherited_index(self, doc):
        """Combine values from each role and organisation
        to be able to exclude and intersect them.
        """
        doc["inherited_kind"] = [doc["kind"]] if doc.get("kind") else []
        doc["inherited_groupings"] = list(doc.get("groupings", []))
        doc["inherited_diseases"] = list(doc.get("diseases", []))
        doc["inherited_membership"] = list(doc.get("membership", []))

    def as_index(self):
        doc = super().as_index()
        self.inherited_index(doc)
        return doc


class Person(Contact):
    relation_fields = [
        "pk",
        "first_name",
        "last_name",
        "email",
        "uri",
        "resource",
        "label",
        "country",
    ]
    resource_fields = (
        relation_fields
        + ByAt.resource_fields
        + [
            "title",
            "phone",
            "cell_phone",
            "languages",
            "groupings",
            "diseases",
            "roles",
            "keywords",
            "comment",
            "filemaker_pk",
            "scopes",
            "attachments",
        ]
    )
    label_fields = Contact.label_fields + [("first_name", 0.5), ("last_name", 1)]

    title = StringField(
        required=False, choices=list(constants.TITLES.keys()), editable=True
    )
    first_name = StringField(required=False, editable=True)
    last_name = StringField(required=True, editable=True)
    languages = fields.ListField(
        StringField(choices=constants.LANGUAGES), required=False, editable=True
    )
    keywords = fields.ListField(
        ReferenceField("Keyword"), required=False, editable=True
    )
    filemaker_username = StringField(unique=True, required=False, null=False)

    def __str__(self):
        if not self.first_name:
            return self.last_name
        return f"{self.first_name} {self.last_name}"

    @property
    def name(self):
        return str(self)

    def get_roles(self):
        roles = Role.objects.filter(person=self.pk).select_related(1)
        # TODO use Mongo internal sort instead ?
        return sorted(roles, key=lambda r: (not r.is_default, not r.is_primary, r.organisation.name))

    @property
    def default_role(self):
        try:
            return Role.objects.filter(person=self.pk).filter(is_default=True).get()
        except Role.DoesNotExist:
            if len(self.roles) == 1:
                return self.roles[0]
        except Role.MultipleObjectsReturned:
            return Role.objects.filter(person=self.pk).filter(is_default=True)[0]
        return None

    @property
    def alumni(self):
        # Slow if events are not yet loaded.
        return "training" in (r.event.kind for r in self.events)

    @property
    def scopes(self):
        return getattr(self, "_scopes", None) or []

    def compute_scopes(self, email):
        return ["delete", "import", "admin"] if email in config.STAFF else []

    def clean(self):
        super().clean()
        self.title = self.title.lower() if self.title else None
        try:
            for keyword in self.keywords:
                assert keyword.for_person is True
        except AssertionError:
            raise ValueError(f"Invalid keyword for Person: `{keyword}`")

    @classmethod
    def from_email(cls, email):
        i_email = re.compile(f"^{email}$", re.IGNORECASE)
        try:
            return Person.objects.get(email=i_email)
        except Person.DoesNotExist:
            try:
                role = Role.objects.get(email=i_email)
            except (Role.DoesNotExist, Role.MultipleObjectsReturned):
                pass
            else:
                return role.person

    @classmethod
    @lru_cache()
    def get_name(cls, pk):
        data = cls._get_collection().find_one(
            {"_id": pk}, {"first_name": True, "last_name": True}
        )
        if not data:  # Query returns None if object does not exist.
            return str(pk)
        return " ".join([data.get("first_name", ""), data.get("last_name")])

    @property
    def _deindex_filter(self):
        return {"_id.person": self.pk}

    def inherited_index(self, doc):
        """Combine values from each role and organisation
        to be able to exclude and intersect them.
        """
        super().inherited_index(doc)
        doc["inherited_groups"] = []
        doc["inherited_events"] = []
        for role in self.roles:
            role_doc = role.to_mongo()
            doc["inherited_groups"].extend(role_doc["groups"])
            doc["inherited_events"].extend(role_doc["events"])
            organisation_doc = role.organisation.as_index()
            kind = organisation_doc.get("kind")
            if kind:
                doc["inherited_kind"].append(kind)
            doc["inherited_groupings"].extend(organisation_doc["groupings"])
            doc["inherited_diseases"].extend(organisation_doc["diseases"])
            doc["inherited_membership"].extend(organisation_doc["membership"])
        # Make values unique, but keep a list to make mongoengine happy
        doc["inherited_kind"] = list(set(doc["inherited_kind"]))
        doc["inherited_groupings"] = list(set(doc["inherited_groupings"]))
        doc["inherited_diseases"] = list(set(doc["inherited_diseases"]))
        # doc["inherited_groups"] = list(set(doc["inherited_groups"]))

    def get_email(self):
        # emails list with different priorities
        # 1: person's email; 2: default role's email; 3: primary roles' emails; 4: roles' emails
        emails = []
        if self.email:
            emails.append((self.email, 1))
        for role in self.roles:
            priority = 4
            if role.is_default and role.email:
                priority = 2
            elif role.is_primary and role.email:
                priority = 3
            if role.email:
                emails.append((role.email, priority))     
        emails.sort(key=lambda x: x[1])
        email = None
        if len(emails) > 0:
            email = emails[0][0]
        return email

class Organisation(Contact):
    relation_fields = ["pk", "name", "uri", "label", "resource", "country", "kind"]
    resource_fields = (
        relation_fields
        + ByAt.resource_fields
        + [
            "email",
            "phone",
            "cell_phone",
            "acronym",
            "address",
            "postcode",
            "city",
            "website",
            "groupings",
            "roles",
            "diseases",
            "keywords",
            "membership",
            "comment",
            "filemaker_pk",
            "adherents",
            "budget",
            "creation_year",
            "board",
            "paid_staff",
            "pharma_funding",
            "attachments",
        ]
    )
    label_fields = Contact.label_fields + [("acronym", 0.9)]
    name = StringField(editable=True, required=True)
    acronym = StringField(required=False, editable=True)
    address = StringField(required=False, editable=True)
    postcode = StringField(required=False, editable=True)
    city = StringField(required=False, editable=True)
    website = fields.URLField(required=False, editable=True)
    keywords = fields.ListField(
        ReferenceField("Keyword"), required=False, editable=True
    )
    kind = fields.IntField(
        required=False, choices=constants.ORGANISATION_KIND, editable=True
    )
    membership = fields.EmbeddedDocumentListField(Membership, editable=True)
    adherents = fields.IntField(editable=True)
    creation_year = fields.IntField(
        choices=range(1900, utcnow().year + 1), editable=True
    )
    budget = fields.IntField(editable=True)
    board = fields.StringField(editable=True)
    paid_staff = fields.IntField(editable=True)
    pharma_funding = fields.IntField(editable=True)

    def __str__(self):
        return self.name

    def get_roles(self):
        roles = Role.objects.filter(organisation=self.pk).select_related(1)
        # TODO use Mongo internal sort instead ?
        return sorted(roles, key=lambda r: (not r.is_primary, r.person.last_name))

    def organisation_membership(self, org_obj):
        for membership in self.membership:
            if membership.is_active_to(org_obj):
                return membership

    def organisation_membership_status(self, org_obj):
        membership = self.organisation_membership(org_obj)
        if membership:
            return constants.MEMBERSHIP_STATUSES.get(membership.status, None)

    @property
    def unarchived_roles(self):
        return [role for role in self.roles if not role.is_archived]

    def save(self, *args, **kwargs):
        if self.membership:
            for org in self.membership:
                if "fees" in org and org["fees"]:
                    org["fees"] = sorted(
                        org["fees"], key=lambda x: x["year"], reverse=True
                    )
        return super().save(*args, **kwargs)

    def clean(self):
        super().clean()
        membership = [
            m.organisation
            for m in self.membership or []
            if not m.end_date
            if m.organisation
        ]
        duplicate = [org.name for org, c in Counter(membership).items() if c > 1]
        if duplicate:
            raise ValidationError(f"Duplicate membership: {membership}")
        try:
            for keyword in self.keywords:
                assert keyword.for_organisation is True
        except AssertionError:
            raise ValueError(f"Invalid keyword for Organisation: `{keyword}`")

    @classmethod
    def get_organisation(cls, org_str):
        try:
            if org_str == config.EURORDIS_STR:
                return cls.objects.get(pk=config.EURORDIS_PK)
            elif org_str == config.RDI_STR:
                return cls.objects.get(pk=config.RDI_PK)
            else:
                return
        except cls.DoesNotExist:
            pass

    @classmethod
    def organisation_members(cls, org_obj, status=["f", "a"]):
        return cls.objects.filter(
            __raw__={
                "membership": {
                    "$elemMatch": {
                        "organisation": str(org_obj.pk),
                        "end_date": None,
                        "status": {"$in": status},
                    }
                }
            }
        ).order_by("country", "name")

    @classmethod
    def organisation_full_members_without_voting(cls, org_obj):
        raw = cls.objects.aggregate(
            [
                {
                    "$match": {
                        "_cls": "Contact.Organisation",
                        "membership": {
                            "$elemMatch": {
                                "organisation": str(org_obj.pk),
                                "end_date": None,
                                "status": "f",
                            }
                        },
                    }
                },
                {
                    "$lookup": {
                        "from": Role._get_collection_name(),
                        "localField": "_id",
                        "foreignField": "organisation",
                        "as": "roles",
                    }
                },
                {"$match": {"roles.is_voting": {"$ne": True}}},
                {"$sort": {"country": 1, "name": 1}},
            ]
        )
        ids = [o["_id"] for o in raw]
        return cls.objects(pk__in=ids)

    @property
    def _deindex_filter(self):
        return {"_id.organisation": self.pk}

    def index(self, cascade):
        super().index(cascade)
        if cascade:
            for role in self.roles:
                role.person.index(cascade=False)

    @property
    def primary_roles(self):
        return [r for r in self.roles if r.is_primary]


class Role(Resource):
    resource_fields = relation_fields = [
        "name",
        "is_primary",
        "is_default",
        "is_voting",
        "is_archived",
        "person",
        "organisation",
        "phone",
        "email",
        "groups",
        "events",
    ]
    indexes = [
        (
            [("person", pymongo.ASCENDING), ("organisation", pymongo.ASCENDING)],
            {"unique": True, "name": "person_organisation"},
        ),
        (
            [("person", pymongo.ASCENDING), ("is_default", pymongo.ASCENDING)],
            {
                "unique": True,
                "name": "is_default",
                "partialFilterExpression": {"is_default": True},
            },
        ),
        (
            [("organisation", pymongo.ASCENDING), ("is_voting", pymongo.ASCENDING)],
            {
                "unique": True,
                "name": "is_voting",
                "partialFilterExpression": {"is_voting": True},
            },
        ),
    ]
    label_fields = [("name", 0.7)]

    person = ReferenceField(Person, required=True)
    organisation = ReferenceField(Organisation, required=True)
    name = StringField(required=False, editable=True, null=True)
    email = EmailField(required=False, editable=True, null=True)
    phone = StringField(required=False, editable=True, null=True)
    is_primary = BooleanField(default=False, editable=True)
    is_default = BooleanField(default=False, editable=True)
    is_voting = BooleanField(default=False, editable=True)
    is_archived = BooleanField(default=False, editable=True)
    groups = fields.EmbeddedDocumentListField(GroupBelonging, editable=True)
    events = fields.EmbeddedDocumentListField(RoleToEvent, editable=True)

    @classmethod
    def get(cls, organisation, person):
        return Role.objects.get(organisation=organisation, person=person)

    def __str__(self):
        return f"{self.person} - {self.name} - {self.organisation}"

    def clean(self):
        try:
            groups = {g.group: g for g in self.groups or []}  # Unique
        except DoesNotExist as err:
            raise ValueError(err)
        self.groups = list(groups.values())
        events = {g.event.pk: g for g in self.events or []}  # Unique
        self.events = list(events.values())

    def as_person_index(self):
        doc = self.person.as_index()
        organisation_doc = self.organisation.as_index()
        role_doc = self.to_mongo()
        doc["_id"] = {
            # Order is important.
            "person": self.person.pk,
            "organisation": self.organisation.pk,
        }
        doc["diseases"].extend(organisation_doc.get("diseases") or [])
        doc["groupings"].extend(organisation_doc["groupings"])
        doc["groups"] = role_doc.get("groups")
        doc["events"] = role_doc.get("events")
        doc["membership"] = organisation_doc.get("membership") or []
        doc["kind"] = organisation_doc.get("kind")
        doc["ft"] += make_ft(self)
        self.person.inherited_index(doc)
        email = []
        if self.person.email:
            email.append(self.person.email)
        if self.organisation.email:
            email.append(self.organisation.email)
        if self.email:
            email.append(self.email)
        doc["email"] = email
        doc["role"] = {
            "organisation": self.organisation.as_relation(),
            "email": self.email,
            "is_primary": self.is_primary,
            "phone": self.phone,
            "name": self.name,
        }
        # Make that, when both match, contact is selected instead of contact role.
        doc["priority"] = 2
        return doc

    def as_organisation_index(self):
        doc = self.organisation.as_index()
        role_doc = self.to_mongo()
        doc["_id"] = {
            # Order is important.
            "organisation": self.organisation.pk,
            "person": self.person.pk,
        }
        doc["groups"] = role_doc.get("groups")
        email = []
        if self.organisation.email:
            email.append(self.organisation.email)
        if self.email:
            email.append(self.email)
        doc["email"] = email
        doc["role"] = {
            "person": self.person.as_relation(),
            "email": self.email,
            "is_primary": self.is_primary,
            "phone": self.phone,
        }
        return doc

    def index(self, cascade):
        index(self.as_person_index())
        index(self.as_organisation_index())

    def deindex(self):
        deindex(
            {"_id": {"person": self.person.pk, "organisation": self.organisation.pk}}
        )
        deindex(
            {"_id": {"organisation": self.organisation.pk, "person": self.person.pk}}
        )

    def related_to(self, other):
        if isinstance(other, Person):
            return self.organisation
        return self.person

    def save(self, *args, **kwargs):
        related = kwargs.pop("related", None)
        ghost = kwargs.pop("ghost", None)
        inst = super().save(*args, **kwargs)
        if related:
            related.save(ghost=ghost, cascade=False)
        else:
            self.person.save(ghost=ghost, cascade=False)
            self.organisation.save(ghost=ghost, cascade=False)
        self.index(cascade=False)
        return inst

    def delete(self, *args, **kwargs):
        related = kwargs.pop("related", None)
        super().delete(*args, **kwargs)
        if related:
            related.save()
        else:
            self.person.save()
            self.organisation.save()
        self.deindex()

    def add_group(self, **kwargs):
        self.groups.append(GroupBelonging(**kwargs))
        return self

    def get_email(self):
        return self.email or self.person.email or self.organisation.email

    def attended(self, event):
        relations = [r for r in self.events if r.event == event]
        return relations and relations[0] or None

    @classmethod
    def ePAG_representatives(cls):
        groups_in = list(Group.objects(kind="epag").scalar("pk").no_dereference())
        return cls.objects.filter(groups__is_active=True, groups__role="Patient Representative", groups__group__in=groups_in)


class Grouping(Serializable, EmbeddedDocument):
    relation_fields = resource_fields = ["name", "validated"]
    name = StringField(required=True, choices=constants.GROUPING)
    validated = fields.BooleanField(default=False)

    def __repr__(self):
        return f"<Grouping {self}>"

    def __str__(self):
        return constants.GROUPING[self.name]


class Disease(ByAt, Searchable, Resource):
    indexes = []
    meta = {"queryset_class": SearchableQueryset}
    relation_fields = [
        "pk",
        "orpha_number",
        "name",
        "label",
        "resource",
        "depth",
        "max_depth",
        "group_of_type",
    ]
    resource_fields = relation_fields + [
        "synonyms",
        "groupings",
        "roots",
        "disorder_type",
        "status",
    ]
    label_fields = Searchable.label_fields + [("synonyms", 0.9)]
    orpha_number = fields.IntField(primary_key=True)
    name = StringField(required=True)
    synonyms = fields.ListField(StringField(), required=False)
    roots = fields.ListField(ReferenceField("Disease"), required=False)
    ancestors = fields.ListField(ReferenceField("Disease"), required=False)
    descendants = fields.ListField(ReferenceField("Disease"), required=False)
    groupings = EmbeddedDocumentListField(Grouping, editable=True)
    depth = fields.IntField(default=0)
    max_depth = fields.IntField(default=0)
    group_of_type = StringField(
        required=True,
        choices=constants.DISEASE_GROUP_OF_TYPES,
        default="disorder",
    )
    disorder_type = StringField(required=True, default="Disease")
    status = StringField(required=True, default="active")

    def __str__(self):
        return f"{self.name} ({self.orpha_number})"

    def __repr__(self):
        return f"<Disease {self.name} ({self.orpha_number})>"

    @property
    def label(self):
        return self.name

    @classmethod
    def search(cls, q, limit=3):
        if q.isdigit():
            return cls.objects.filter(orpha_number=q).limit(limit)
        return cls.objects.search(q, limit=limit)

    @staticmethod
    def extrapolate_search_pattern(pattern):
        diseases = []
        if pattern.startswith(("<", ">")):
            try:
                pk, ancestors, descendants = (
                    Disease.objects.filter(pk=int(pattern[1:]))
                    .scalar("pk", "ancestors", "descendants")
                    .no_dereference()
                    .get()
                )
            except TypeError:
                raise ValueError(f"Invalid disease id {pattern}")
            diseases.append(pk)
            related = ancestors if pattern.startswith("<") else descendants
            diseases.extend(d.id for d in related)
        else:
            diseases = [int(pattern)]
        return diseases

    def delete(self, *args, **kwargs):
        if Contact.objects(diseases=self):
            raise ValueError("Cannot delete a disease still linked to contacts.")
        super().delete(*args, **kwargs)

    def index_weight(self):
        score = (
            Contact._get_collection()
            .find(
                {
                    "diseases": self.pk,
                    # Make sure to use "diseases" index.
                    "_cls": {"$in": ["Contact.Person", "Contact.Organisation"]},
                }
            )
            .count()
        )
        return round(score / self.index_max_score, 2)

    def save(self, *args, **kwargs):
        inst = super().save(*args, **kwargs)
        if kwargs.pop("cascade", True):
            for contact in Contact.objects(diseases=self):
                # Update Contact.grouping
                contact.save(*args, **kwargs)
        return inst


class Event(ByAt, Searchable, Resource):
    meta = {"queryset_class": SearchableQueryset}
    relation_fields = [
        "pk",
        "name",
        "kind",
        "label",
        "resource",
        "start_date",
        "duration",
        "location",
    ]
    resource_fields = relation_fields + ByAt.resource_fields
    # Use autoincrement pk so we deal with readable ids in search URLs.
    id = SequenceField(required=True, primary_key=True)
    name = StringField(required=True, editable=True)
    kind = StringField(required=False, choices=constants.EVENT_KIND, editable=True)
    start_date = DateField(required=True, editable=True)
    duration = fields.IntField(required=True, editable=True)
    location = StringField(required=True, editable=True)

    def __str__(self):
        return f"{self.name} ({as_date(self.start_date)})"

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        # MongoDB pleasure. cf https://github.com/MongoEngine/mongoengine/issues/1592
        for role in self.attendees:
            role.events = [e for e in role.events if e._data["event"].id != self.pk]
            role.save()

    @property
    def attendees(self):
        return Role.objects.filter(events__event=self)


class Group(ByAt, Searchable, Resource):
    meta = {"queryset_class": SearchableQueryset}
    relation_fields = [
        "pk",
        "name",
        "description",
        "label",
        "resource",
        "kind",
    ]
    resource_fields = relation_fields + ByAt.resource_fields
    # Use autoincrement pk so we deal with readable ids in search URLs.
    id = SequenceField(required=True, primary_key=True)
    name = StringField(required=True, editable=True)
    description = StringField(required=False, editable=True)
    old_pk = StringField(required=False, editable=False)
    kind = StringField(required=False, editable=True, choices=constants.GROUP_KIND)

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        # MongoDB pleasure. cf https://github.com/MongoEngine/mongoengine/issues/1592
        for role in Role.objects.filter(groups__group=self):
            role.groups = [g for g in role.groups if g._data["group"].id != self.pk]
            role.save()


class Keyword(ByAt, Searchable, Resource):
    meta = {"queryset_class": SearchableQueryset}
    relation_fields = [
        "pk",
        "name",
        "for_person",
        "for_organisation",
        "label",
        "resource",
        "kind",
    ]
    resource_fields = relation_fields + ByAt.resource_fields
    # Use autoincrement pk so we deal with readable ids in search URLs.
    id = SequenceField(required=True, primary_key=True)
    name = StringField(required=True, editable=True)
    for_person = fields.BooleanField(default=True, editable=True, required=True)
    for_organisation = fields.BooleanField(default=True, editable=True, required=True)
    old_pk = StringField(required=False, editable=False)
    kind = StringField(required=False, editable=True, choices=constants.KEYWORD_KIND)

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        # MongoDB pleasure. cf https://github.com/MongoEngine/mongoengine/issues/1592
        for contact in Contact.objects.filter(keywords=self):
            contact.keywords = [k for k in contact.keywords if k.id != self.pk]
            contact.save()


class Version(Serializable, Document):
    indexes = [
        ([("of", pymongo.ASCENDING)], {"name": "ref"}),
        ([("by", pymongo.ASCENDING)], {"name": "by"}),
        (
            [("of", pymongo.ASCENDING), ("version", pymongo.ASCENDING)],
            {"unique": True, "name": "of_version"},
        ),
    ]
    resource_fields = relation_fields = ["at", "by", "diff"]
    COUNTER = "counter"

    at = DateTimeField(required=True)
    by = ReferenceField(Person, required=True)
    of = fields.GenericLazyReferenceField(required=True)
    raw = SimpleDictField(required=True)
    diff = SimpleDictField(required=True)
    version = fields.IntField(required=True)
    action = fields.StringField(required=True, choices=["create", "update", "delete"])
    label = fields.StringField()  # Keep a human readable label for deleted objects
    ghost = fields.BooleanField(default=False)

    def __repr__(self):
        ghost = " (👻)" if self.ghost else ""
        return f"<{self.__class__.__name__}: {self.version} of {self.of.id}{ghost}>"

    def __str__(self):
        return repr(self)

    @staticmethod
    def fetch_subordinates_data(subordinates):
        if subordinates is None:
            return
        # iterate over subordinates and add information using their PK
        for subordinate in filter(lambda subordinate: isinstance(subordinate, dict), subordinates):
            states = filter(lambda state: isinstance(state, list), [subordinate.get("new"), subordinate.get("old")])
            for state in states:
                for diff in state:
                    if diff.get("group"):
                        query = Group.objects.filter(pk=diff.get("group")).first()
                        if query and query.name:
                            diff["group_name"] = query.name
                    if diff.get("event"):
                        query = Event.objects.filter(pk=diff.get("event")).first()
                        if query and query.name:
                            diff["event_name"] = query.name
        return subordinates

    def as_resource(self):
        detailed_diff = deepcopy(self.diff)
        # add human readable data to the diff such as organisation names, event names, group names, ...
        for changes in detailed_diff.values():
            for change in changes:
                if isinstance(change, dict):
                    states = filter(lambda state: isinstance(state, dict), [change.get("new"), change.get("old")])
                    for state in states:
                        if state.get("organisation"):
                            query = Organisation.objects.filter(pk=state.get("organisation")).first()
                            if query and query.name:
                                state["organisation_name"] = query.name
                    if change.get("subordinates"):
                        change["subordinates"] = self.fetch_subordinates_data(change.get("subordinates"))
        return {
            "by": self.by.as_relation(),
            "of": self.of_as_resource(),
            "at": self.at.isoformat(),
            "diff": detailed_diff,
            "action": self.action,
        }

    def of_as_resource(self):
        try:
            of = self.of.fetch()
        except DoesNotExist:  # Deleted instance.
            resource = self.of.document_type.resource_name()
            pk = self.of.id
            return {
                "label": self.label,
                "pk": pk,
                "resource": resource,
                "uri": f"/{resource}/{pk}",
            }
        else:
            return {
                "label": str(of),
                "pk": of.pk,
                "resource": of.resource,
                "uri": of.uri,
            }

    @classmethod
    def incr_for(cls, instance):
        # Make sure we have a unique counter for each instance, whether we make
        # simultaneous updates in parallel.
        key = f"{instance.resource}:{instance.pk}"
        collection = instance._get_db()[cls.COUNTER]
        counter = collection.find_and_modify(
            query={"_id": key}, update={"$inc": {"next": 1}}, new=True, upsert=True
        )
        return counter["next"]

    @classmethod
    def backup(cls, inst, ghost=False):
        raw = inst.backup()
        if raw:
            version = Version(
                at=raw["modified_at"],
                by=raw["modified_by"],
                of=inst,
                raw=raw,
                ghost=ghost,
                version=inst.version,
                action="create" if raw["version"] == 1 else "update",
                label=str(inst),
            )
            version.diff = Diff.make(version, version.previous or {})
            if version.diff:
                version.save()
            return version

    @classmethod
    def mark_deleted(cls, inst):
        version = Version(
            at=utcnow(),
            by=Contact.modified_by.to_python(session.user_id.get()),
            of=inst,
            raw={},
            version=cls.incr_for(inst),
            action="delete",
            label=str(inst),
        )
        version.diff = Diff.make(version, version.previous or {})
        version.save()
        return version

    @property
    def previous(self):
        return (
            Version.objects(of=self.of, version__lt=self.version)
            .order_by("-version")
            .first()
        )

    @classmethod
    def history(cls, by=None, limit=10, action=None, field=None):
        qs = cls.objects(ghost__ne=True).order_by("-at", "-version").limit(limit)
        if action:
            qs = qs.filter(action=action)
        if field:
            qs = qs.filter(__raw__={f"diff.{field}": {"$exists": True}})
        return qs

    def restore(self):
        inst = self.of.document_type._from_son(self.raw)
        inst._changed_fields = list(inst._fields.keys())
        return inst
