from mongoengine import fields


class BooleanField(fields.BooleanField):

    TRUTHY = [True, "1", "true", "on"]

    def to_python(self, value):
        return value in self.TRUTHY

    # to_mongo is not called when using objects.modify()
    def prepare_query_value(self, op, value):
        return super().prepare_query_value(op, self.to_mongo(value))


class DateTimeField(fields.DateTimeField):
    def to_python(self, value):
        return self.to_mongo(value)  # Convert strings to datetime.


class DateField(DateTimeField):
    def to_python(self, value):
        return super().to_python(value).date()


class EmailField(fields.EmailField):
    def to_python(self, value):
        value = super().to_python(value)
        if value:
            value = value.lower()
        return value


class SequenceField(fields.SequenceField):
    def __init__(self, *args, **kwargs):
        # Prevent from casting to int everywhere in the API.
        kwargs["value_decorator"] = str
        super().__init__(*args, **kwargs)

    def to_python(self, value):
        value = super().to_python(value)
        return self.value_decorator(value)  # Make sure we deal with string in DBRefs.


class EmbeddedDocumentListField(fields.EmbeddedDocumentListField):
    def __set__(self, instance, value):
        super().__set__(instance, value)
        value = instance._data[self.name]
        instance._data[self.name] = self.to_python(value)


class ReferenceField(fields.ReferenceField):
    def to_python(self, value):
        if isinstance(value, dict):
            value = value["pk"]
        return super().to_python(value)


class SimpleDictField(fields.BaseField):
    """A trully dict field, not a huge ComplexField."""

    def __get__(self, instance, owner):
        value = super().__get__(instance, owner)
        if instance and instance._initialised:
            # Reference value may be changed in place, we can't know in advance, so
            # let's be defensive.
            instance._mark_as_changed(self.name)
        return value

    def to_python(self, value):
        return value or {}

    def validate(self, value):
        if not isinstance(value, dict):
            self.error("SimpleDictField only accepts dicts")


class StringField(fields.StringField):
    def to_python(self, value):
        value = super().to_python(value)
        if isinstance(value, int):
            value = str(value)
        return value


class GridFSProxy(fields.GridFSProxy):
    @property
    def id(self):
        return str(self.grid_id)

    def as_relation(self):
        self.get()
        exclude = ["_id", "uploadDate", "contentType", "md5", "chunkSize"]
        extra = {k: v for k, v in self.gridout._file.items() if k not in exclude}
        return {
            "id": self.id,
            "filename": self.gridout.filename,
            "content_type": self.gridout.content_type,
            "upload_date": self.gridout.upload_date,
            **extra,
        }


class FileField(fields.FileField):
    proxy_class = GridFSProxy
