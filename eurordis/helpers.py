import asyncio
import csv
import functools
import io
import logging
import re
from datetime import date, datetime, timezone
from functools import lru_cache

import editdistance
import requests
from unidecode import unidecode

from . import constants

logger = logging.getLogger(__name__)

NO_TITLE = re.compile(r"^(m[r?\.?|m?e|s]?|pr.?|dr\.?) ", re.IGNORECASE)


def utcnow():
    # Remove microsecond to ease testing datetimes, as MongoDB cut them at
    # 3 digits.
    return datetime.now(timezone.utc).replace(microsecond=0)


def as_date(v):
    return v.strftime("%d/%m/%Y")


def ngrams(value, n=3):
    # Add boundaries.
    value = f"^{value}"
    bound = (len(value) - (n - 1)) or 1
    return [value[i : i + n] for i in range(0, bound)]


def make_trigrams(text):
    return [word for phrase in map(ngrams, fold(text).split()) for word in phrase]


def alphanumerize(text):
    """Remove non alphanumeric chars."""
    return re.sub(
        " {2,}", " ", re.sub(r"\W", " ", re.sub(r"\.", "", text.replace("_", " ")))
    )


def fold(value):
    return phonemicize(clean(value))


def clean(value):
    return alphanumerize(unidecode(value.lower())).strip()


def clean_title(s):
    """Remove title from a name"""
    return NO_TITLE.sub("", s)


@lru_cache()
def phonemicize(s):
    """Very lite custom phonemicization.."""
    rules = (
        ("c(?=[^ieyw])", "k"),
        ("c$", "k"),
        ("(?<=[aeiouy])s(?=[aeiouy])", "z"),
        ("ph", "f"),
        ("(?<=[^sc])h", ""),
        ("(?<=[^siu])s$", ""),  # Remove trailing "s" unless after s, i or u
        ("^h(?=.)+", ""),
        (r"\bw", "v"),
        ("our", "or"),  # tumour vs tumor
        ("qu?", "k"),
        ("ou", "u"),  # androulla vs andrulla
        ("ae", "e"),  # haemato => hemato
        ("y(?=[^aeiou])?", "i"),  # y => i unless it's followed by a vowel
        ("(\\D)(?=\\1)", ""),  # Remove duplicate letters.
    )
    _s = s
    for pattern, repl in rules:
        _s = re.sub(pattern, repl, _s)
    return _s


FOLD_GROUPING = [(k, v, fold(v)) for k, v in constants.GROUPING.items()]
FOLD_COUNTRY = [
    (code, name, fold(name)) for code, name in constants.COUNTRIES.items()
] + [
    ("EU", "European Union", fold("European Union")),
    ("EUR", "Europe", fold("Europe")),
]
FOLD_KIND = [(k, v, fold(v)) for k, v in constants.ORGANISATION_KIND.items()]
FOLD_EVENT_KIND = [(k, v, fold(v)) for k, v in constants.EVENT_KIND.items()]
FOLD_EVENT_ROLE = [(v, v, fold(v)) for v in constants.EVENT_ROLE]
FOLD_KEYWORD_KIND = [(k, v, fold(v)) for k, v in constants.KEYWORD_KIND.items()]
FOLD_GROUP_KIND = [(k, v, fold(v)) for k, v in constants.GROUP_KIND.items()]
FOLD_GROUP_ROLE = [(v, v, fold(v)) for v in constants.GROUP_ROLES]


def complete_grouping(q, limit=3):
    return complete_from_list(q, FOLD_GROUPING, limit, resource="grouping")


def complete_country(q, limit=3):
    return complete_from_list(q, FOLD_COUNTRY, limit, resource="country")


def complete_kind(q, limit=3):
    return complete_from_list(q, FOLD_KIND, limit, resource="kind")


def complete_event_kind(q, limit=3):
    return complete_from_list(q, FOLD_EVENT_KIND, limit, resource="event_kind")


def complete_event_role(q, limit=3):
    return complete_from_list(q, FOLD_EVENT_ROLE, limit, resource="event_role")


def complete_keyword_kind(q, limit=3):
    return complete_from_list(q, FOLD_KEYWORD_KIND, limit, resource="keyword_kind")


def complete_group_kind(q, limit=3):
    return complete_from_list(q, FOLD_GROUP_KIND, limit, resource="group_kind")


def complete_group_role(q, limit=3):
    return complete_from_list(q, FOLD_GROUP_ROLE, limit, resource="group_role")


def complete_from_list(q, data, limit, resource):
    match = []
    q = fold(q)
    # Turn "ep mem" into "\be.?p.*\bm.?e.?m"
    regex = re.compile(".*".join(r"\b" + ".?".join(word) for word in q.split()))

    for key, label, cleaned in data:
        if cleaned == q:
            match.append((key, label, 4))
        elif cleaned.startswith(q):
            match.append((key, label, 3))
        elif q in cleaned:
            match.append((key, label, 2))
        elif regex.match(cleaned):
            match.append((key, label, 1))
        if len(match) >= limit:
            break
    return [
        {"pk": k, "label": label, "resource": resource}
        for k, label, score in sorted(match, key=lambda k: k[2], reverse=True)
    ]


def compute_editdistance(left, right):
    return 1 - editdistance.eval(left, right) / max(len(left), len(right))


def compare_str(search, label):
    if search.isdigit():  # FIXME OrphaNumber case.
        return 1
    search = fold(search)
    words = search.split()
    label = fold(label)
    search_str = search.replace(" ", "")
    label_str = label.replace(" ", "")
    if search_str == label_str:
        return 1
    if label_str.startswith(search_str):
        return 0.9
    if all(re.search(fr"\b{w}", label) for w in words):
        return round(0.7 + compute_editdistance(search, label) * 0.1, 2)
    return round(compute_editdistance(search, label), 2)


async def http(url, method="GET", **options):
    loop = asyncio.get_event_loop()
    response = await loop.run_in_executor(
        None, functools.partial(requests.request, method.upper(), url, **options)
    )
    if not response.ok:
        logger.error(response.content)
        raise ValueError(f"UPSTREAM_ERROR: {url} {response.status_code}")
    return response


def choose_language(wanted, available):
    common = set(wanted or []) & set(available)
    if not wanted or not common:
        return available[0]
    elif len(common) == 1:
        return common.pop()
    else:  # All wanted are common.
        return wanted[0]


def format_validation_error(error):
    # MongoEngine ValidationError is polymorphic.
    errors = {}
    if error.errors:
        for field, err in error.errors.items():
            while isinstance(err, dict):
                err = list(err.values())[0]
                continue
            errors[field] = err.message
    else:
        errors[error.field_name] = error.message
    if not errors:
        errors = {"error": "Unknown error"}
    elif len(errors) > 1:
        errors = {"errors": errors, "error": "Invalid data"}
    else:
        errors = {"error": list(errors.values())[0], "errors": errors}
    return errors


def lang_from_country(country):
    return constants.COUNTRY_TO_LANG.get(country, "en")


def format_for_tabular(raw, **collections):
    diseases = collections.get("diseases", {})
    groups = collections.get("groups", {})
    keywords = collections.get("keywords", {})
    authorship = collections.get("authorship", {})
    kinds = collections.get("kinds", constants.ORGANISATION_KIND)

    def format_value(key, value):
        if key in ["eurordis_membership", "rdi_membership", "membership"]:
            for membership in value:
                status = membership.get("status")
                if status in ["a", "f"] and not membership.get("end_date"):
                    value = constants.MEMBERSHIP_STATUSES.get(status, "")
                    break
            else:
                value = ""
        elif isinstance(value, list):
            value = ",".join(sorted(set(format_value(key, v) for v in value)))
        elif isinstance(value, dict):
            if key == "groups":
                role = value.get("role")
                if value["group"] in groups:
                    value = groups[value["group"]].name
                if role:
                    value = f"{value}/{role}"
            else:
                value = value.get("label", "")
        elif value is True:
            value = "1"
        elif value is False:
            value = ""
        elif key == "keywords":
            value = keywords.get(value, value)
        elif key in ("modified_by", "created_by"):
            value = authorship.get(value, value)
        elif key == "kind":
            value = kinds.get(value, "")
        elif key == "groupings":
            value = constants.GROUPING[value]
        elif key == "title":
            value = constants.TITLES[value]
        elif key == "diseases":
            value = str(diseases.get(value, value))
        elif isinstance(value, (datetime, date)):
            value = value.isoformat()
        elif isinstance(value, int):
            value = str(value)
        return value

    return {key: format_value(key, value) for key, value in raw.items() if value}


GROUPING_MAPPING = {
    "Adult Solid Rare Cancer": "euracan",
    "Rare Bone": "bond",
    "Rare Cardiac": "guardheart",
    "Rare Connective Tissue & Musculoskeletal": "reconnet",
    "Rare Craniofacial & ENT": "cranio",
    "Rare Endocrine": "endo",
    "Rare Epilepsies": "epicare",
    "Rare Eye": "eye",
    "Rare Gastrointestinal": "ernica",
    "Genetic tumour risk": "genturis",
    "Rare Haematological": "eurobloodnet",
    "Rare Hepatic": "rareliver",
    "Rare Immunodeficiency, Autoinflammatory & Autoimmune": "rita",
    "Rare Malformations, Developmental Anomalies & Intellectual Disabilities": "ithaca",
    "Rare Metabolic": "metab",
    "Rare Neurological": "rnd",
    "Rare Neuromuscular": "euronmd",
    "Paediatric cancer": "paedcan",
    "Rare Pulmonary": "lung",
    "Rare Renal": "erknet",
    "Rare Skin": "skin",
    "Transplant Child": "transplantchild",
    "Rare Urogenital": "eurogen",
    "Rare Vascular": "vasc",
}


def compare_grouping(other):
    from .models import Disease

    data = {}
    for line in other.splitlines()[1:]:
        orphanumber, groupings = line.split("=")
        grouping = [GROUPING_MAPPING[g.strip()] for g in groupings.split(";")]
        data[orphanumber] = {"others": set(grouping)}
    diseases = {
        str(d["_id"]): {
            "name": d.get("name"),
            "type": d.get("group_of_type"),
            "groupings": d.get("groupings"),
        }
        for d in Disease._get_collection().find(
            {}, ["name", "group_of_type", "groupings"]
        )
    }
    for orphanumber, disease in diseases.items():
        groupings = disease["groupings"]
        if not groupings:
            continue
        data.setdefault(orphanumber, {})
        data[orphanumber]["ours"] = set(g["name"] for g in groupings)
    out = io.StringIO()
    writer = csv.writer(out, delimiter=";")
    writer.writerow(
        [
            "orphanumber",
            "ours",
            "others",
            "status",
            "'+ours",
            "'+others",
            "name",
            "group of type",
        ]
    )
    for orphanumber, groupings in data.items():
        ours = groupings.get("ours", set())
        others = groupings.get("others", set())
        if ours == others:
            status = "="
        elif not ours:
            if orphanumber in diseases:
                status = "only in RBV"
            else:
                status = "not in RD-CODE"
        elif not others:
            status = "only in CDB"
        else:
            status = "different"
        metadata = diseases.get(orphanumber, {})
        writer.writerow(
            [
                orphanumber,
                ",".join(sorted(ours)),
                ",".join(sorted(others)),
                status,
                ",".join(sorted(ours - others)),
                ",".join(sorted(others - ours)),
                metadata.get("name"),
                metadata.get("type"),
            ]
        )
    out.seek(0)
    return out.read()
