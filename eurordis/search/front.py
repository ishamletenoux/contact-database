import re
from datetime import datetime, timedelta

from .back import get_collection
from .. import config, constants
from ..models import Role, Person, Disease, Event, Contact, Group, Keyword


class Filter:
    inherited = False
    REGISTRY = []

    def __init__(self, request):
        self.request = request

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.REGISTRY.append(cls)

    def __call__(self, values):
        out = []
        if self.skip():
            return out
        include, exclude, all_ = Filter.include_or_exclude(values)
        if include:
            filters = self.include(include)
            if filters:
                out.extend(filters)
        if exclude:
            filters = self.exclude(exclude)
            if filters:
                out.extend(filters)
        if all_:
            filters = self.all(all_)
            if filters:
                out.extend(filters)
        return out

    def skip(self):
        return False

    @property
    def inherited_key(self):
        return f"inherited_{self.internal}" if self.inherited else self.internal

    def extrapolate(self, values):
        out = []
        for v in values:
            out.extend(self.extrapolate_one(v))
        return out

    def extrapolate_one(self, value):
        # Nothing to extrapolate, as default.
        return [value or None]  # Cast "" to None

    def include(self, values):
        return [{self.internal: {"$in": self.extrapolate(values)}}]

    def exclude(self, values):
        return [{self.inherited_key: {"$nin": self.extrapolate(values)}}]

    def all(self, values):
        filters = []
        for value in values:
            # Match at least one from this group of values.
            filters.append({self.inherited_key: {"$in": self.extrapolate([value])}})
        return filters

    def filter_facet(self):
        return []

    def format_facet(self, raw):
        pass

    @staticmethod
    def include_or_exclude(values):
        include = []
        exclude = []
        all_ = []
        for value in values:
            if value.startswith("-"):
                exclude.append(value[1:])
            elif value.startswith("+"):
                all_.append(value[1:])
            else:
                include.append(value)
        return include, exclude, all_

    @staticmethod
    def ignore_case(values):
        return [re.compile(v, re.IGNORECASE) if v else None for v in values]


class KindFilter(Filter):

    external = "kind"
    internal = "kind"
    inherited = True

    def extrapolate_one(self, value):
        value = int(value)
        if value % 10 == 0:  # We look for a whole category
            return list(range(value, value + 10))
        return [value]


class DiseaseFilter(Filter):

    external = "disease"
    internal = "diseases"
    inherited = True

    def extrapolate_one(self, value):
        return Disease.extrapolate_search_pattern(value)


class GroupingFilter(Filter):

    external = "grouping"
    internal = "groupings"
    inherited = True


class EventFilter(Filter):

    external = "event"
    internal = "events.event"
    inherited = True

    def include(self, values):
        filters = {"event": {"$in": self.extrapolate(values)}}
        roles = self.request.query.list("event_role", [])
        if roles:
            include, exclude, all_ = self.include_or_exclude(roles)
            filters["role"] = {}
            if include or all_:
                # For now, let's pretend all_ equals include, as we don't have a better
                # behaviour and it's better than ignoring.
                include.extend(all_)
                filters["role"]["$in"] = self.ignore_case(include)
            if exclude:
                filters["role"]["$nin"] = self.ignore_case(exclude)
        return [{"events": {"$elemMatch": filters}}]


class EventRoleFilter(Filter):

    external = "event_role"
    internal = "events.role"
    inherited = True

    def skip(self):
        # Do not use this filter alone if event is defined, as it will be included in
        # the event elemMatch object.
        return bool(self.request.query.list("event", []))

    def extrapolate(self, values):
        return self.ignore_case(values)

    def include(self, values):
        values = self.extrapolate(values)
        return [{"events": {"$elemMatch": {"role": {"$in": values}}}}]


class EventKindFilter(EventFilter):
    external = "event_kind"
    inherited = True

    def extrapolate(self, values):
        return list(Event.objects(kind__in=values).scalar("pk").no_dereference())


class CountryFilter(Filter):

    external = "country"
    internal = "country"


class KeywordFilter(Filter):

    external = "keyword"
    internal = "keywords"


class KeywordKindFilter(KeywordFilter):
    external = "keyword_kind"

    def extrapolate(self, values):
        return list(Keyword.objects(kind__in=values).scalar("pk").no_dereference())


class GroupFilter(Filter):

    external = "group"
    internal = "groups.group"
    inherited = True

    @staticmethod
    def status(values):
        values = values or []
        cleaned = [v[1:] if v and not v[0].isalnum() else v for v in values]
        if not values or values[0].startswith("*"):
            return None, cleaned
        if values[0].startswith("<"):
            return False, cleaned
        return True, cleaned

    def include(self, values):
        # Only the first group status is considered, all the following ones
        # are ignored
        values = self.extrapolate(values)
        status, values = self.status(values)
        status = {"is_active": status} if status is not None else {}
        filter_ = {"group": {"$in": values}, **status}
        roles = self.request.query.list("group_role", [])
        if roles:
            include, exclude, all_ = self.include_or_exclude(roles)
            filter_["role"] = {}
            if all_:
                filters = []
                for role in all_:
                    filter_["role"] = role
                    filters.append({"groups": {"$elemMatch": dict(filter_)}})
                return filters
            if include:
                # Cast "" to None
                filter_["role"]["$in"] = [g or None for g in include]
            if exclude:
                filter_["role"]["$nin"] = [g or None for g in exclude]
        return [{"groups": {"$elemMatch": filter_}}]

    def all(self, values):
        filters = []
        values = self.extrapolate(values)
        for value in values:
            status, (value, *_) = self.status([value])
            status = {"is_active": status} if status is not None else {}
            filters.append(
                {"inherited_groups": {"$elemMatch": {"group": value, **status}}}
            )
        return filters

    def exclude(self, values):
        values = self.extrapolate(values)
        status, values = self.status(values)
        if status is None:
            return super().exclude(values)
        return [
            {
                "inherited_groups": {
                    "$not": {
                        "$elemMatch": {"is_active": status, "group": {"$in": values}}
                    }
                }
            }
        ]


class GroupRoleFilter(Filter):

    external = "group_role"
    internal = "groups.role"
    inherited = True

    def skip(self):
        group = self.request.query.list("group", [])
        group_kind = self.request.query.list("group_kind", [])
        # On group exclusion, we'll still manage group_role from here.
        if group and not group[0].startswith("-"):
            return True
        if group_kind and not group_kind[0].startswith("-"):
            return True

    def include(self, values):
        status, values = GroupFilter.status(values)
        values = self.extrapolate(values)
        if status is None:
            return super().include(values)
        return [
            {"groups": {"$elemMatch": {"is_active": status, "role": {"$in": values}}}}
        ]

    def exclude(self, values):
        status, values = GroupFilter.status(values)
        values = self.extrapolate(values)
        if status is None:
            return super().exclude(values)
        return [
            {
                "inherited_groups": {
                    "$not": {
                        "$elemMatch": {"is_active": status, "role": {"$in": values}}
                    }
                }
            }
        ]

    def all(self, values):
        status, values = GroupFilter.status(values)
        values = self.extrapolate(values)
        if status is None:
            return super().all(values)
        filters = []
        for value in values:
            # Match at least one from this group of values.
            filters.append(
                {"groups": {"$elemMatch": {"is_active": status, "role": value}}}
            )
        return filters


class GroupKindFilter(GroupFilter):
    external = "group_kind"

    def extrapolate_one(self, value):
        operator = None
        if not value[:1].isalnum():
            operator = value[0]
            value = value[1:]
        values = list(Group.objects(kind=value).scalar("pk").no_dereference())
        if operator:
            values = [f"{operator}{v}" for v in values]
        return values


class MembershipFilter(Filter):

    external = "membership"
    internal = "membership.organisation"
    inherited = True

    def include(self, values):
        return [
            {
                "membership": {
                    "$elemMatch": {
                        "organisation": {"$in": values},
                        "end_date": None,
                        "status": {"$ne": "w"},
                    }
                }
            }
        ]

    def all(self, values):
        filters = []
        for value in values:
            filters.append(
                {
                    "membership": {
                        "$elemMatch": {
                            "organisation": value,
                            "end_date": None,
                            "status": {"$ne": "w"},
                        }
                    }
                }
            )
        return filters


class SearchQuery:
    def __init__(self, request):
        self.request = request
        self.filters = []
        self.q = None
        self.limit = self.request.query.int("limit", 100)
        self.parse_filters()
        self.parse_q()

    def __bool__(self):
        return bool(self.q or self.filters)

    def parse_filters(self):
        for klass in Filter.REGISTRY:
            values = self.request.query.list(klass.external, [])
            if not values:
                continue
            if values[0] == "-":
                self.filters.append({klass.internal: {"$in": [None, []]}})
            else:
                filter_ = klass(self.request)
                self.filters.extend(filter_(values))

    def parse_q(self):
        q = self.request.query.get("q", "")
        ft = []
        for node in q.split():
            if ":" in node:
                field, value = node.split(":", maxsplit=1)
                operator = None
                if field == "no":
                    if value == "inherit":
                        # Exclude roles from index.
                        self.filters.append({"_id.organisation": {"$exists": False}})
                        continue
                    elif value in ["voting", "primary"]:
                        organisations = Role._get_collection().find(
                            {f"is_{value}": True}, {"organisation": 1, "_id": 0}
                        )
                        pks = set(r["organisation"] for r in organisations)
                        self.filters.append(
                            {"pk": {"$nin": list(pks)}, "_cls": "Contact.Organisation"}
                        )
                        continue
                    field = value
                    self.filters.append({field: {"$in": [None, []]}})
                elif field == "has":
                    field = value
                    field, *modifiers = field.split(":")
                    if field == "groups":
                        status = modifiers[0] if modifiers else "active"
                        if status != "all":
                            status = True if status == "active" else False
                            self.filters.append(
                                {"groups": {"$elemMatch": {"is_active": status}}}
                            )
                            # If status is all, just continue to normal filters.
                            continue
                    self.filters.append({field: {"$exists": True}})
                    self.filters.append({field: {"$nin": [None, [], ""]}})
                elif field == "members":
                    try:
                        name, value = value.split(":", maxsplit=1)
                    except ValueError:
                        continue
                    org = config.RDI_PK if name == "rdi" else config.EURORDIS_PK
                    self.filters.append(
                        {
                            "membership": {
                                "$elemMatch": {
                                    "organisation": org,
                                    "end_date": None,
                                    "status": value,
                                }
                            }
                        }
                    )
                elif field == "is":
                    if value == "orphan":
                        organisations = Role._get_collection().find(
                            {}, {"organisation": 1, "_id": 0}
                        )
                        persons = Role._get_collection().find(
                            {}, {"person": 1, "_id": 0}
                        )
                        roles = set(r["organisation"] for r in organisations) | set(
                            r["person"] for r in persons
                        )
                        self.filters.append({"pk": {"$nin": list(roles)}})
                    elif value == "offline":
                        persons = Role._get_collection().find(
                            {"email": {"$ne": None}}, {"person": 1, "_id": 0}
                        )
                        roles = set(r["person"] for r in persons)
                        self.filters.append(
                            {
                                "pk": {"$nin": list(roles)},
                                "email": None,
                                "_cls": "Contact.Person",
                            }
                        )
                    elif value == "unreachable":
                        orgas = [
                            r["_id"]
                            for r in Contact._get_collection().find(
                                {
                                    "email": {"$ne": None},
                                    "_cls": "Contact.Organisation",
                                },
                                {"_id": 1},
                            )
                        ]
                        persons = Role._get_collection().find(
                            {
                                "$or": [
                                    {"email": {"$ne": None}},
                                    {"organisation": {"$in": orgas}},
                                ]
                            },
                            {"person": 1, "_id": 0},
                        )
                        roles = set(r["person"] for r in persons)
                        self.filters.append(
                            {
                                "pk": {"$nin": list(roles)},
                                "email": None,
                                "_cls": "Contact.Person",
                            }
                        )
                    elif value == "mute":
                        persons = Role._get_collection().find(
                            {"phone": {"$ne": None}}, {"person": 1, "_id": 0}
                        )
                        pks = set(r["person"] for r in persons)
                        self.filters.append(
                            {
                                "pk": {"$nin": list(pks)},
                                "phone": None,
                                "cell_phone": None,
                                "_cls": "Contact.Person",
                            }
                        )
                else:
                    if value.startswith("-"):
                        value = value[1:]
                        operator = "$ne"
                    elif value.startswith("<"):
                        value = value[1:]
                        operator = "$lt"
                    elif value.startswith(">"):
                        value = value[1:]
                        operator = "$gt"
                    if value.isdigit():  # Poor man duck typing.
                        value = int(value)
                    # Allow to force a str even with a digit, as for a pk
                    elif value.startswith('"') and value.endswith('"'):
                        value = value[1:-1]
                    elif "@" in value:
                        person = Person.from_email(value)
                        if person:
                            value = person.pk
                    elif field.endswith(("_at", "_date")):
                        try:
                            value = datetime.strptime(value, r"%Y-%m-%d")
                        except ValueError:
                            pass
                        else:
                            if not operator:  # Can't compare date with datetime.
                                value = {
                                    "$gte": value,
                                    "$lt": value + timedelta(days=1),
                                }
                    if operator:
                        value = {operator: value}
                    self.filters.append({field: value})
            elif "@" in node:
                self.filters.append({"email": re.compile(node, re.IGNORECASE)})
            else:
                ft.append(node)
        self.q = " ".join(ft)

    def filter_fk_for_facets(self, key):
        values = self.request.query.list(key, [])
        return [
            v[1:] if v.startswith("+") else v for v in values if not v.startswith("-")
        ]

    def compute_facets(self, pks):
        collection = get_collection()
        events = self.filter_fk_for_facets("event")
        event_kinds = self.request.query.list("event_kind", [])
        if event_kinds:
            events.extend(
                Event.objects(kind__in=event_kinds).scalar("pk").no_dereference()
            )
        match_events = [{"$match": {"events.event": {"$in": events}}}] if events else []
        groups = self.filter_fk_for_facets("group")
        keywords = self.filter_fk_for_facets("keyword")
        match_keywords = (
            [{"$match": {"keywords": {"$in": keywords}}}] if keywords else {}
        )
        status = True  # Default.
        if groups:
            status, groups = GroupFilter.status(groups)
        match_group_status = (
            [{"$match": {"groups.is_active": status}}] if status is not None else []
        )
        groups = [g[1:] if not g[:1].isdigit() else g for g in groups]
        group_kinds = self.filter_fk_for_facets("group_kind")
        if group_kinds:
            groups.extend(
                Group.objects(kind__in=group_kinds).scalar("pk").no_dereference()
            )
        match_groups = [{"$match": {"groups.group": {"$in": groups}}}] if groups else {}
        limit = 24
        self._raw_facets = collection.aggregate(
            [
                {
                    "$match": {
                        "_cls": {"$in": ["Contact.Person", "Contact.Organisation"]},
                        "pk": {"$in": pks},
                    }
                },
                {
                    "$facet": {
                        "disease": [
                            {"$unwind": "$diseases"},
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "disease": "$diseases"},
                                    "diseases": {"$first": "$diseases"},
                                }
                            },
                            {"$sortByCount": "$diseases"},
                            {"$limit": limit},
                        ],
                        "grouping": [
                            {"$unwind": "$groupings"},
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "grouping": "$groupings"},
                                    "grouping": {"$first": "$groupings"},
                                }
                            },
                            {"$sortByCount": "$grouping"},
                            {"$limit": limit},
                        ],
                        "country": [
                            {"$unwind": "$country"},
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "country": "$country"},
                                    "country": {"$first": "$country"},
                                }
                            },
                            {"$sortByCount": "$country"},
                            {"$limit": limit},
                        ],
                        "event": [
                            {"$unwind": "$events"},
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "event": "$events.event"},
                                    "event": {"$first": "$events.event"},
                                }
                            },
                            {"$sortByCount": "$event"},
                            {"$limit": limit},
                        ],
                        "event_kind": [
                            {"$unwind": "$events"},
                            *match_events,
                            {
                                "$lookup": {
                                    "from": "event",
                                    "localField": "events.event",
                                    "foreignField": "_id",
                                    "as": "event",
                                }
                            },
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "event": "$event.kind"},
                                    "kind": {"$first": "$event.kind"},
                                }
                            },
                            {"$unwind": "$kind"},
                            {"$sortByCount": "$kind"},
                            {"$limit": limit},
                        ],
                        "event_role": [
                            {"$unwind": "$events"},
                            *match_events,
                            {
                                "$group": {
                                    "_id": {
                                        "_id": "$pk",
                                        "role": {"$ifNull": ["$events.role", None]},
                                    },
                                    "role": {"$first": "$events.role"},
                                }
                            },
                            {"$sortByCount": "$role"},
                            {"$limit": limit},
                        ],
                        "kind": [
                            {"$unwind": "$kind"},
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "kind": "$kind"},
                                    "kind": {"$first": "$kind"},
                                }
                            },
                            {"$sortByCount": "$kind"},
                            {"$limit": limit},
                        ],
                        "kind_category": [
                            {"$unwind": "$kind"},
                            {
                                "$project": {
                                    "pk": "$pk",
                                    "kind": {"$floor": {"$divide": ["$kind", 10]}},
                                }
                            },
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "kind": "$kind"},
                                    "kind": {"$first": "$kind"},
                                }
                            },
                            {"$sortByCount": "$kind"},
                            {"$limit": limit},
                        ],
                        "group": [
                            {"$unwind": "$groups"},
                            *match_group_status,
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "group": "$groups.group"},
                                    "group": {"$first": "$groups.group"},
                                }
                            },
                            {"$sortByCount": "$group"},
                            {"$limit": limit},
                        ],
                        "group_role": [
                            {"$unwind": "$groups"},
                            *match_groups,
                            {
                                "$group": {
                                    "_id": {
                                        "_id": "$pk",
                                        "role": {"$ifNull": ["$groups.role", None]},
                                    },
                                    "role": {"$first": "$groups.role"},
                                }
                            },
                            {"$sortByCount": "$role"},
                            {"$limit": limit},
                        ],
                        "group_kind": [
                            {"$unwind": "$groups"},
                            *match_groups,
                            {
                                "$lookup": {
                                    "from": "group",
                                    "localField": "groups.group",
                                    "foreignField": "_id",
                                    "as": "group",
                                }
                            },
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "group": "$group.kind"},
                                    "kind": {"$first": "$group.kind"},
                                }
                            },
                            {"$unwind": "$kind"},
                            {"$sortByCount": "$kind"},
                            {"$limit": limit},
                        ],
                        "resource": [
                            {"$unwind": "$_cls"},
                            {"$group": {"_id": "$pk", "resource": {"$first": "$_cls"}}},
                            {"$sortByCount": "$resource"},
                            {"$limit": limit},
                        ],
                        "membership": [
                            {"$unwind": "$membership"},
                            {
                                "$match": {
                                    "membership.end_date": None,
                                    "membership.status": {"$ne": "w"},
                                }
                            },
                            {
                                "$group": {
                                    "_id": {
                                        "_id": "$pk",
                                        "membership": "$membership.organisation",
                                    },
                                    "membership": {
                                        "$first": "$membership.organisation"
                                    },
                                }
                            },
                            {"$sortByCount": "$membership"},
                            {"$limit": 5},
                        ],
                        "keyword": [
                            {"$unwind": "$keywords"},
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "keyword": "$keywords"},
                                    "keyword": {"$first": "$keywords"},
                                }
                            },
                            {"$sortByCount": "$keyword"},
                            {"$limit": limit},
                        ],
                        "keyword_kind": [
                            {"$unwind": "$keywords"},
                            *match_keywords,
                            {
                                "$lookup": {
                                    "from": "keyword",
                                    "localField": "keywords",
                                    "foreignField": "_id",
                                    "as": "keyword",
                                }
                            },
                            {
                                "$group": {
                                    "_id": {"_id": "$pk", "keyword": "$keyword.kind"},
                                    "kind": {"$first": "$keyword.kind"},
                                }
                            },
                            {"$unwind": "$kind"},
                            {"$sortByCount": "$kind"},
                            {"$limit": limit},
                        ],
                    }
                },
            ]
        ).next()
        facets = {}
        for key, data in self._raw_facets.items():
            func = getattr(self, f"{key}_facet", None)
            if not func:
                continue  # Helper facet.
            facets[key] = sorted(
                func(data),
                key=lambda x: (x["count"], x.get("weight", x["pk"] or "")),
                reverse=True,
            )
        return facets

    def disease_facet(self, raw):
        disease_pks = [d["_id"] for d in raw]
        diseases = {
            pk: (label, depth, max_depth)
            for pk, label, depth, max_depth in Disease.objects(pk__in=disease_pks)
            .scalar("pk", "name", "depth", "max_depth")
            .no_dereference()
        }
        return [
            {
                "pk": d["_id"],
                "label": diseases[d["_id"]][0],
                "count": d["count"],
                "depth": diseases[d["_id"]][1],
                "max_depth": diseases[d["_id"]][2],
            }
            for d in raw
        ]

    def event_facet(self, raw):
        event_pks = [d["_id"] for d in raw]
        events = {
            pk: label
            for pk, label in Event.objects(pk__in=event_pks)
            .scalar("pk", "name")
            .no_dereference()
        }
        return [
            {"pk": d["_id"], "label": events[d["_id"]], "count": d["count"]}
            for d in raw
        ]

    def event_kind_facet(self, raw):
        return [
            {
                "pk": c["_id"],
                "label": constants.EVENT_KIND.get(c["_id"], c["_id"]),
                "count": c["count"],
            }
            for c in raw
        ]

    def event_role_facet(self, raw):
        return [
            {"pk": c["_id"] or "", "label": c["_id"] or "[Empty]", "count": c["count"]}
            for c in raw
        ]

    def membership_facet(self, raw):
        membership_pks = [d["_id"] for d in raw]
        membership = {
            pk: label
            for pk, label in Contact.objects(pk__in=membership_pks)
            .scalar("pk", "name")
            .no_dereference()
        }
        return [
            {"pk": d["_id"], "label": membership[d["_id"]], "count": d["count"]}
            for d in raw
            if d["_id"]
        ]

    def country_facet(self, raw):
        ordered = list(constants.COUNTRIES_AND_AREAS.keys())
        return [
            {
                "pk": c["_id"],
                "label": constants.COUNTRIES_AND_AREAS.get(c["_id"]),
                "count": c["count"],
                # Used for secondary sort.
                "weight": ordered.index(c["_id"]) if c["_id"] in ordered else 0,
            }
            for c in raw
        ]

    def kind_facet(self, raw):
        # Compute category counters.
        categories = {
            f["_id"] * 10: f["count"] for f in self._raw_facets["kind_category"]
        }
        for kind in raw:
            if kind["_id"] % 10 == 0:
                kind["count"] = categories.get(kind["_id"], kind["count"])
        return [
            {
                "pk": c["_id"],
                "label": constants.ORGANISATION_KIND[c["_id"]],
                "count": c["count"],
            }
            for c in raw
        ]

    def grouping_facet(self, raw):
        return [
            {
                "pk": c["_id"],
                "label": constants.GROUPING.get(c["_id"], c["_id"]),
                "count": c["count"],
            }
            for c in raw
        ]

    def group_facet(self, raw):
        group_pks = [d["_id"] for d in raw]
        groups = {
            pk: label
            for pk, label in Group.objects(pk__in=group_pks)
            .scalar("pk", "name")
            .no_dereference()
        }
        return [
            {"pk": c["_id"], "label": groups[c["_id"]], "count": c["count"]}
            for c in raw
            if c["_id"]
        ]

    def group_role_facet(self, raw):
        return [
            {
                "pk": c["_id"] or None,
                "label": c["_id"] or "[Empty]",
                "count": c["count"],
            }
            for c in raw
        ]

    def group_kind_facet(self, raw):
        return [
            {
                "pk": c["_id"],
                "label": constants.GROUP_KIND.get(c["_id"], c["_id"]),
                "count": c["count"],
            }
            for c in raw
        ]

    def keyword_facet(self, raw):
        keyword_pks = [d["_id"] for d in raw]
        keywords = {
            pk: label
            for pk, label in Keyword.objects(pk__in=keyword_pks)
            .scalar("pk", "name")
            .no_dereference()
        }
        return [
            {"pk": c["_id"], "label": keywords[c["_id"]], "count": c["count"]}
            for c in raw
            if c["_id"]
        ]

    def keyword_kind_facet(self, raw):
        return [
            {
                "pk": c["_id"],
                "label": constants.KEYWORD_KIND.get(c["_id"], c["_id"]),
                "count": c["count"],
            }
            for c in raw
        ]

    def resource_facet(self, raw):
        # Values are in the form Contact.Person, Contact.Organisation.
        return [
            {
                "pk": c["_id"][8:].lower(),
                "label": c["_id"][8:].title(),
                "count": c["count"],
            }
            for c in raw
        ]
