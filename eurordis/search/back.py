import pymongo
from mongoengine.connection import get_db
from pymongo.errors import OperationFailure
from mongoengine import QuerySet

from .. import constants
from ..helpers import make_trigrams


class SearchableQueryset(QuerySet):
    def steps(self, text=None, limit=3, **filters):
        # Prevent MongoEngine to add an initial pipeline stage to filter the
        # class. Only the first pipeline stage uses the index, so that stage
        # would prevent trigrams index to be used.
        self._class_check = False
        _cls_query = {"_cls": {"$in": self._document._subclasses}}
        if "$and" in filters:
            # Put it before, to use indexes.
            filters["$and"].insert(0, _cls_query)
        else:
            filters.update(_cls_query)
        if text:
            trigrams = list(set(make_trigrams(text)))
            # aggregate will return full documents, do let's cheat mongodb in order
            # to get back full python object (so we can use serialize, while
            # keeping this query generic)
            steps = [
                # Only select documents that match at least one trigram.
                {
                    "$match": {
                        "ft.trigrams": {"$elemMatch": {"$in": trigrams}},
                        **filters,
                    }
                },
                # Explode searchable strings.
                {"$unwind": {"path": "$ft"}},
                {
                    "$project": {
                        "_id": "$pk",
                        "role": "$role",
                        "sort": "$sort",
                        "priority": "$priority",
                        "found": {
                            "$size": {
                                # Dedupe the matched trigrams.
                                "$setUnion": [
                                    {
                                        # Extract the trigram that matched.
                                        "$filter": {
                                            "input": "$ft.trigrams",
                                            "as": "item",
                                            "cond": {"$in": ["$$item", trigrams]},
                                        }
                                    }
                                ]
                            }
                        },
                        "length": {"$size": {"$setUnion": "$ft.trigrams"}},
                        "label_weight": "$ft.score",
                        "inst_weight": "$weight",
                    }
                },
                {
                    "$project": {
                        "role": "$role",
                        "sort": "$sort",
                        "score": {
                            "$divide": [
                                {
                                    "$add": [
                                        # How much the query matched THAT document.
                                        {"$divide": ["$found", "$length"]},
                                        # How much THAT document matched the query
                                        {
                                            "$multiply": [
                                                {"$divide": ["$found", len(trigrams)]},
                                                1.3,
                                            ]
                                        },
                                        {"$multiply": ["$label_weight", 0.1]},
                                        {"$multiply": ["$inst_weight", 0.1]},
                                    ]
                                },
                                2.5,  # Max score.
                            ]
                        },
                        "found": "$found",
                        "length": "$length",
                        "priority": "$priority",
                    }
                },
                {"$match": {"score": {"$gte": 0.5}}},
                {"$sort": {"priority": 1}},
                {
                    "$group": {
                        "_id": "$_id",  # Distinct by id.
                        "score": {"$max": "$score"},
                        "found": {"$first": "$found"},
                        "length": {"$first": "$length"},
                        "role": {"$first": "$role"},
                        "sort": {"$first": "$sort"},
                    }
                },
                {"$sort": {"score": -1}},
            ]
        else:
            steps = [
                {"$match": filters},
                {"$sort": {"priority": 1}},
                {
                    "$group": {
                        "_id": "$pk",
                        "role": {"$first": "$role"},
                        "sort": {"$first": "$sort"},
                    }
                },
                {"$sort": {"sort": 1}},
            ]
        if limit:
            # No way to pass a null limit to MongoDB.
            steps.append({"$limit": limit})
        return steps

    def search(self, text=None, limit=3, raw=False, **filters):
        collection = get_collection()
        steps = self.steps(text=text, limit=limit, **filters)
        if raw:
            return collection.aggregate(steps)
        steps.extend(
            [
                {
                    "$lookup": {
                        "from": self._document._get_collection_name(),
                        "localField": "_id",
                        "foreignField": "_id",
                        "as": "fromItems",
                    }
                },
                {"$replaceRoot": {"newRoot": {"$arrayElemAt": ["$fromItems", 0]}}},
            ]
        )
        self._cursor_obj = collection.aggregate(steps)
        return self

    def context_search(self, text=None, limit=3, raw=False, **filters):
        """Search person with their role context."""
        collection = get_collection()
        steps = self.steps(text=text, limit=limit, **filters)
        steps.append({"$project": {"pk": "$_id", "role": "$role", "_id": False}})
        return collection.aggregate(steps)


class Searchable:
    label_fields = [("name", 1)]
    _index_max_scores = {}

    def save(self, *args, **kwargs):
        inst = super().save(*args, **kwargs)
        cascade = kwargs.get("cascade", True)
        inst.index(cascade)
        return inst

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        self.deindex()

    @property
    def ft(self):
        return make_ft(self)

    @property
    def labels(self):
        labels = []
        for name, score in self.label_fields:
            value = getattr(self, name, None)
            if not isinstance(value, list):
                value = [value]
            labels.extend(v for v in value if v)
        return list(set(labels))  # Deduplicate

    def _index_id(self):
        """value for _id in search index"""
        return {self.resource: self.pk}

    def index_weight(self):
        return 1

    def as_index(self):
        doc = dict(self.to_mongo())
        doc["ft"] = self.ft
        doc["pk"] = doc["_id"]
        doc["_id"] = {self.resource: self.pk}
        doc["sort"] = str(self).lower()
        doc["priority"] = 1
        doc["weight"] = self.index_weight()
        doc["_cls"] = self._class_name
        country = doc.get("country")
        if country:
            if not isinstance(country, list):
                country = [country]
            if set(country) & set(constants.EU_COUNTRIES):
                country.append("EU")
            if set(country) & set(constants.EUROPE_COUNTRIES):
                country.append("EUR")
            doc["country"] = country
        return doc

    @property
    def _deindex_filter(self):
        return {f"_id.{self.resource}": self.pk}

    def index(self, cascade):
        index(self.as_index())

    def deindex(self):
        deindex(self._deindex_filter)

    @property
    def index_max_score(self):
        return self._index_max_scores[self.resource]


def index(doc):
    collection = get_collection()
    collection.replace_one({"_id": doc["_id"]}, doc, upsert=True)


def deindex(doc):
    collection = get_collection()
    collection.delete_many(doc)


def drop_collection():
    get_collection().drop()


def create_indexes():
    print("Creating search indexes")
    collection = get_collection()
    indexes = [
        ([("ft.trigrams", pymongo.ASCENDING)], {"name": "ft"}),
        (
            [("_cls", pymongo.ASCENDING), ("ft.trigrams", pymongo.ASCENDING)],
            {"name": "cls"},
        ),
        ([("_cls", pymongo.ASCENDING), ("pk", pymongo.ASCENDING)], {"name": "pks"},),
    ]
    for fields_, options in indexes:
        try:
            collection.drop_index(options["name"])
        except OperationFailure:
            pass  # Index does not exist ?
        name = collection.create_index(fields_, **options)
        print(f"  Created index {name}")


def get_collection():
    return get_db()["search"]


def make_ft(inst):
    out = []
    for name, score in inst.label_fields:
        value = getattr(inst, name, None)
        if not isinstance(value, list):
            value = [value]
        for s in value:
            trigrams = make_trigrams(s or "")
            if not trigrams:
                continue
            ft = {"trigrams": trigrams, "score": score}
            if ft not in out:
                out.append(ft)
    return out


def compute_index_max_scores():
    from eurordis.models import Contact
    qs = Contact._get_collection().aggregate(
        [
            {"$project": {"diseases": "$diseases"}},
            {"$unwind": "$diseases"},
            {"$facet": {"disease": [{"$sortByCount": "$diseases"}, {"$limit": 1}]}},
            {"$project": {"total": {"$arrayElemAt": ["$disease.count", 0]}}},
        ]
    )
    Searchable._index_max_scores["disease"] = qs.next().get("total") or 1


def init():
    compute_index_max_scores()
