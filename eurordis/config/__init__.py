import os
from pathlib import Path

getenv = os.environ.get

READONLY = False
ROOT = Path(__file__).parent
DATA_ROOT = ROOT.parent.parent / "data"
DBNAME = "eurordis"
DBHOST = "localhost"
DBPORT = 27017
MAILCHIMP_APIKEY = ""
MAILCHIMP_USER = "eurordis"
MAILCHIMP_LIST_EURORDIS_MEMBERS = ""
MAILCHIMP_LIST_RDI_MEMBERS = ""
MAILCHIMP_LIST_TEST = ""
MAILCHIMP_MONITORING_INIT_KEYWORD = 0
MAILCHIMP_LIST_ALL = ""
MAILCHIMP_LIST_ALUMNI = ""
MAILCHIMP_REPORT_EMAILS = ["contact-database@eurordis.org"]
EURORDIS_FM_PK = "so4146"
EURORDIS_PK = "1554"
EURORDIS_STR = "eurordis"
INDIVIDUALS_ORG = "17805"
RDI_PK = "4524"
RDI_STR = "rdi"
JWT_SECRET = "sikretfordevonly"
JWT_ALGORITHM = "HS256"
SEND_EMAILS = False
SEND_REPORT_EMAILS = ["contact-database@eurordis.org"]
SMTP_HOST = "127.0.0.1"
SMTP_PORT = 1025
SMTP_PASSWORD = ""
SMTP_LOGIN = ""
SMTP_TLS = False
FROM_EMAIL = "contact-database@eurordis.org"
FROM_EMAIL_RECEIPTS = "annie.rahajarizafy@eurordis.org"
ACCESS_EMAIL_DOMAINS = ("@eurordis.org", "@rarediseasesint.org")
BOT_EMAIL = "contact-database@eurordis.org"
API_URL = ""
STAFF = ["contact-database@eurordis.org"]
SOFT_DELETE_QUARANTINE = 7  # Days.
TOBEREVIEWED_KEYWORD = 0
TOBEDELETED_KEYWORD = 0
HARDBOUNCE_KEYWORD = 0
GDPR_HARDBOUNCE_KEYWORD = 0
MAILCHIMP_TEST_KEYWORD = 60
NON_RARE_ORPHA_NUMBER = 0
# ePAG - European Patient Advocacy Group
EPAG_GROUP = 5


def init():
    for key, value in globals().items():
        if key.isupper():
            env_key = "EURORDIS_" + key
            typ = type(value)
            if typ in (list, tuple, set):
                real_type, typ = typ, lambda x: real_type(x.split(","))
            if env_key in os.environ:
                globals()[key] = typ(os.environ[env_key])


init()
