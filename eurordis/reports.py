import csv
import io
from itertools import count
import re
from dataclasses import astuple, dataclass
from datetime import datetime
from collections import defaultdict

from eurordis import config, constants, helpers
from eurordis.models import (
    Contact,
    Disease,
    Event,
    Group,
    Keyword,
    Organisation,
    Person,
    Role,
    Version,
)
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook


def fees(year):
    wb = Workbook()
    ws = wb.active
    ws.title = "Eurordis Contact Database"  # Less than 32 chars.
    ws.append(["id", "old id", "name", "country", "fee", "payment date", "status"])
    EURORDIS = Organisation.get_organisation(config.EURORDIS_STR)
    orgs = Organisation.objects.filter(
        __raw__={
            "membership": {
                "$elemMatch": {"organisation": str(EURORDIS.pk), "fees.year": year}
            }
        }
    ).order_by("id")
    for org in orgs:
        amount = 0
        payment_date = ""
        status = ""
        for membership in org.membership:
            if membership.organisation == EURORDIS:
                for fee in membership.fees:
                    if fee.year == year:
                        amount = fee.amount
                        if fee.payment_date:
                            payment_date = fee.payment_date.isoformat()
                        status = constants.MEMBERSHIP_STATUSES.get(membership.status)
                        break
        ws.append(
            [
                org.pk,
                org.filemaker_pk,
                org.label,
                org.country,
                amount,
                payment_date,
                status,
            ]
        )
    return save_virtual_workbook(wb)


@dataclass
class Year:
    year: int
    total: int = 0
    withdrawn: int = 0
    new: int = 0
    full: int = 0
    associate: int = 0
    unpaid: int = 0
    never_paid: int = 0
    exempted: int = 0
    paid: int = 0
    percent_paid: int = 0
    total_fees: int = 0


def members_stats(membership):
    """
    Args:
        membership (str): 'eurordis' or 'rdi'
    """
    membership_org = Organisation.get_organisation(membership)
    if not membership_org:
        return
    wb = Workbook()
    ws = wb.active
    ws.title = f"{membership.capitalize()} Contact Database"  # Less than 32 chars.
    fields = [
        "year",
        "total",
        "withdrawn",
        "new",
        "full",
        "associate",
        "unpaid",
        "never paid",
        "exempted",
        "paid",
        "% paid + exempted",
        "total fees",
    ]
    ws.append(fields)
    start_year = 1997
    for year in range(start_year, helpers.utcnow().year + 1):
        december_31 = datetime(year, 12, 31)
        january_1 = datetime(year, 1, 1)
        current = Year(year)
        current.associate = Organisation.objects.filter(
            __raw__={
                "membership": {
                    "$elemMatch": {
                        "organisation": str(membership_org.pk),
                        "start_date": {"$lte": december_31},
                        "status": "a",
                        "$or": [{"end_date": None}, {"end_date": {"$gt": december_31}}],
                    }
                }
            }
        ).count()
        current.full = Organisation.objects.filter(
            __raw__={
                "membership": {
                    "$elemMatch": {
                        "organisation": str(membership_org.pk),
                        "start_date": {"$lte": december_31},
                        "status": "f",
                        "$or": [{"end_date": None}, {"end_date": {"$gt": december_31}}],
                    }
                }
            }
        ).count()
        current.withdrawn = Organisation.objects.filter(
            __raw__={
                "membership": {
                    "$elemMatch": {
                        "organisation": str(membership_org.pk),
                        "start_date": {"$gte": january_1, "$lte": december_31},
                        "status": "w",
                    }
                }
            }
        ).count()
        current.new = Organisation.objects.filter(
            __raw__={
                "membership": {
                    "$elemMatch": {
                        "organisation": str(membership_org.pk),
                        "start_date": {"$gte": january_1, "$lte": december_31},
                        "status": {"$in": ["f", "a"]},
                    }
                }
            }
        ).count()
        current.never_paid = Organisation.objects.filter(
            __raw__={
                "membership": {
                    "$elemMatch": {
                        "organisation": str(membership_org.pk),
                        "start_date": {"$lte": december_31},
                        "$or": [{"end_date": None}, {"end_date": {"$gt": december_31}}],
                        "status": {"$in": ["f", "a"]},
                    }
                },
                "membership.fees.year": {
                    "$not": {"$in": list(range(start_year, year + 1))}
                },
            }
        ).count()
        fees = list(
            Organisation._get_collection().aggregate(
                [
                    {"$project": {"_id": False, "memberships": "$membership"}},
                    {"$unwind": "$memberships"},
                    {"$replaceRoot": {"newRoot": "$memberships"}},
                    {
                        "$match": {
                            "organisation": str(membership_org.pk),
                            "status": {"$in": ["f", "a"]},
                            "fees.year": year,
                            "start_date": {"$lte": december_31},
                            "$or": [
                                {"end_date": None},
                                {"end_date": {"$gt": december_31}},
                            ],
                        }
                    },
                    {"$unwind": "$fees"},
                    {"$replaceRoot": {"newRoot": "$fees"}},
                    {"$match": {"year": year}},
                ]
            )
        )
        current.total = current.associate + current.full
        current.exempted = len([f for f in fees if f["exempted"]])
        current.total_fees = round(sum(f["amount"] for f in fees), 2)
        current.paid = len(fees)
        current.unpaid = current.total - current.paid
        if current.total:
            current.percent_paid = round(
                (current.exempted + current.paid) / current.total * 100, 2
            )
        ws.append(astuple(current))
    return save_virtual_workbook(wb)


def representatives_stats():
    wb = Workbook()
    ws = wb.active
    ws.title = "ePAG Representatives Statistics"  # Less than 32 chars.
    fields = [
        "country",
        "Count of person_pk",
        "",
        "group_name",
        "Count of person_pk"
    ]
    ws.append(fields)
    representatives_roles = Role.ePAG_representatives()
    countries = defaultdict(int)
    group_names = defaultdict(int)
    for representative_role in representatives_roles:
        # country field
        if not representative_role.person.country_label:
            countries["(empty)"] += 1
        else:
            countries[representative_role.person.country_label] += 1
        # group_name field
        for group in representative_role.groups:
            # TODO: use a constant variable for the group kind
            if group.group.kind == "epag":
                group_names[group.name] += 1
    countries = list(countries.items())
    group_names = list(group_names.items())
    longest_column_length = len(countries) if len(countries) > len(group_names) else len(group_names)
    for row_index in range(longest_column_length + 1):
        row_value = []
        # countries columns
        if row_index < len(countries):
            row_value.append(countries[row_index][0])
            row_value.append(countries[row_index][1])
        elif row_index == len(countries):
            row_value.append("Total")
            row_value.append(sum(country[1] for country in countries))
        else:
            row_value.append("")
            row_value.append("")
        # empty column
        row_value.append("")
        # group_names columns
        if row_index < len(group_names):
            row_value.append(group_names[row_index][0])
            row_value.append(group_names[row_index][1])
        elif row_index == len(group_names):
            row_value.append("Total")
            row_value.append(sum(group_name[1] for group_name in group_names))
        else:
            row_value.append("")
            row_value.append("")
        ws.append(row_value)
    return save_virtual_workbook(wb)


def members_status(membership):
    """
    Args:
        membership (str): 'eurordis' or 'rdi'
    """
    membership_org = Organisation.get_organisation(membership)
    if not membership_org:
        return
    orgs = Organisation.organisation_members(membership_org)
    wb = Workbook()
    ws = wb.active
    ws.title = f"{membership.capitalize()} Members Status"
    fields = [
        "pk",
        "name",
        "email",
        "phone",
        "country",
        "is national alliance",
        "is federation",
        "adherents",
        "budget",
        "creation year",
        "board",
        "paid staff",
        "pharma %",
        "status",
        "since",
        "last payment date",
        "last payment amount",
        "receipt sent",
    ]
    ws.append(fields)
    for org in orgs:
        org_membership = org.organisation_membership(membership_org)
        fee = org_membership.last_fee
        membership_status = org.organisation_membership_status(membership_org)
        membership_start_date = org_membership.start_date
        amount = None
        if fee:
            amount = "exempted" if fee.exempted else fee.amount
        ws.append(
            [
                org.pk,
                org.label,
                org.email,
                org.phone,
                org.country,
                org.kind == 62,
                org.kind == 61,
                org.adherents,
                org.budget,
                org.creation_year,
                org.board,
                org.paid_staff,
                org.pharma_funding,
                membership_status,
                membership_start_date,
                fee and fee.payment_date,
                amount,
                fee and fee.receipt_sent,
            ]
        )
    return save_virtual_workbook(wb)


def withdrawn_members(membership):
    """
    Args:
        membership (str): 'eurordis' or 'rdi'
    """
    membership_org = Organisation.get_organisation(membership)
    if not membership_org:
        return
    wb = Workbook()
    ws = wb.active
    ws.title = f"{membership.capitalize()} Withdrawn Members"
    orgs = Organisation.organisation_members(membership_org, status=["w"])
    ws.append(["pk", "name", "at"])
    rows = []
    for org in orgs:
        date = None
        for membership in org.membership:
            if membership.organisation == membership_org and not membership.end_date:
                date = membership.start_date
                break
        rows.append(
            [
                org.pk,
                org.label,
                date,
            ]
        )
    for row in sorted(rows, key=lambda row: row[2], reverse=True):
        ws.append(row)
    return save_virtual_workbook(wb)


def full_members_without_voting(membership):
    """
    Args:
        membership (str): 'eurordis' or 'rdi'
    """
    membership_org = Organisation.get_organisation(membership)
    if not membership_org:
        return
    orgs = Organisation.organisation_full_members_without_voting(membership_org)
    wb = Workbook()
    ws = wb.active
    ws.title = f"{membership.capitalize()} Full Members without Voting Contact"
    ws.append(["pk", "name", "status"])
    for org in orgs:
        status = ""
        for membership in org.membership:
            if membership.organisation == membership_org and not membership.end_date:
                status = constants.MEMBERSHIP_STATUSES.get(membership.status)
                break
        ws.append([org.pk, org.name, status])
    return save_virtual_workbook(wb)


def members_xlsx(membership):
    """
    Args:
        membership (str): 'eurordis' or 'rdi'
    """
    wb = Workbook()
    ws = wb.active
    ws.title = f"{membership.capitalize()} Members"
    membership_org = Organisation.get_organisation(membership)
    orgs = Organisation.organisation_members(membership_org)
    for org in orgs:
        ws.append(
            [
                constants.COUNTRIES.get(org.country),
                org.label,
                org.website,
                org.email,
                f"{org.organisation_membership_status(membership_org)} Member",
            ]
        )
    return save_virtual_workbook(wb)


def members():
    EURORDIS = Organisation.get_organisation(config.EURORDIS_STR)
    return [
        {
            "pk": org.pk,
            "name": org.name,
            "email": org.email,
            "phone": org.phone,
            "country": org.country,
            "country_label": org.country_label,
            "website": org.website,
            "is_national_alliance": org.kind == 62,
            "is_federation": org.kind == 61,
            "status": org.organisation_membership_status(EURORDIS),
            "diseases": [{"orphanumber": d.pk, "name": d.name} for d in org.diseases],
        }
        for org in Organisation.organisation_members(EURORDIS)
    ]


def events():
    wb = Workbook()
    ws = wb.active
    fields = Event.resource_fields[:] + ["attendees"]
    fields.remove("label")
    fields.remove("resource")
    ws.append(fields)
    events = list(Event._get_collection().find())
    pks = set()
    for event in events:
        pks |= {event["created_by"], event["modified_by"]}
    authorship = {
        pk: email or f"{first_name} {last_name}"
        for pk, email, first_name, last_name in Person.objects.filter(pk__in=pks)
        .scalar("pk", "email", "first_name", "last_name")
        .no_dereference()
    }
    for raw in events:
        raw["pk"] = raw["_id"]
        raw["start_date"] = raw["start_date"].date()  # Getting raw value from DB.
        raw["attendees"] = Role._get_collection().count_documents(
            {"events": {"event": raw["pk"]}}
        )
        resource = helpers.format_for_tabular(
            raw=raw, authorship=authorship, kinds=constants.EVENT_KIND
        )
        ws.append(resource.get(field) for field in fields)
    return save_virtual_workbook(wb)


def role_to_group():
    wb = Workbook()
    ws = wb.active
    roles = Role.objects.filter(__raw__={"groups": {"$nin": [None, []]}})
    fields = [
        "person_pk",
        "first_name",
        "last_name",
        "email",
        "country",
        "organisation_pk",
        "organisation",
        "organisation_kind",
        "group_name",
        "group_role",
        "group_kind",
        "is_active",
    ]
    ws.append(fields)
    for role in roles:
        for rel in role["groups"]:
            ws.append(
                [
                    role.person.pk,
                    role.person.first_name,
                    role.person.last_name,
                    role.get_email(),
                    role.person.country_label,
                    role.organisation.pk,
                    str(role.organisation),
                    constants.ORGANISATION_KIND.get(role.organisation.kind),
                    str(rel["group"]),
                    rel["role"],
                    constants.GROUP_KIND.get(rel["group"].kind),
                    rel["is_active"],
                ]
            )
    return save_virtual_workbook(wb)


def groups():
    wb = Workbook()
    ws = wb.active
    fields = Group.resource_fields[:]
    fields.remove("label")
    fields.remove("resource")
    fields.append("active contacts")
    fields.append("inactive contacts")
    fields.append("all contacts")
    ws.append(fields)
    groups = list(Group._get_collection().find())
    pks = set()
    for group in groups:
        pks |= {group["created_by"], group["modified_by"]}
    authorship = {
        pk: email or f"{first_name} {last_name}"
        for pk, email, first_name, last_name in Person.objects.filter(pk__in=pks)
        .scalar("pk", "email", "first_name", "last_name")
        .no_dereference()
    }
    for raw in groups:
        raw["pk"] = raw["_id"]
        raw["all contacts"] = (
            Role._get_collection().find({"groups.group": raw["_id"]}).count()
        )
        raw["active contacts"] = (
            Role._get_collection()
            .find({"groups": {"$elemMatch": {"group": raw["_id"], "is_active": True}}})
            .count()
        )
        raw["inactive contacts"] = (
            Role._get_collection()
            .find({"groups": {"$elemMatch": {"group": raw["_id"], "is_active": False}}})
            .count()
        )
        resource = helpers.format_for_tabular(
            raw=raw, authorship=authorship, kinds=constants.GROUP_KIND
        )
        ws.append(resource.get(field) for field in fields)
    return save_virtual_workbook(wb)


def keywords():
    wb = Workbook()
    ws = wb.active
    fields = Keyword.resource_fields[:]
    fields.remove("label")
    fields.remove("resource")
    fields.append("count")
    ws.append(fields)
    keywords = list(Keyword._get_collection().find())
    pks = set()
    for keyword in keywords:
        pks |= {keyword["created_by"], keyword["modified_by"]}
    authorship = {
        pk: email or f"{first_name} {last_name}"
        for pk, email, first_name, last_name in Person.objects.filter(pk__in=pks)
        .scalar("pk", "email", "first_name", "last_name")
        .no_dereference()
    }
    for raw in keywords:
        raw["pk"] = raw["_id"]
        raw["count"] = Contact._get_collection().find({"keywords": raw["_id"]}).count()
        resource = helpers.format_for_tabular(
            raw=raw, authorship=authorship, kinds=constants.KEYWORD_KIND
        )
        ws.append(resource.get(field) for field in fields)
    return save_virtual_workbook(wb)


def attendees(event):
    wb = Workbook()
    ws = wb.active
    fields = ["pk", "first_name", "last_name", "email", "role", "organisation"]
    ws.append(fields)
    for role in event.attendees:
        raw = {
            "pk": role.person.pk,
            "first_name": role.person.first_name,
            "last_name": role.person.last_name,
            "email": role.get_email(),
            "organisation": str(role.organisation),
        }
        for e in role.events:
            if e.event == event:
                raw["role"] = e.role
        raw = helpers.format_for_tabular(raw=raw)
        ws.append([raw.get(f, "") for f in fields])
    return save_virtual_workbook(wb)


def all_attendees():
    wb = Workbook()
    ws = wb.active
    fields = [
        "pk",
        "first_name",
        "last_name",
        "email",
        "role",
        "kind of event",
        "event name",
        "event start date",
        "event duration",
        "event location",
        "organisation",
    ]
    ws.append(fields)
    for role in Role.objects(__raw__={"events": {"$nin": [None, []]}}):
        for event in role.events:
            ws.append(
                [
                    role.person.pk,
                    role.person.first_name,
                    role.person.last_name,
                    role.get_email(),
                    event.role,
                    constants.EVENT_KIND.get(event.event.kind),
                    event.event.name,
                    event.event.start_date.isoformat(),
                    event.event.duration,
                    event.event.location,
                    str(role.organisation),
                ]
            )
    return save_virtual_workbook(wb)


def all_voting_contacts():
    wb = Workbook()
    ws = wb.active
    ws.title = "Voting Contacts"
    fields = [
        "person_pk",
        "first_name",
        "last_name",
        "email",
        "role",
        "country",
        "phone",
        "is_primary",
        "is_default",
        "is_voting",
        "organisation_pk",
        "organisation",
        "kind",
        "groups",
        "eurordis_membership"
    ]
    ws.append(fields)
    groups = {g.pk: g for g in Group.objects}
    voting_roles = Role.objects.filter(is_voting=True, is_archived__ne=True)
    for role in voting_roles:
        data = {
            "person_pk": role.person.pk,
            "first_name": role.person.first_name,
            "last_name": role.person.last_name,
            "organisation_pk": role.organisation.pk,
            "organisation": role.organisation.name,
            "kind": role.organisation.kind,
            "role": role.name,
            "country": role.person.country_label,
            "phone": role.phone,
            "email": role.get_email(),
            "is_primary": role.is_primary,
            "is_default": role.is_default,
            "is_voting": role.is_voting,
            "groups": [{"group": str(group.group.pk), "role": group.role, "is_active": group.is_active} for group in role.groups],
            "eurordis_membership": [membership.as_resource() for membership in role.organisation.membership if membership.organisation.pk == config.EURORDIS_PK]
        }
        data = helpers.format_for_tabular(data, groups=groups)
        ws.append([data.get(f) for f in fields])
    return save_virtual_workbook(wb)



def history():
    re_email = re.compile(f"({'|'.join(config.ACCESS_EMAIL_DOMAINS)})$", re.IGNORECASE)
    emails = Role._get_collection().find({"email": re_email}, {"person": 1, "email": 1})
    emails = {e["person"]: e["email"] for e in emails}
    data = list(
        Version._get_collection().find({}, {"by": 1, "at": 1, "action": 1, "_id": -1})
    )
    pks = [d["by"] for d in data]
    people = {
        p["_id"]: emails.get(p["_id"], p.get("email", p["_id"]))
        for p in Person._get_collection().find({"_id": {"$in": pks}}, {"email": 1})
    }
    rows = []
    for version in data:
        rows.append([people[version["by"]], version["at"], version.get("action")])
    output = io.StringIO()
    writer = csv.writer(output, delimiter=";")
    writer.writerow(["by", "at", "action"])
    writer.writerows(rows)
    output.seek(0)
    return output.read()


def diseases():
    covered = set([d.pk for d in Contact.objects.distinct("diseases")])
    fields = [
        "OrphaNumber",
        "Name",
        "Groupings",
        "Validated Groupings",
        "Organisations",
        "Persons",
        "Total",
        "Group of Type",
        "Status",
        "Covered",
    ]
    rows = []
    for disease in Disease.objects.all().no_dereference():
        persons = Person.objects(diseases=disease.pk).count()
        organisations = Organisation.objects(diseases=disease.pk).count()
        if disease.pk in covered:
            is_covered = "direct"
        elif set([ref.id for ref in disease.ancestors]) & covered:
            is_covered = "parent"
        elif set([ref.id for ref in disease.descendants]) & covered:
            is_covered = "child"
        else:
            is_covered = ""
        rows.append(
            [
                disease.pk,
                disease.name,
                ",".join(f"{rel.name}" for rel in disease.groupings),
                ",".join(f"{rel.name}" for rel in disease.groupings if rel.validated),
                organisations,
                persons,
                organisations + persons,
                constants.DISEASE_GROUP_OF_TYPES.get(disease.group_of_type),
                disease.status,
                is_covered,
            ]
        )
    output = io.StringIO()
    writer = csv.writer(output, delimiter=";")
    writer.writerow(fields)
    writer.writerows(rows)
    output.seek(0)
    return output.read()


def voting():
    roles = Role.objects(is_voting=True)
    wb = Workbook()
    ws = wb.active
    fields = ["pk", "name", "country", "contact", "email"]
    ws.append(fields)
    for role in roles:
        ws.append(
            [
                role.organisation.pk,
                role.organisation.name,
                role.organisation.country_label,
                str(role.person),
                role.get_email(),
            ]
        )
    return save_virtual_workbook(wb)


def organisations():
    authorship = {
        pk: email or f"{first_name} {last_name}"
        for pk, email, first_name, last_name in Person.objects.scalar(
            "pk", "email", "first_name", "last_name"
        ).no_dereference()
    }
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    fields = Organisation.resource_fields[:]
    fields.insert(fields.index("country") + 1, "country_label")
    fields.insert(fields.index("membership") + 1, "rdi_membership")
    fields.insert(fields.index("membership") + 1, "eurordis_membership")
    fields.remove("membership")
    fields.remove("uri")
    fields.remove("label")
    fields.remove("resource")
    fields.remove("roles")
    fields.remove("attachments")
    ws.append(fields)
    groups = {g.pk: g for g in Group.objects.filter()}
    keywords = {k.pk: k.name for k in Keyword.objects.filter()}
    qs = Contact._get_collection().find({"_cls": Organisation._class_name}).sort("_id")
    for raw in qs:
        raw["pk"] = raw["_id"]
        raw["groups"] = raw.get("groups") or []
        raw["diseases"] = raw.get("diseases") or []
        raw["organisation"] = raw.get("name")
        raw["country_label"] = constants.COUNTRIES.get(raw.get("country"))
        # Different fields for RDI and EURORDIS membership
        raw["eurordis_membership"] = []
        raw["rdi_membership"] = []
        if raw.get("membership"):
            for membership in raw.get("membership"):
                if membership.get("organisation") == config.EURORDIS_PK:
                    raw["eurordis_membership"].append(membership)
                elif membership.get("organisation") == config.RDI_PK:
                    raw["rdi_membership"].append(membership)
            del raw["membership"]
        resource = helpers.format_for_tabular(
            raw=raw, keywords=keywords, groups=groups, authorship=authorship
        )
        ws.append(resource.get(field) for field in fields)
    return save_virtual_workbook(wb)


def epag_members():
    groups = list(Group.objects(kind="epag").scalar("pk").no_dereference())
    roles = Role.objects(
        __raw__={
            "groups": {
                "$elemMatch": {
                    "group": {"$in": groups},
                    "role": "Patient Representative",
                    "is_active": True,
                }
            }
        }
    )
    out = []
    for role in roles:
        ern = [r.group.name for r in role.groups if r.group.name.startswith("ERN ")]
        out.append(
            {
                "fullname": str(role.person),
                "organisation": str(role.organisation),
                "website": role.organisation.website,
                "country": role.person.country,
                "ern": ern[0] if ern else None,
            }
        )
    return out


def dump(model):
    authorship = {
        pk: email or f"{first_name} {last_name}"
        for pk, email, first_name, last_name in Person.objects.scalar(
            "pk", "email", "first_name", "last_name"
        ).no_dereference()
    }
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    fields = model.resource_fields[:]
    fields.insert(fields.index("country") + 1, "country_label")
    fields.remove("uri")
    fields.remove("label")
    fields.remove("resource")
    fields.remove("roles")
    fields.remove("attachments")
    if model == Person:
        events = {
            pk: f"{name} ({start_date})"
            for pk, name, start_date in Event.objects.scalar(
                "pk", "name", "start_date"
            ).no_dereference()
        }
    else:
        events = {}
    ws.append(fields)
    groups = {g.pk: g for g in Group.objects.filter()}
    keywords = {k.pk: k.name for k in Keyword.objects.filter()}
    qs = Contact._get_collection().find({"_cls": model._class_name}).sort("_id")
    for raw in qs:
        raw["pk"] = raw["_id"]
        raw["groups"] = raw.get("groups") or []
        raw["diseases"] = raw.get("diseases") or []
        raw["organisation"] = raw.get("name")
        raw["events"] = [events[r["event"]] for r in raw.get("events") or []]
        raw["country_label"] = constants.COUNTRIES.get(raw.get("country"))
        resource = helpers.format_for_tabular(
            raw=raw, keywords=keywords, groups=groups, authorship=authorship
        )
        ws.append(resource.get(field) for field in fields)
    return save_virtual_workbook(wb)


def dump_roles():
    persons = {
        pk: [first_name, last_name]
        for pk, first_name, last_name in Person.objects.scalar(
            "pk", "first_name", "last_name"
        ).no_dereference()
    }
    organisations = {
        pk: (name, kind, email)
        for pk, name, kind, email in Organisation.objects.scalar(
            "pk", "name", "kind", "email"
        ).no_dereference()
    }
    groups = {g.pk: g for g in Group.objects.filter()}
    roles = Role._get_collection().find({"is_archived": {"$ne": True}})
    fields = [
        "person_pk",
        "first_name",
        "last_name",
        "email",
        "role",
        "phone",
        "is_primary",
        "is_default",
        "is_voting",
        "organisation_pk",
        "organisation",
        "kind",
        "groups",
    ]
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    ws.append(fields)
    for role in roles:
        organisation, organisation_kind, organisation_email = organisations[role["organisation"]]
        pk = role["person"]
        data = {
            "person_pk": pk,
            "first_name": persons[pk][0],
            "last_name": persons[pk][1],
            "organisation_pk": role["organisation"],
            "organisation": organisation,
            "kind": organisation_kind,
            "role": role.get("name"),
            "phone": role.get("phone"),
            "email": role.get("email") or organisation_email,
            "is_primary": role.get("is_primary"),
            "is_default": role.get("is_default"),
            "is_voting": role.get("is_voting"),
            "groups": role.get("groups") or [],
        }
        data = helpers.format_for_tabular(data, groups=groups)
        ws.append([data.get(f) for f in fields])
    return save_virtual_workbook(wb)
