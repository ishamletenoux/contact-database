import contextvars
from contextlib import contextmanager

user_id = contextvars.ContextVar("user_id")
ghost = contextvars.ContextVar("ghost", default=False)


@contextmanager
def bot():
    from eurordis.models import Person
    from eurordis import config
    bot = Person.objects.get(email=config.BOT_EMAIL)
    user_id.set(bot.pk)
    yield


@contextmanager
def as_user(user):
    old = user_id.get(None)
    user_id.set(user.pk)
    yield
    user_id.set(old)
