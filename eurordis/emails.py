import smtplib
import ssl
from email.message import EmailMessage

from . import config
from .loggers import logger


ACCESS_GRANTED = """Hi {person},

You've been granted an access to the Contact Database, please click the following link:

{link}

It will expire on {date} at {time}.

The Eurordis Contact Database Team
"""

STAFF_REPORT = """Hi Eurordis Staff,

Here is your database chore report.

Full members without a primary contact:

{forsaken}

Full members without a voting contact:

{no_voting}

Duplicate emails in roles:

{in_roles}

Emails from roles that also are in contacts:

{in_contacts}

Diseases linked to contacts but without grouping:

{no_groupings}

Persons with multiple organisations but no default one:

{no_default}

Persons with a single organisation which is set as default :

{single_default}

Your Devoted Little Bot
"""

RECEIPT = """Dear {person},

Following the reception of your contribution {year}, we are pleased to address your receipt for payment.

Best regards,

Annie Rahajarizafy
Accounting Manager

EURORDIS - Rare Diseases Europe
Paris Office - Plateforme Maladies Rares - 96 rue Didot 75014 Paris - France
Phone : +33 1 56 53 13 65
"""

MAILCHIMP_REPORT = """Hi Eurordis Staff,

Some Mailchimp tests have failed:

{messages}
"""


def send(to, subject, body, attachment=None, sender=config.FROM_EMAIL):
    msg = EmailMessage()
    msg.set_content(body)
    msg["Subject"] = subject
    msg["From"] = sender
    msg["To"] = to
    if attachment:
        maintype, subtype = attachment.content_type.split("/", 1)
        msg.add_attachment(
            attachment.read(),
            maintype=maintype,
            subtype=subtype,
            filename=attachment.filename,
        )
    if not config.SEND_EMAILS:
        return print("Sending email", str(msg))
    context = ssl.create_default_context()
    with smtplib.SMTP(config.SMTP_HOST, config.SMTP_PORT) as server:
        if config.SMTP_TLS:
            server.starttls(context=context)
        try:
            if config.SMTP_LOGIN:
                server.login(config.SMTP_LOGIN, config.SMTP_PASSWORD)
            server.send_message(msg)
        except smtplib.SMTPException as err:
            raise RuntimeError from err
        else:
            logger.debug(f"Email sent to {to}: {subject}")
