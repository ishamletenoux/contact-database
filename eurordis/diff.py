from datetime import datetime

from mongoengine.errors import DoesNotExist

from . import constants


class Diff:

    @property
    def of(self):
        try:
            of = self.source.of.fetch()
        except DoesNotExist:  # Deleted instance.
            resource = self.source.of.document_type.resource_name()
            pk = self.source.of.id
            return {
                "label": self.source.label,
                "pk": pk,
                "resource": resource,
                "uri": f"/{resource}/{pk}",
            }
        else:
            return {
                "label": str(of),
                "pk": of.pk,
                "resource": of.resource,
                "uri": of.uri,
            }

    @classmethod
    def make(cls, new, old):
        if not isinstance(old, dict):  # Version
            old = old.raw
        keys = list(new.raw) + list(old)
        keys = set(keys) & set(new.of.document_type.editable_fields + ["roles"])
        diff = {}
        for key in keys:
            old_value = old.get(key)
            new_value = new.raw.get(key)
            subordinates = []
            if (new_value or old_value) and new_value != old_value:
                func = f"diff_{key}"
                if hasattr(cls, func):
                    subordinates = getattr(cls, func)(old_value, new_value)
                else:
                    subordinates = cls.flat_diff(key, old_value, new_value)
            if subordinates:
                diff[key] = subordinates
        return diff

    @staticmethod
    def flat_diff(key, old, new):
        if isinstance(old, datetime):
            old = old.date().isoformat()
        if isinstance(new, datetime):
            new = new.date().isoformat()
        verb = "update"
        if old in ["", None] and new:
            verb = "create"
        elif new in ["", None] and old:
            verb = "delete"
        subordinate = {"subject": key, "verb": verb}
        if old not in [None, ""]:
            subordinate["old"] = old
        if new not in [None, ""]:
            subordinate["new"] = new
        return subordinate

    @staticmethod
    def diff_kind(old, new):
        return {
            "subject": "kind",
            "verb": old and new and "update" or new and "create" or "delete",
            "old": constants.ORGANISATION_KIND.get(old, old),
            "new": constants.ORGANISATION_KIND.get(new, new),
        }

    @staticmethod
    def diff_groups(old, new):
        from .models import Group

        keys = ["is_active", "role"]
        subordinates = []
        # Skip old groups relations for now (pk=str instead of group=id)
        old = {v["group"]: v for v in old or [] if "group" in v}
        new = {v["group"]: v for v in new or [] if "group" in v}
        for pk, value in new.items():
            if pk not in old:
                subordinates.append(
                    {
                        "subject": Group.get_name(value["group"]),
                        "verb": "create",
                        "new": value,
                    }
                )
            elif new[pk] != old[pk]:
                subs = []
                for key in keys:
                    before = old[pk].get(key)
                    after = new[pk].get(key)
                    if (before or after) and before != after:
                        subs.append(Diff.flat_diff(key, before, after))
                if subs:  # Do we have a real diff (not eg. "" vs None)
                    subordinates.append(
                        {
                            "subject": Group.get_name(value["group"]),
                            "verb": "update",
                            "subordinates": subs,
                        }
                    )
        for pk, value in old.items():
            if pk not in new:
                subordinates.append(
                    {
                        "subject": Group.get_name(value["group"]),
                        "verb": "delete",
                        "old": value,
                    }
                )

        return subordinates

    @staticmethod
    def diff_events(old, new):
        from .models import Event

        keys = ["role"]
        subordinates = []
        old = {v["event"]: v for v in old or []}
        new = {v["event"]: v for v in new or []}
        for pk, value in new.items():
            if pk not in old:
                subordinates.append(
                    {
                        "subject": Event.get_name(value["event"]),
                        "verb": "create",
                        "new": value,
                    }
                )
            elif new[pk] != old[pk]:
                subs = []
                for key in keys:
                    before = old[pk].get(key)
                    after = new[pk].get(key)
                    if (before or after) and before != after:
                        subs.append(Diff.flat_diff(key, before, after))
                if subs:  # Do we have a real diff (not eg. "" vs None)
                    subordinates.append(
                        {
                            "subject": Event.get_name(value["event"]),
                            "verb": "update",
                            "subordinates": subs,
                        }
                    )
        for pk, value in old.items():
            if pk not in new:
                subordinates.append(
                    {
                        "subject": Event.get_name(value["event"]),
                        "verb": "delete",
                        "old": value,
                    }
                )

        return subordinates

    @staticmethod
    def diff_membership(old, new):
        from .models import Organisation

        keys = [
            "status",
            "start_date",
            "end_date",
            "fees",
        ]
        subordinates = []
        old = {v["organisation"]: v for v in old or [] if "organisation" in v}
        new = {v["organisation"]: v for v in new or [] if "organisation" in v}
        for pk, value in new.items():
            if pk not in old:
                subordinates.append(
                    {
                        "subject": Organisation.get_name(value["organisation"]),
                        "verb": "create",
                        "new": value,
                    }
                )
            elif new[pk] != old[pk]:
                subs = []
                for key in keys:
                    before = old[pk].get(key)
                    after = new[pk].get(key)
                    if (before or after) and before != after:
                        if key == "fees":
                            old_fees = {f["year"]: f for f in before}
                            new_fees = {f["year"]: f for f in after}
                            for year, props in new_fees.items():
                                if year not in old_fees:
                                    subs.append(
                                        {
                                            "subject": f"fee {year}",
                                            "verb": "create",
                                            "new": props,
                                        }
                                    )
                                elif old_fees[year] != new_fees[year]:
                                    for skey in [
                                        "amount",
                                        "payment_date",
                                        "exempted",
                                        "receipt_sent",
                                    ]:
                                        sbefore = old_fees[year].get(skey)
                                        safter = new_fees[year].get(skey)
                                        if sbefore != safter:
                                            subs.append(
                                                Diff.flat_diff(
                                                    f"fee {year} {skey}",
                                                    sbefore,
                                                    safter,
                                                )
                                            )
                        else:
                            subs.append(Diff.flat_diff(key, before, after))
                if subs:  # Do we have a real diff (not eg. "" vs None)
                    subordinates.append(
                        {
                            "subject": Organisation.get_name(value["organisation"]),
                            "verb": "update",
                            "subordinates": subs,
                        }
                    )
        for pk, value in old.items():
            if pk not in new:
                subordinates.append(
                    {
                        "subject": Organisation.get_name(value["organisation"]),
                        "verb": "delete",
                        "old": value,
                    }
                )

        return subordinates

    @staticmethod
    def diff_keywords(old, new):
        from .models import Keyword

        old = old or []
        new = new or []
        subordinates = [
            {"subject": Keyword.get_name(v), "verb": "delete", "value": v}
            for v in old
            if v not in new
        ]
        subordinates += [
            {"subject": Keyword.get_name(v), "verb": "create", "value": v}
            for v in new
            if v not in old
        ]
        return subordinates

    @staticmethod
    def diff_diseases(old, new):
        from .models import Disease

        old = old or []
        new = new or []
        subordinates = [
            {"subject": Disease.get_name(v), "verb": "delete", "value": v}
            for v in old
            if v not in new
        ]
        subordinates += [
            {"subject": Disease.get_name(v), "verb": "create", "value": v}
            for v in new
            if v not in old
        ]
        return subordinates

    @staticmethod
    def diff_languages(old, new):
        old = old or []
        new = new or []
        subordinates = [
            {"subject": constants.LANGUAGES.get(v, v), "verb": "delete", "value": v}
            for v in old
            if v not in new
        ]
        subordinates += [
            {"subject": constants.LANGUAGES.get(v, v), "verb": "create", "value": v}
            for v in new
            if v not in old
        ]
        return subordinates

    @staticmethod
    def diff_roles(old, new):
        from .models import Organisation, Person, Role

        keys = Role.relation_fields
        subordinates = []
        old = old or []
        new = new or []
        if not old and not new:
            return
        # Are we dealing with Person or Organisation?
        first = (old + new)[0]
        if "person" in first:
            klass = Person
            relation = "person"
        else:
            klass = Organisation
            relation = "organisation"
        old = {v[relation]: v for v in old}
        new = {v[relation]: v for v in new}
        for pk, value in new.items():
            if pk not in old:
                subordinates.append(
                    {
                        "subject": klass.get_name(value[relation]),
                        "verb": "create",
                        "new": value,
                    }
                )
            elif new[pk] != old[pk]:
                subs = []
                for key in keys:
                    before = old[pk].get(key)
                    after = new[pk].get(key)
                    if (before or after) and before != after:
                        subs.append(Diff.flat_diff(key, before, after))
                if subs:  # Do we have a real diff (not eg. "" vs None)
                    subordinates.append(
                        {
                            "subject": klass.get_name(value[relation]),
                            "verb": "update",
                            "subordinates": subs,
                        }
                    )
        for pk, value in old.items():
            if pk not in new:
                subordinates.append(
                    {
                        "subject": klass.get_name(value[relation]),
                        "verb": "delete",
                        "old": value,
                    }
                )

        return subordinates
