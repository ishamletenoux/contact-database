import re
from zipfile import BadZipFile

from mongoengine.errors import ValidationError
from openpyxl import Workbook, load_workbook

from . import config, constants, helpers
from .models import Keyword, Organisation, Person, Role

COLLATION = {"locale": "en", "strength": 1, "alternate": "shifted"}


def process(wb):
    if not isinstance(wb, Workbook):
        try:
            wb = load_workbook(wb, data_only=True, read_only=True)
        except BadZipFile:
            raise ValueError("Can't read file. Is that an XLSX?")
    reports = []
    persons = []
    roles = []
    rows = list(wb.active.values)
    headers = rows[0]
    if not set(headers) >= {"email", "last_name", "first_name"}:
        raise ValueError("Missing mandatory columns: first_name, last_name, email")
    for row in rows[1:]:
        if not any(row):
            continue  # Empty row
        raw = dict(zip(headers, row))
        raw = {k: v for k, v in raw.items() if k}
        person, status, report, role = get_or_create(**raw)
        data = {}
        persons.append(person)
        if person:
            data = person.as_relation()
            if not role:
                role = person.default_role
        roles.append(role)
        reports.append({"data": data, "raw": raw, "report": report, "status": status})
    return reports, persons, roles


def get_or_create(email, first_name, last_name, **extra):
    report = {}
    if config.TOBEREVIEWED_KEYWORD:
        keywords = [Keyword.get(config.TOBEREVIEWED_KEYWORD)]
    else:
        keywords = []
    first_name = helpers.clean_title(first_name or "").strip()
    last_name = helpers.clean_title(last_name or "").strip()
    re_fn = re.compile(first_name, re.IGNORECASE)
    re_ln = re.compile(last_name, re.IGNORECASE)
    if not email:
        report["email"] = "Missing email"
        return None, "error", report, None
    email = email.lower().strip()
    # 1. Match with email.
    try:
        org = Organisation.objects.get(email=email)
    except Organisation.DoesNotExist:
        # We can seach for a person
        person = Person.from_email(email)
        org = None
    else:
        person = None
        report["email"] = f"Email found on organisation `{org}`"
        status = "error"
    if not person:
        # 2. Match with first name and last name, without diacritic, case insensitive.
        person = Person.objects.collation(COLLATION).filter(
            __raw__={"$text": {"$search": first_name}, "last_name": last_name}
        )
        if not person:
            person = Person.objects.collation(COLLATION).filter(
                __raw__={"$text": {"$search": last_name}, "first_name": first_name}
            )
        if len(person) > 1:
            roles = None
            if org:
                roles = Role.objects.filter(organisation=org, person=person)
            if roles:
                status = "found"
                person = roles[0].person
                del report["email"]
            else:
                status = "error"
                report["error"] = "Multiple objects with same first name and last name"
                person = None
        elif not person:
            if org:
                person = None
            else:
                # 3. Create new.
                person = Person(
                    email=email,
                    first_name=first_name,
                    last_name=last_name,
                    keywords=keywords,
                )
                status = "created"
        else:
            status = "found"
            person = person[0]
            emails = {r.email for r in person.roles}
            # The email used is not even in roles emails.
            if email not in emails:
                if person.email:
                    report[
                        "email"
                    ] = f"User `{person}` has another email in DB: `{person.email}`"
                # Do not attach email if it was found on an organisation.
                elif not org:
                    person.email = email
                    report["email"] = f"Attached email `{person.email}`"
    else:
        status = "found"
    if status == "found":
        fn_matched = re_fn.search(person.first_name)
        ln_matched = re_ln.search(person.last_name)
        if not fn_matched:
            # Not an exact match, let's warn.
            report[
                "first_name"
            ] = f"Searched '{first_name}', found '{person.first_name}'"
        if not ln_matched:
            # Not an exact match, let's warn.
            report["last_name"] = f"Searched '{last_name}', found '{person.last_name}'"
    if status != "error":
        for key, value in extra.items():
            if value and key in Person._fields and getattr(person, key, None) is None:
                field = Person._fields[key]
                value = field.to_python(value)
                if key == "country" and value in constants.COUNTRIES_REVERSED:
                    value = constants.COUNTRIES_REVERSED[value]
                setattr(person, key, value)
        if person._created or person._get_changed_fields():
            try:
                person.save()
            except ValidationError as e:
                person = None
                status = "error"
                # Expose email value for error reporting.
                # We don't use native MongoEngine error messages, as those can be very
                # verbose, eg. when given an invalid value for a country we have the
                # whole list of valid values in the error message.
                extra["email"] = email
                errors = helpers.format_validation_error(e).get("errors", {})
                errors = [f"Invalid value for {k}: '{extra.get(k)}'" for k in errors]
                report = {"error": ", ".join(errors)}
    role = None
    if status != "error":
        role = attach_organisation(person, email, report, extra)
    return person, status, report, role


def attach_organisation(person, email, report, extra):
    name = extra.get("organisation")
    position = extra.get("position")
    domain = email.split('@')[1] if len(email.split('@')) > 1 else ''
    re_email = re.compile(f"{domain}$", re.IGNORECASE)
    re_website = re.compile(domain, re.IGNORECASE)
    organisation = None
    if name:
        organisation = Organisation.objects.collation(COLLATION).filter(
            __raw__={"$or": [{"name": name}, {"acronym": name}]}
        )
        if len(organisation) > 1:
            report["organisation"] = f"Multiple organisation matching '{name}'"
            organisation = None
        elif not organisation:
            report["organisation"] = f"No organisation matching '{name}'"
    if not organisation and domain not in constants.GENERIC_EMAILS:
        organisation = Organisation.objects(
            __raw__={"$or": [{"email": re_email}, {"website": re_website}]}
        )
    if organisation and len(organisation) > 1:
        pks = ",".join([o.pk for o in organisation])
        report["organisation"] = f"Multiple organisation matching '{domain}': {pks}"
        organisation = None
    if organisation:
        organisation = organisation[0]
        try:
            role = Role.get(organisation=organisation, person=person)
        except Role.DoesNotExist:
            role = Role(organisation=organisation, person=person)
            report["organisation"] = f"Attached to organisation '{organisation}'"
        if position and position != role.name:
            role.name = position
        org_website = organisation.website or ""
        org_email = organisation.email or ""
        if domain in org_website or domain in org_email or email != person.email:
            if person.email == email:
                person.email = None
                person.save()
            role.email = email
        if role._created or role._get_changed_fields():
            role.save()
        return role
